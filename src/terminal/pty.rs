//! Handles a connection to a terminal. A lot of this is borrowed from
//! alacritty: https://github.com/alacritty/alacritty

use std::{
  env,
  ffi::CStr,
  fs::File,
  io::{Error, ErrorKind, Result},
  mem,
  mem::MaybeUninit,
  os::unix::{
    io::{FromRawFd, RawFd},
    process::CommandExt,
  },
  process::{Child, Command, Stdio},
  ptr,
};

use libc::{self, c_int, winsize, TIOCSCTTY};

/// Get raw fds for master/slave ends of a new PTY.
fn make_pty(size: libc::winsize) -> Result<(RawFd, RawFd)> {
  let mut window_size = size;
  window_size.ws_xpixel = 0;
  window_size.ws_ypixel = 0;

  let mut slave = mem::MaybeUninit::<libc::c_int>::uninit();
  let mut master = mem::MaybeUninit::<libc::c_int>::uninit();
  unsafe {
    let ret = libc::openpty(
      master.as_mut_ptr(),
      slave.as_mut_ptr(),
      ptr::null_mut(),
      ptr::null_mut(),
      &size,
    );

    assert_eq!(ret, 0, "openpty failed");

    Ok((master.assume_init(), slave.assume_init()))
  }
}

/// Really only needed on BSD, but should be fine elsewhere.
fn set_controlling_terminal(fd: c_int) {
  let res = unsafe {
    // TIOSCTTY changes based on platform and the `ioctl` call is different
    // based on architecture (32/64). So a generic cast is used to make sure
    // there are no issues. To allow such a generic cast the clippy warning
    // is disabled.
    #[allow(clippy::cast_lossless)]
    libc::ioctl(fd, TIOCSCTTY as _, 0)
  };

  if res < 0 {
    panic!("ioctl TIOCSCTTY failed: {}", Error::last_os_error());
  }
}

#[derive(Debug)]
struct Passwd<'a> {
  name:  &'a str,
  dir:   &'a str,
  shell: &'a str,
}

/// Return a Passwd struct with pointers into the provided buf.
fn get_passwd<'a>(buf: &'a mut [i8; 1024]) -> Result<Passwd<'a>> {
  // Create zeroed passwd struct.
  let mut entry: MaybeUninit<libc::passwd> = MaybeUninit::uninit();

  let mut res: *mut libc::passwd = ptr::null_mut();

  // Try and read the pw file.
  let uid = unsafe { libc::getuid() };
  let status = unsafe {
    libc::getpwuid_r(uid, entry.as_mut_ptr(), buf.as_mut_ptr() as *mut _, buf.len(), &mut res)
  };
  let entry = unsafe { entry.assume_init() };

  if status < 0 {
    return Err(Error::new(ErrorKind::Other, "getpwuid_r failed"));
  }

  if res.is_null() {
    return Err(Error::new(ErrorKind::Other, "pw not found"));
  }

  // Sanity check.
  assert_eq!(entry.pw_uid, uid);

  // Build a borrowed Passwd struct.
  Ok(Passwd {
    name:  unsafe { CStr::from_ptr(entry.pw_name).to_str().unwrap() },
    dir:   unsafe { CStr::from_ptr(entry.pw_dir).to_str().unwrap() },
    shell: unsafe { CStr::from_ptr(entry.pw_shell).to_str().unwrap() },
  })
}

pub struct Pty {
  pub child: Child,
  pub file:  File,
}

/// User information that is required for a new shell session.
struct ShellUser {
  user:  String,
  home:  String,
  shell: String,
}

impl ShellUser {
  /// look for shell, username, longname, and home dir in the respective
  /// environment variables before falling back on looking in to `passwd`.
  fn from_env() -> Result<Self> {
    let user = env::var("USER").ok();
    let home = env::var("HOME").ok();
    let shell = env::var("SHELL").ok();

    match (user, home, shell) {
      (Some(user), Some(home), Some(shell)) => Ok(Self { user, home, shell }),
      (user, home, shell) => {
        let mut buf = [0; 1024];
        let pw = get_passwd(&mut buf)?;

        Ok(Self {
          user:  user.unwrap_or_else(|| pw.name.to_owned()),
          home:  home.unwrap_or_else(|| pw.dir.to_owned()),
          shell: shell.unwrap_or_else(|| pw.shell.to_owned()),
        })
      }
    }
  }
}

// This is what alacritty does, so I'll do the same.

#[cfg(not(target_os = "macos"))]
fn default_shell_command(shell: &str, _user: &str) -> Command { Command::new(shell) }

#[cfg(target_os = "macos")]
fn default_shell_command(shell: &str, user: &str) -> Command {
  let shell_name = shell.rsplit('/').next().unwrap();

  // On macOS, use the `login` command so the shell will appear as a tty session.
  let mut login_command = Command::new("/usr/bin/login");

  // Exec the shell with argv[0] prepended by '-' so it becomes a login shell.
  // `login` normally does this itself, but `-l` disables this.
  let exec = format!("exec -a -{} {}", shell_name, shell);

  // -f: Bypasses authentication for the already-logged-in user.
  // -l: Skips changing directory to $HOME and prepending '-' to argv[0].
  // -p: Preserves the environment.
  //
  // XXX: we use zsh here over sh due to `exec -a`.
  login_command.args(["-flp", user, "/bin/zsh", "-c", &exec]);
  login_command
}

/// Create a new TTY and return a handle to interact with it.
impl Pty {
  pub fn new() -> Result<Pty> {
    // TODO
    let num_lines = 40;
    let num_cols = 100;
    let cell_width = 20;
    let cell_height = 50;

    let ws_row = num_lines as libc::c_ushort;
    let ws_col = num_cols as libc::c_ushort;

    let ws_xpixel = ws_col * cell_width as libc::c_ushort;
    let ws_ypixel = ws_row * cell_height as libc::c_ushort;
    let size = winsize { ws_row, ws_col, ws_xpixel, ws_ypixel };

    let (master, slave) = make_pty(size)?;

    unsafe {
      let mut termios = MaybeUninit::<libc::termios>::uninit();
      libc::tcgetattr(master, termios.as_mut_ptr());
      let mut termios = termios.assume_init();
      termios.c_iflag |= libc::IUTF8;
      let _ = libc::tcsetattr(master, libc::TCSANOW, &termios);
    }

    let user = ShellUser::from_env()?;
    let mut builder = default_shell_command(&user.shell, &user.user);

    // Setup child stdin/stdout/stderr as slave fd of PTY.
    // Ownership of fd is transferred to the Stdio structs and will be closed by
    // them at the end of this scope. (It is not an issue that the fd is closed
    // three times since File::drop ignores error on libc::close.).
    builder.stdin(unsafe { Stdio::from_raw_fd(slave) });
    builder.stderr(unsafe { Stdio::from_raw_fd(slave) });
    builder.stdout(unsafe { Stdio::from_raw_fd(slave) });

    // Setup shell environment.
    builder.env("USER", user.user);
    builder.env("HOME", user.home);

    // TODO:
    // Set Window ID for clients relying on X11 hacks.
    // builder.env("WINDOWID", window_id);

    unsafe {
      builder.pre_exec(move || {
        // Create a new process group.
        let err = libc::setsid();
        if err == -1 {
          return Err(Error::new(ErrorKind::Other, "Failed to set session id"));
        }

        set_controlling_terminal(slave);

        // No longer need slave/master fds.
        libc::close(slave);
        libc::close(master);

        libc::signal(libc::SIGCHLD, libc::SIG_DFL);
        libc::signal(libc::SIGHUP, libc::SIG_DFL);
        libc::signal(libc::SIGINT, libc::SIG_DFL);
        libc::signal(libc::SIGQUIT, libc::SIG_DFL);
        libc::signal(libc::SIGTERM, libc::SIG_DFL);
        libc::signal(libc::SIGALRM, libc::SIG_DFL);

        Ok(())
      });
    }

    // TODO:
    // Handle set working directory option.
    // if let Some(dir) = &config.working_directory {
    //   builder.current_dir(dir);
    // }

    match builder.spawn() {
      Ok(child) => {
        unsafe {
          set_nonblocking(master);
        }

        let pty = Pty { child, file: unsafe { File::from_raw_fd(master) } };
        // TODO:
        // pty.on_resize(window_size);
        Ok(pty)
      }
      Err(err) => Err(Error::new(
        err.kind(),
        format!("Failed to spawn command '{}': {}", builder.get_program().to_string_lossy(), err),
      )),
    }
  }
}

impl Drop for Pty {
  fn drop(&mut self) {
    // Make sure the PTY is terminated properly.
    unsafe {
      libc::kill(self.child.id() as i32, libc::SIGHUP);
    }
    let _ = self.child.wait();
  }
}

/*
impl EventedReadWrite for Pty {
  type Reader = File;
  type Writer = File;

  #[inline]
  fn register(
    &mut self,
    poll: &mio::Poll,
    token: &mut dyn Iterator<Item = mio::Token>,
    interest: mio::Ready,
    poll_opts: mio::PollOpt,
  ) -> Result<()> {
    self.token = token.next().unwrap();
    poll.register(&EventedFd(&self.file.as_raw_fd()), self.token, interest, poll_opts)?;

    self.signals_token = token.next().unwrap();
    poll.register(&self.signals, self.signals_token, mio::Ready::readable(), mio::PollOpt::level())
  }

  #[inline]
  fn reregister(
    &mut self,
    poll: &mio::Poll,
    interest: mio::Ready,
    poll_opts: mio::PollOpt,
  ) -> Result<()> {
    poll.reregister(&EventedFd(&self.file.as_raw_fd()), self.token, interest, poll_opts)?;

    poll.reregister(
      &self.signals,
      self.signals_token,
      mio::Ready::readable(),
      mio::PollOpt::level(),
    )
  }

  #[inline]
  fn deregister(&mut self, poll: &mio::Poll) -> Result<()> {
    poll.deregister(&EventedFd(&self.file.as_raw_fd()))?;
    poll.deregister(&self.signals)
  }

  #[inline]
  fn reader(&mut self) -> &mut File { &mut self.file }

  #[inline]
  fn read_token(&self) -> mio::Token { self.token }

  #[inline]
  fn writer(&mut self) -> &mut File { &mut self.file }

  #[inline]
  fn write_token(&self) -> mio::Token { self.token }
}
*/

/*
impl EventedPty for Pty {
  #[inline]
  fn next_child_event(&mut self) -> Option<ChildEvent> {
    self.signals.pending().next().and_then(|signal| {
      if signal != sigconsts::SIGCHLD {
        return None;
      }

      match self.child.try_wait() {
        Err(e) => {
          error!("Error checking child process termination: {}", e);
          None
        }
        Ok(None) => None,
        Ok(_) => Some(ChildEvent::Exited),
      }
    })
  }

  #[inline]
  fn child_event_token(&self) -> mio::Token { self.signals_token }
}
*/

/*
impl OnResize for Pty {
  /// Resize the PTY.
  ///
  /// Tells the kernel that the window size changed with the new pixel
  /// dimensions and line/column counts.
  fn on_resize(&mut self, window_size: WindowSize) {
    let win = window_size.to_winsize();

    let res = unsafe { libc::ioctl(self.file.as_raw_fd(), libc::TIOCSWINSZ, &win as *const _) };

    if res < 0 {
      die!("ioctl TIOCSWINSZ failed: {}", Error::last_os_error());
    }
  }
}
*/

unsafe fn set_nonblocking(fd: c_int) {
  use libc::{fcntl, F_GETFL, F_SETFL, O_NONBLOCK};

  let res = fcntl(fd, F_SETFL, fcntl(fd, F_GETFL, 0) | O_NONBLOCK);
  assert_eq!(res, 0);
}

#[test]
fn test_get_passwd() {
  let mut buf: [i8; 1024] = [0; 1024];
  let _pw = get_passwd(&mut buf).unwrap();
}
