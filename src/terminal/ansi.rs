use crate::{
  editor::Pos,
  gui::{
    api::Color,
    text::{Cell, StringWithColors},
  },
};

// Stores all the state of the current ansi cursor.
pub struct RenderCursor {
  pub screen: Vec<StringWithColors>,
  pub cursor: Pos,

  flags:      AnsiFlags,
  foreground: AnsiColor,
  background: AnsiColor,
}

#[derive(Debug, PartialEq, Eq)]
enum AnsiColor {
  Default,
  Builtin(BuiltinColor),
  BrightBuiltin(BuiltinColor),
  RGB(Color),
}

#[derive(Debug, PartialEq, Eq)]
enum BuiltinColor {
  Black,
  Red,
  Green,
  Yellow,
  Blue,
  Magenta,
  Cyan,
  White,
}

impl AnsiColor {
  pub fn to_rgb(&self) -> Color {
    match self {
      // TODO: Use theme here
      AnsiColor::Default => 0xffffff.into(),
      AnsiColor::Builtin(c) => c.to_rgb(),
      AnsiColor::BrightBuiltin(c) => c.to_rgb_bright(),
      AnsiColor::RGB(c) => *c,
    }
  }
}

impl BuiltinColor {
  fn to_rgb(&self) -> Color {
    match self {
      BuiltinColor::Black => Color::from_hex(0x303030),
      BuiltinColor::Red => Color::from_hex(0xe84f4f),
      BuiltinColor::Green => Color::from_hex(0xb7ce42),
      BuiltinColor::Yellow => Color::from_hex(0xfea63c),
      BuiltinColor::Blue => Color::from_hex(0x66aabb),
      BuiltinColor::Magenta => Color::from_hex(0xb7416e),
      BuiltinColor::Cyan => Color::from_hex(0x6d878d),
      BuiltinColor::White => Color::from_hex(0xdddddd),
    }
  }

  fn to_rgb_bright(&self) -> Color {
    match self {
      BuiltinColor::Black => Color::from_hex(0x666666),
      BuiltinColor::Red => Color::from_hex(0xd23d3d),
      BuiltinColor::Green => Color::from_hex(0xbde077),
      BuiltinColor::Yellow => Color::from_hex(0xffe863),
      BuiltinColor::Blue => Color::from_hex(0xaaccbb),
      BuiltinColor::Magenta => Color::from_hex(0xe16a98),
      BuiltinColor::Cyan => Color::from_hex(0x42717b),
      BuiltinColor::White => Color::from_hex(0xcccccc),
    }
  }
}

#[derive(Default)]
struct AnsiFlags(u16);

bitflags::bitflags! {
  impl AnsiFlags: u16 {
    const BOLD             = 0b00000000001;
    const DIM              = 0b00000000010;
    const ITALIC           = 0b00000000100;
    const UNDERLINE        = 0b00000001000;
    const DOUBLE_UNDERLINE = 0b00000010000;
    const SLOW_BLINK       = 0b00000100000;
    const RAPID_BLINK      = 0b00001000000;
    const SWAP_FG_BG       = 0b00010000000;
    const HIDE_TEXT        = 0b00100000000;
    const STRIKETHROUGH    = 0b01000000000;
    const FRAKTUR          = 0b10000000000;
  }
}

impl RenderCursor {
  pub fn new() -> Self {
    RenderCursor {
      screen:     vec![StringWithColors::from_string(" ".into())],
      cursor:     Pos { row: 0, col: 0 },
      flags:      AnsiFlags::default(),
      foreground: AnsiColor::Default,
      background: AnsiColor::Default,
    }
  }

  pub fn resize(&mut self, rows: u16, cols: u16) {
    self.screen.resize_with(rows as usize, || StringWithColors::new());
    for row in &mut self.screen {
      row.resize(cols as usize);
    }
  }

  fn draw_char(&mut self, c: char) {
    if self.cursor.row >= self.screen.len() as u64
      || self.cursor.col >= self.screen[self.cursor.row as usize].len() as u64
    {
      return;
    }
    self.screen[self.cursor.row as usize].replace_cell(
      self.cursor.col as usize,
      Cell {
        ch:    c,
        bold:  self.flags.contains(AnsiFlags::BOLD),
        color: self.foreground.to_rgb(),
      },
    );
  }

  fn set_row(&mut self, row: u16) {
    self.cursor.row = row.saturating_sub(1).into();
    if self.cursor.row > self.screen.len() as u64 - 1 {
      self.cursor.row = self.screen.len() as u64 - 1;
    }
  }
  fn set_col(&mut self, col: u16) {
    self.cursor.col = col.saturating_sub(1).into();
    if self.cursor.col > self.screen[self.cursor.row as usize].len() as u64 - 1 {
      self.cursor.col = self.screen[self.cursor.row as usize].len() as u64 - 1;
    }
  }

  fn row(&self) -> u16 { self.cursor.row as u16 + 1 }
  fn col(&self) -> u16 { self.cursor.col as u16 + 1 }

  fn change_color(&mut self, params: &vte::Params) {
    let mut iter = params.iter();
    while let Some(params) = iter.next() {
      match params {
        [] | [0] => {
          self.foreground = AnsiColor::Default;
          self.background = AnsiColor::Default;
          self.flags.set(AnsiFlags::all(), false);
        }
        [1] => self.flags.set(AnsiFlags::BOLD, true),
        [2] => self.flags.set(AnsiFlags::DIM, true),
        [3] => self.flags.set(AnsiFlags::ITALIC, true),
        [4] => self.flags.set(AnsiFlags::UNDERLINE, true),
        [5] => self.flags.set(AnsiFlags::SLOW_BLINK, true),
        [6] => self.flags.set(AnsiFlags::RAPID_BLINK, true),
        [7] => self.flags.set(AnsiFlags::SWAP_FG_BG, true),
        [8] => self.flags.set(AnsiFlags::HIDE_TEXT, true),
        [9] => self.flags.set(AnsiFlags::STRIKETHROUGH, true),

        [10] => {}      // TODO: default font (???)
        [11..=19] => {} // TODO: alternate font (???)

        [20] => self.flags.set(AnsiFlags::FRAKTUR, true),
        [21] => self.flags.set(AnsiFlags::DOUBLE_UNDERLINE, true),
        [22] => self.flags.set(AnsiFlags::BOLD | AnsiFlags::DIM, false),
        [23] => self.flags.set(AnsiFlags::ITALIC | AnsiFlags::FRAKTUR, false),
        [24] => self.flags.set(AnsiFlags::UNDERLINE | AnsiFlags::DOUBLE_UNDERLINE, false),
        [25] => self.flags.set(AnsiFlags::SLOW_BLINK | AnsiFlags::RAPID_BLINK, false),
        [26] => {} // TODO: proportional spacing (???)
        [27] => self.flags.set(AnsiFlags::SWAP_FG_BG, false),
        [28] => self.flags.set(AnsiFlags::HIDE_TEXT, false),
        [29] => self.flags.set(AnsiFlags::STRIKETHROUGH, false),

        [n @ (30..=37)] => {
          let color = parse_8_color(n - 30).unwrap();
          self.foreground = AnsiColor::Builtin(color);
        }
        [38] => {
          parse_extended_color(&mut iter).map(|c| self.foreground = c);
        }
        [38, rest @ ..] => {
          parse_extended_color_slice(rest).map(|c| self.foreground = c);
        }
        [39] => self.foreground = AnsiColor::Default,

        [n @ (40..=47)] => {
          let color = parse_8_color(n - 40).unwrap();
          self.background = AnsiColor::Builtin(color);
        }
        [48] => {
          parse_extended_color(&mut iter).map(|c| self.background = c);
        }
        [48, rest @ ..] => {
          parse_extended_color_slice(rest).map(|c| self.background = c);
        }
        [49] => self.background = AnsiColor::Default,

        [50] => {} // TODO: disable proportional spacing
        [51] => {} // TODO: framed
        [52] => {} // TODO: encircled
        [53] => {} // TODO: overlined
        [54] => {} // TODO: not framed, not encircled
        [55] => {} // TODO: not overlined

        [58] => {} // TODO: set underline color
        [59] => {} // TODO: default underline color

        [73] => {} // TODO: superscript
        [74] => {} // TODO: subscript
        [75] => {} // TODO: neither super or subscript

        [n @ (90..=97)] => {
          let color = parse_8_color(n - 90).unwrap();
          self.foreground = AnsiColor::BrightBuiltin(color);
        }

        [n @ (100..=107)] => {
          let color = parse_8_color(n - 100).unwrap();
          self.background = AnsiColor::BrightBuiltin(color);
        }

        _ => {}
      }
    }
  }
}

fn parse_extended_color<'a>(params: &mut impl Iterator<Item = &'a [u16]>) -> Option<AnsiColor> {
  let first = params.next()?;
  match first {
    [5] => parse_256_color(*params.next()?.get(0)?),
    [2] => {
      let r = *params.next()?.get(0)? as u8;
      let g = *params.next()?.get(0)? as u8;
      let b = *params.next()?.get(0)? as u8;
      Some(AnsiColor::RGB(Color { r, g, b }))
    }
    _ => None,
  }
}

fn parse_extended_color_slice(params: &[u16]) -> Option<AnsiColor> {
  match params {
    [5, n] => parse_256_color(*n),
    [2, r, g, b] => Some(AnsiColor::RGB(Color { r: *r as u8, g: *g as u8, b: *b as u8 })),
    _ => None,
  }
}

fn parse_256_color(n: u16) -> Option<AnsiColor> {
  match n as u8 {
    0..=7 => parse_8_color(n).map(AnsiColor::Builtin),
    8..=15 => parse_8_color(n - 8).map(AnsiColor::BrightBuiltin),
    16..=231 => {
      let n = n - 16;
      let r = n / 36;
      let g = (n % 36) / 6;
      let b = n % 6;
      Some(AnsiColor::RGB(Color {
        r: (r * 40 + 55) as u8,
        g: (g * 40 + 55) as u8,
        b: (b * 40 + 55) as u8,
      }))
    }
    232..=255 => {
      let n = n - 232;
      let c = (n * 10 + 8) as u8;
      Some(AnsiColor::RGB(Color { r: c, g: c, b: c }))
    }
  }
}

fn parse_8_color(n: u16) -> Option<BuiltinColor> {
  match n {
    0 => Some(BuiltinColor::Black),
    1 => Some(BuiltinColor::Red),
    2 => Some(BuiltinColor::Green),
    3 => Some(BuiltinColor::Yellow),
    4 => Some(BuiltinColor::Blue),
    5 => Some(BuiltinColor::Magenta),
    6 => Some(BuiltinColor::Cyan),
    7 => Some(BuiltinColor::White),
    _ => None,
  }
}

impl vte::Perform for RenderCursor {
  fn print(&mut self, c: char) {
    self.draw_char(c);
    self.set_col(self.col() + 1);
  }

  fn csi_dispatch(
    &mut self,
    params: &vte::Params,
    intermediates: &[u8],
    _ignore: bool,
    action: char,
  ) {
    let mut params_iter = params.iter();
    let mut next_param = || params_iter.next().and_then(|p| p.get(0).copied());

    match action {
      'A' => self.set_row(self.row().saturating_sub(next_param().unwrap_or(1))),
      'B' => self.set_row(self.row().saturating_add(next_param().unwrap_or(1))),
      'C' => self.set_col(self.col().saturating_add(next_param().unwrap_or(1))),
      'D' => self.set_col(self.col().saturating_sub(next_param().unwrap_or(1))),
      'E' => {
        self.set_row(self.row().saturating_add(next_param().unwrap_or(1)));
        self.set_col(1);
      }
      'F' => {
        self.set_row(self.row().saturating_sub(next_param().unwrap_or(1)));
        self.set_col(1);
      }
      'G' => self.set_col(next_param().unwrap_or(1)),
      'H' => {
        self.set_row(next_param().unwrap_or(1));
        self.set_col(next_param().unwrap_or(1));
      }
      'J' => match params_iter.next().unwrap_or(&[]) {
        // clear from cursor to end of screen
        [] | [0] => {
          self.screen[self.cursor.row as usize].clear_range(self.cursor.col as usize..);
          for row in self.screen.iter_mut().skip(self.cursor.row as usize + 1) {
            row.clear();
          }
        }
        // clear from cursor to beginning of screen
        [1] => {
          self.screen[self.cursor.row as usize].clear_range(0..self.cursor.col as usize);
          for row in &mut self.screen.iter_mut().take(self.cursor.row as usize) {
            row.clear();
          }
        }
        // clear entire screen, and home cursor
        [2 | 3] => {
          for row in &mut self.screen.iter_mut() {
            row.clear();
          }
          self.set_row(1);
          self.set_col(1);
          // TODO: 3 should clear scrollback buffer too.
        }
        _ => println!("unknown args to J: {params:?}"),
      },
      'K' => match params_iter.next().unwrap_or(&[]) {
        // clear from cursor to end of the line
        [] | [0] => self.screen[self.cursor.row as usize].clear_range(self.cursor.col as usize..),
        // clear from cursor to beginning of screen
        [1] => self.screen[self.cursor.row as usize].clear_range(0..self.cursor.col as usize),
        // clear entire line (cursor position does not change)
        [2] => self.screen[self.cursor.row as usize].clear(),
        _ => println!("unknown args to K: {params:?}"),
      },
      'h' => {
        match (intermediates, next_param()) {
          (b"?", Some(25)) => {
            // TODO: show cursor
          }
          (b"?", Some(1004)) => {
            // TODO: enable reporting focus. ESC [I and ESC [O should be sent
            // focus is gained/lost.
          }
          (b"?", Some(1049)) => {
            // TODO: enable alternate buffer.
          }
          (b"?", Some(2004)) => {
            // TODO: enable bracketed paste. this puts ESC [200~ before parses,
            // so that tools can treat it as a paste, not a command.
          }
          _ => println!("unknown args to h: {params:?}"),
        }
      }
      'l' => {
        match (intermediates, next_param()) {
          (b"?", Some(25)) => {
            // TODO: hide cursor
          }
          (b"?", Some(1004)) => {
            // TODO: disable reporting focus. ESC [I and ESC [O should be sent
            // focus is gained/lost.
          }
          (b"?", Some(1049)) => {
            // TODO: disable alternate buffer.
          }
          (b"?", Some(2004)) => {
            // TODO: disable bracketed paste. this puts ESC [200~ before parses,
            // so that tools can treat it as a paste, not a command.
          }
          _ => println!("unknown args to l: {params:?}"),
        }
      }
      's' => {
        // TODO: Save cursor position
        println!("todo: save cursor position");
      }
      'u' => {
        // TODO: Restore cursor position
        println!("todo: restore cursor position");
      }
      'm' => self.change_color(params),
      _ => println!("unknown action {action}"),
    }
  }

  fn execute(&mut self, byte: u8) {
    match byte {
      b'\n' => self.set_row(self.row() + 1),
      b'\r' => self.set_col(1),
      b'\x08' => self.set_col(self.col().saturating_sub(1)),
      _ => println!("unknown execute byte {byte} {:?}", char::from_u32(byte as u32)),
    }
  }
}

#[cfg(test)]
mod tests {
  use super::*;

  fn parse(cursor: &mut RenderCursor, str: &str) {
    let mut parser = vte::Parser::new();
    for byte in str.bytes() {
      parser.advance(cursor, byte);
    }
  }

  #[test]
  fn change_color() {
    let mut cursor = RenderCursor::new();
    parse(&mut cursor, "\x1b[35m");
    assert_eq!(cursor.foreground, AnsiColor::Builtin(BuiltinColor::Magenta));

    let mut cursor = RenderCursor::new();
    parse(&mut cursor, "\x1b[38;2;255;0;0m");
    assert_eq!(cursor.foreground, AnsiColor::RGB(Color { r: 255, g: 0, b: 0 }));

    let mut cursor = RenderCursor::new();
    parse(&mut cursor, "\x1b[48;2;255;0;0m");
    assert_eq!(cursor.background, AnsiColor::RGB(Color { r: 255, g: 0, b: 0 }));

    let mut cursor = RenderCursor::new();
    parse(&mut cursor, "\x1b[38:2:255:0:0m");
    assert_eq!(cursor.foreground, AnsiColor::RGB(Color { r: 255, g: 0, b: 0 }));

    let mut cursor = RenderCursor::new();
    parse(&mut cursor, "\x1b[48:2:255:0:0m");
    assert_eq!(cursor.background, AnsiColor::RGB(Color { r: 255, g: 0, b: 0 }));

    let mut cursor = RenderCursor::new();
    parse(&mut cursor, "\x1b[0;1m");
    assert!(cursor.flags.contains(AnsiFlags::BOLD));

    let mut cursor = RenderCursor::new();
    parse(&mut cursor, "\x1b[38;2;255;0;0;1m");
    assert_eq!(cursor.foreground, AnsiColor::RGB(Color { r: 255, g: 0, b: 0 }));
    assert!(cursor.flags.contains(AnsiFlags::BOLD));

    let mut cursor = RenderCursor::new();
    parse(&mut cursor, "\x1b[38:2:255:0:0;1m");
    assert_eq!(cursor.foreground, AnsiColor::RGB(Color { r: 255, g: 0, b: 0 }));
    assert!(cursor.flags.contains(AnsiFlags::BOLD));
  }
}
