use crate::{
  event::{watch, Event, Events, Token},
  input::{Key, KeyWithMods},
  tab::{editor::ScopedCtx, CursorMode},
};
use std::io::{Read, Write};

mod ansi;
mod pty;

use ansi::RenderCursor;
use pty::Pty;

pub struct Terminal {
  pty: Pty,

  outgoing: String,

  parser: vte::Parser,
  cursor: RenderCursor,
}

impl Terminal {
  pub fn new() -> Self {
    let pty = Pty::new().unwrap();
    Terminal {
      pty,
      outgoing: String::new(),
      parser: vte::Parser::new(),
      cursor: RenderCursor::new(),
    }
  }

  pub fn render(&mut self, ctx: &mut ScopedCtx) {
    self.cursor.resize(ctx.rows(), ctx.cols());

    for (i, line) in self.cursor.screen.iter().enumerate() {
      ctx.draw_str(i as u16, 0, line.clone());
    }

    ctx.draw_cursor_px(
      ctx.col_to_px(self.cursor.cursor.col as u16),
      ctx.row_to_px(self.cursor.cursor.row as u16),
      CursorMode::Block,
    );
  }

  pub fn register(&mut self, e: &mut Events) -> Token {
    e.register(&self.pty.file, watch::READ | watch::WRITE)
  }

  pub fn user_event(&mut self, e: Event) {
    if e.kind & watch::READ != 0 {
      self.flush_read();
    }
    if e.kind & watch::WRITE != 0 {
      self.flush_write();
    }
  }

  pub fn input(&mut self, key: KeyWithMods) {
    if key.mods.has_ctrl() {
      match key.key {
        Key::Char('c') => self.write("\x03"),
        Key::Char('d') => self.write("\x04"),
        Key::Char('z') => self.write("\x1a"),
        _ => {}
      }
    } else {
      match key.key {
        Key::Char(c) => {
          let mut tmp = [0u8; 4];
          self.write(c.encode_utf8(&mut tmp));
        }
        Key::Enter => self.write("\r"),
        Key::Backspace => self.write("\x7f"),
        _ => {}
      }
    }
  }

  fn flush_read(&mut self) {
    let mut buf = [0; 1024];
    loop {
      let n = match self.pty.file.read(&mut buf) {
        Ok(0) => panic!("pty closed"),
        Ok(n) => n,
        Err(e) if e.kind() == std::io::ErrorKind::WouldBlock => break,
        Err(e) => panic!("error reading from pty: {}", e),
      };
      for byte in &buf[..n] {
        self.parser.advance(&mut self.cursor, *byte);
      }
    }
  }

  fn flush_write(&mut self) {
    while !self.outgoing.is_empty() {
      let n = match self.pty.file.write(self.outgoing.as_bytes()) {
        Ok(0) => break,
        Ok(n) => n,
        Err(e) if e.kind() == std::io::ErrorKind::WouldBlock => break,
        Err(e) => panic!("error reading from pty: {}", e),
      };
      self.outgoing.drain(0..n);
    }
  }

  pub fn write(&mut self, buf: &str) {
    if buf.is_empty() {
      return;
    }

    if self.outgoing.is_empty() {
      // We aren't blocked yet, so write directly.

      let n = match self.pty.file.write(self.outgoing.as_bytes()) {
        Ok(n) => n,
        Err(e) if e.kind() == std::io::ErrorKind::WouldBlock => 0,
        Err(e) => panic!("error reading from pty: {}", e),
      };

      if n < buf.len() {
        self.outgoing.push_str(&buf[n..]);
      }
    } else {
      self.outgoing.push_str(buf);
    }
  }
}
