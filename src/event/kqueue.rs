use super::{Event, Token};
use std::{mem::MaybeUninit, ptr};

pub use KQueue as Impl;

pub struct KQueue {
  kq:         i32,
  next_token: u64,
}

impl super::EventImpl for KQueue {
  fn new() -> Self {
    unsafe {
      let kq = libc::kqueue();
      if kq == -1 {
        panic!("Failed to create kqueue descriptor");
      }
      KQueue { kq, next_token: 0 }
    }
  }

  fn register(&mut self, fd: i32, watch: u8) -> Token {
    let token = self.next_token;
    self.next_token += 1;

    unsafe {
      let mut events = [MaybeUninit::<libc::kevent>::uninit(); 2];
      let mut num_events = 0;

      if watch & super::watch::READ != 0 {
        events[num_events].write(libc::kevent {
          ident:  fd as usize,
          filter: libc::EVFILT_READ,
          flags:  libc::EV_CLEAR | libc::EV_ADD,
          fflags: 0,
          data:   0,
          // Arbitrary user data, typed as a pointer. We'll just store the `token` here directly.
          udata:  token as *mut _,
        });
        num_events += 1;
      }
      if watch & super::watch::WRITE != 0 {
        events[num_events].write(libc::kevent {
          ident:  fd as usize,
          filter: libc::EVFILT_WRITE,
          flags:  libc::EV_CLEAR | libc::EV_ADD,
          fflags: 0,
          data:   0,
          // Arbitrary user data, typed as a pointer. We'll just store the `token` here directly.
          udata:  token as *mut _,
        });
        num_events += 1;
      }

      if num_events > 0 {
        let ret = libc::kevent(
          self.kq,
          events.as_ptr() as *const _,
          num_events as i32,
          ptr::null_mut(),
          0,
          ptr::null_mut(),
        );
        if ret != 0 {
          panic!("Failed to register fd to kqueue");
        }
      }
    }

    Token(token)
  }

  fn poll<const N: usize>(&mut self, events: &mut [Event; N]) -> usize {
    unsafe {
      let mut kq_events = [libc::kevent {
        ident:  0,
        filter: 0,
        flags:  0,
        fflags: 0,
        data:   0,
        udata:  std::ptr::null_mut(),
      }; N];
      let count = libc::kevent(
        self.kq,
        ptr::null_mut(),
        0,
        kq_events.as_mut_ptr(),
        N as i32,
        ptr::null_mut(),
      );

      for i in 0..count {
        let event = &mut events[i as usize];
        let kq_event = kq_events[i as usize];

        event.token = Token(kq_event.udata as u64);

        if kq_event.flags & libc::EV_ERROR != 0 {
          panic!("kq returned an error");
        }

        event.kind = 0;
        info!("filter: {:#x?}", kq_event.filter);
        info!("EVFILT_READ: {:#x?}", libc::EVFILT_READ);
        info!("EVFILT_WRITE: {:#x?}", libc::EVFILT_WRITE);
        if kq_event.filter == libc::EVFILT_READ {
          info!("got read event");
          event.kind |= super::watch::READ;
        }
        if kq_event.filter == libc::EVFILT_WRITE {
          info!("got write event");
          event.kind |= super::watch::WRITE;
        }
      }

      count as usize
    }
  }
}

impl Drop for KQueue {
  fn drop(&mut self) {
    unsafe {
      libc::close(self.kq);
    }
  }
}
