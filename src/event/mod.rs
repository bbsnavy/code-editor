use std::os::fd::AsRawFd;

#[cfg(target_os = "linux")]
mod epoll;
#[cfg(target_os = "linux")]
use self::epoll as imp;

#[cfg(target_os = "macos")]
mod kqueue;
#[cfg(target_os = "macos")]
use self::kqueue as imp;

#[cfg(not(any(target_os = "linux", target_os = "macos")))]
mod imp {
  compile_error!("target_os is not supported");

  pub struct Dummy {}
  impl super::EventImpl for Dummy {
    fn new() -> Self { Dummy {} }
    fn register(&mut self, fd: i32, watch: u8) -> super::Token { super::Token(0) }
    fn poll<const N: usize>(&mut self, events: &mut [super::Event; N]) -> usize { 0 }
  }

  pub use Dummy as Impl;
}

trait EventImpl {
  fn new() -> Self;
  fn register(&mut self, fd: i32, watch: u8) -> Token;
  fn poll<const N: usize>(&mut self, events: &mut [Event; N]) -> usize;
}

pub struct Events {
  imp: imp::Impl,
}

pub mod watch {
  pub const READ: u8 = 0x01;
  pub const WRITE: u8 = 0x02;
}

#[derive(Debug, Clone, Copy, PartialEq, Eq, Hash)]
pub struct Token(pub u64);

#[derive(Debug, Clone)]
pub struct Event {
  pub token: Token,
  pub kind:  u8,
}

pub const NONE: Event = Event { token: Token(0), kind: 0 };

impl Events {
  pub fn new() -> Self { Events { imp: imp::Impl::new() } }

  pub fn register(&mut self, fd: &impl AsRawFd, watch: u8) -> Token {
    self.imp.register(fd.as_raw_fd(), watch)
  }
  pub fn poll<const N: usize>(&mut self, events: &mut [Event; N]) -> usize { self.imp.poll(events) }
}
