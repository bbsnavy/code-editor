use super::{Event, Token};

pub use Epoll as Impl;

pub struct Epoll {
  fd:         i32,
  next_token: u64,
}

impl super::EventImpl for Epoll {
  fn new() -> Self {
    unsafe {
      let fd = libc::epoll_create1(0);
      if fd == -1 {
        panic!("Failed to create epoll descriptor");
      }
      Epoll { fd, next_token: 0 }
    }
  }

  fn register(&mut self, fd: i32, watch: u8) -> Token {
    let token = self.next_token;
    self.next_token += 1;

    unsafe {
      let mut events = libc::EPOLLET;
      if watch & super::watch::READ != 0 {
        events |= libc::EPOLLIN;
      }
      if watch & super::watch::WRITE != 0 {
        events |= libc::EPOLLOUT;
      }
      let mut event = libc::epoll_event { events: events as u32, u64: token };
      let ret = libc::epoll_ctl(self.fd, libc::EPOLL_CTL_ADD, fd, &mut event);
      if ret != 0 {
        panic!("Failed to register fd to epoll");
      }
    }

    Token(token)
  }

  fn poll<const N: usize>(&mut self, events: &mut [Event; N]) -> usize {
    unsafe {
      let mut epoll_events = [libc::epoll_event { events: 0, u64: 0 }; N];
      // timeout is in milliseconds
      let count = libc::epoll_wait(self.fd, epoll_events.as_mut_ptr(), N as i32, 30000);

      for i in 0..count {
        let event = &mut events[i as usize];
        let epoll_event = epoll_events[i as usize];

        event.token = Token(epoll_event.u64);

        event.kind = 0;
        if epoll_event.events & libc::EPOLLIN as u32 != 0 {
          event.kind |= super::watch::READ;
        }
        if epoll_event.events & libc::EPOLLOUT as u32 != 0 {
          event.kind |= super::watch::WRITE;
        }
      }

      count as usize
    }
  }
}

impl Drop for Epoll {
  fn drop(&mut self) {
    unsafe {
      libc::close(self.fd);
    }
  }
}
