use std::fmt;

macro_rules! icons {
  ($( $name:ident = $path:expr; )*) => {
    #[allow(unused)]
    #[derive(Clone, Copy, Hash, PartialEq, Eq)]
    pub enum Icon {
      $(
        $name,
      )*
    }

    impl Icon {
      pub fn name(&self) -> &'static str {
        match self {
          $(
            Icon::$name => $path,
          )*
        }
      }

      pub fn svg(&self) -> &'static str {
        match self {
          $(
            Icon::$name => include_str!(concat!("../../assets/icons/", $path, ".svg")),
          )*
        }
      }
    }
  }
}

icons! {
  CarrotUp = "carrot-up";
  CarrotDown = "carrot-down";
  CarrotLeft = "carrot-left";
  CarrotRight = "carrot-right";
  CircleX = "circle-x";
  Warning = "warning";
}

impl fmt::Debug for Icon {
  fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result { write!(f, "{}", self.name()) }
}
