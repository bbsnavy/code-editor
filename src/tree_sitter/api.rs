type TSStateId = u16;

type TSSymbol = u16;
type TSFieldId = u16;

#[derive(Clone, Copy, Debug)]
#[repr(C)]
pub struct TSFieldMapEntry {
  pub field_id:    TSFieldId,
  pub child_index: u8,
  pub inherited:   bool,
}

#[derive(Clone, Copy, Debug)]
#[repr(C)]
pub struct TSFieldMapSlice {
  pub index:  u16,
  pub length: u16,
}

#[derive(Clone, Copy, Debug)]
#[repr(C)]
pub struct TSSymbolMetadata {
  pub visible:   bool,
  pub named:     bool,
  pub supertype: bool,
}

#[derive(Clone, Copy, Debug)]
#[repr(C)]
pub struct TSLexer {
  pub lookahead:     i32,
  pub result_symbol: TSSymbol,
  /*
  pub void (*advance)(TSLexer *, bool),
  pub void (*mark_end)(TSLexer *),
  pub uint32_t (*get_column)(TSLexer *),
  pub bool (*is_at_included_range_start)(const TSLexer *),
  pub bool (*eof)(const TSLexer *),
  */
}

#[derive(Clone, Copy, Debug)]
#[repr(C)]
pub struct TSParseShiftAction {
  pub _type:      u8,
  pub state:      TSStateId,
  pub extra:      bool,
  pub repetition: bool,
}

#[derive(Clone, Copy, Debug)]
#[repr(C)]
pub struct TSParseReduceAction {
  pub _type:              u8,
  pub child_count:        u8,
  pub symbol:             TSSymbol,
  pub dynamic_precedence: i16,
  pub production_id:      u16,
}

#[derive(Clone, Copy)]
#[repr(C)]
pub union TSParseAction {
  pub shift:  TSParseShiftAction,
  pub reduce: TSParseReduceAction,
  pub _type:  u8,
}

#[derive(Clone, Copy, Debug)]
#[repr(C)]
pub struct TSLexMode {
  pub lex_state:          u16,
  pub external_lex_state: u16,
}

#[derive(Clone, Copy, Debug)]
#[repr(C)]
pub struct TSParseEntry {
  pub count:    u8,
  pub reusable: bool,
}

#[derive(Clone, Copy)]
#[repr(C)]
pub union TSParseActionEntry {
  pub action: TSParseAction,
  pub entry:  TSParseEntry,
}

#[derive(Clone, Copy, Debug)]
#[repr(C)]
pub struct TSLanguage {
  pub version:                   u32,
  pub symbol_count:              u32,
  pub alias_count:               u32,
  pub token_count:               u32,
  pub external_token_count:      u32,
  pub state_count:               u32,
  pub large_state_count:         u32,
  pub production_id_count:       u32,
  pub field_count:               u32,
  pub max_alias_sequence_length: u16,

  pub parse_table:           *const u16,
  pub small_parse_table:     *const u16,
  pub small_parse_table_map: *const u32,
  pub parse_actions:         *const TSParseActionEntry,
  pub symbol_names:          *const *const libc::c_char,
  pub field_names:           *const *const libc::c_char,
  pub field_map_slices:      *const TSFieldMapSlice,
  pub field_map_entries:     *const TSFieldMapEntry,
  pub symbol_metadata:       *const TSSymbolMetadata,
  pub public_symbol_map:     *const TSSymbol,
  pub alias_map:             *const u16,
  pub alias_sequences:       *const TSSymbol,
  pub lex_modes:             *const TSLexMode,

  pub lex_fn:         unsafe extern "C" fn(*const TSLexer, TSStateId) -> bool,
  pub keyword_lex_fn: unsafe extern "C" fn(*const TSLexer, TSStateId) -> bool,

  pub keyword_capture_token: TSSymbol,

  pub external_scanner:  TSExternalScanner,
  pub primary_state_ids: *const TSStateId,
}

#[derive(Clone, Copy, Debug)]
#[repr(C)]
pub struct TSExternalScanner {
  pub states:     *const bool,
  pub symbol_map: *const TSSymbol,

  pub create:  unsafe extern "C" fn() -> *const libc::c_void,
  pub destroy: unsafe extern "C" fn(),

  pub scan: unsafe extern "C" fn(*const libc::c_void, *const TSLexer, *const bool) -> bool,

  pub serialize:   unsafe extern "C" fn(*const libc::c_void, *const libc::c_char) -> libc::c_uint,
  pub deserialize: unsafe extern "C" fn(*const libc::c_void, *const libc::c_char, libc::c_uint),
}

#[derive(Clone, Copy, Debug)]
#[repr(C)]
pub struct TSParser {
  _unused: [u8; 0],
}

#[derive(Clone, Copy, Debug)]
#[repr(C)]
pub struct TSQuery {
  _unused: [u8; 0],
}

#[derive(Clone, Copy, Debug)]
#[repr(C)]
pub struct TSQueryCursor {
  _unused: [u8; 0],
}

#[derive(Clone, Copy, Debug)]
#[repr(C)]
pub struct TSQueryMatch {
  pub id:            u32,
  pub pattern_index: u16,
  pub capture_count: u16,
  pub captures:      *const TSQueryCapture,
}

#[derive(Clone, Copy, Debug)]
#[repr(C)]
pub struct TSQueryCapture {
  pub node:  TSNode,
  pub index: u32,
}

#[derive(Clone, Copy, Debug)]
#[repr(C)]
pub struct TSTree {
  pub root: u64,
  pub lang: *const TSLanguage,
}

#[derive(Clone, Copy, Debug)]
#[repr(C)]
pub struct TSNode {
  pub context: [u32; 4],
  pub id:      *const libc::c_void,
  pub tree:    *const TSTree,
}

#[derive(Clone, Copy, Debug)]
#[repr(C)]
pub struct TSTreeCursor {
  pub tree:    *const TSTree,
  pub id:      *const libc::c_void,
  pub context: [u32; 2],
}

#[derive(Clone, Copy, Debug)]
#[repr(C)]
pub enum TSQueryError {
  None = 0,
  Syntax,
  NodeType,
  Field,
  Capture,
  Structure,
  Language,
}

#[derive(Clone, Copy)]
#[repr(C)]
pub enum TSLogType {
  TSLogTypeParse,
  TSLogTypeLex,
}

#[derive(Clone, Copy)]
#[repr(C)]
pub struct TSLogger {
  pub payload: *mut libc::c_void,
  pub log:
    unsafe extern "C" fn(payload: *mut libc::c_void, ty: TSLogType, buf: *const libc::c_char),
}

extern "C" {
  pub fn ts_parser_new() -> *mut TSParser;
  pub fn ts_parser_delete(parser: *mut TSParser);
  pub fn ts_parser_set_language(self_: *mut TSParser, language: *const TSLanguage) -> bool;
  pub fn ts_parser_parse_string(
    self_: *mut TSParser,
    old_tree: *const TSTree,
    string: *const libc::c_char,
    length: u32,
  ) -> *mut TSTree;
  pub fn ts_parser_set_logger(self_: *mut TSParser, logger: TSLogger);

  pub fn ts_tree_root_node(foo: *const TSTree) -> TSNode;

  pub fn ts_node_type(self_: TSNode) -> *const libc::c_char;
  pub fn ts_node_start_byte(self_: TSNode) -> u32;
  pub fn ts_node_end_byte(self_: TSNode) -> u32;
  pub fn ts_node_string(self_: TSNode) -> *mut libc::c_char;
  pub fn ts_node_is_null(self_: TSNode) -> bool;
  pub fn ts_node_is_named(self_: TSNode) -> bool;
  pub fn ts_node_is_missing(self_: TSNode) -> bool;
  pub fn ts_node_is_extra(self_: TSNode) -> bool;
  pub fn ts_node_parent(self_: TSNode) -> TSNode;
  pub fn ts_node_child(self_: TSNode, index: u32) -> TSNode;
  pub fn ts_node_child_count(self_: TSNode) -> u32;

  pub fn ts_tree_cursor_new(node: TSNode) -> TSTreeCursor;
  pub fn ts_tree_cursor_copy(self_: *const TSTreeCursor) -> TSTreeCursor;
  pub fn ts_tree_cursor_delete(self_: *mut TSTreeCursor);
  pub fn ts_tree_cursor_current_node(self_: *const TSTreeCursor) -> TSNode;
  pub fn ts_tree_cursor_current_field_name(self_: *const TSTreeCursor) -> *const libc::c_char;
  pub fn ts_tree_cursor_goto_parent(self_: *mut TSTreeCursor) -> bool;
  pub fn ts_tree_cursor_goto_next_sibling(self_: *mut TSTreeCursor) -> bool;
  pub fn ts_tree_cursor_goto_previous_sibling(self_: *mut TSTreeCursor) -> bool;
  pub fn ts_tree_cursor_goto_first_child(self_: *mut TSTreeCursor) -> bool;
  pub fn ts_tree_cursor_goto_last_child(self_: *mut TSTreeCursor) -> bool;
  pub fn ts_tree_cursor_current_depth(self_: *const TSTreeCursor) -> u32;
  pub fn ts_tree_cursor_goto_first_child_for_byte(self_: *mut TSTreeCursor, goal: u32) -> i64;

  pub fn ts_query_new(
    lang: *const TSLanguage,
    source: *const libc::c_char,
    source_len: u32,
    error_offset: *mut u32,
    error_type: *mut TSQueryError,
  ) -> *mut TSQuery;
  pub fn ts_query_delete(self_: *mut TSQuery);
  pub fn ts_query_capture_name_for_id(
    self_: *const TSQuery,
    index: u32,
    length: *mut u32,
  ) -> *const libc::c_char;

  pub fn ts_query_cursor_new() -> *mut TSQueryCursor;
  pub fn ts_query_cursor_delete(self_: *mut TSQueryCursor);
  pub fn ts_query_cursor_set_byte_range(self_: *mut TSQueryCursor, start: u32, end: u32);
  pub fn ts_query_cursor_exec(self_: *mut TSQueryCursor, query: *const TSQuery, node: TSNode);
  pub fn ts_query_cursor_next_match(self_: *mut TSQueryCursor, match_: *mut TSQueryMatch) -> bool;
  pub fn ts_query_cursor_next_capture(
    self_: *mut TSQueryCursor,
    match_: *mut TSQueryMatch,
    capture_index: *mut u32,
  ) -> bool;
}
