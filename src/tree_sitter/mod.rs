// Allow dead code in here, as a lot of these functions are just wrappers around
// the C library, and I'd rather not remove them if I'm not using them.
#![allow(dead_code)]

use core::fmt;
use std::{ffi::CStr, marker::PhantomData, mem::MaybeUninit, ptr};

mod api;
mod library;

#[cfg(test)]
mod tests;

pub use library::Library;

const CURRENT_VERSION: u32 = 14;

pub struct Parser {
  ptr: *mut api::TSParser,
}

unsafe extern "C" fn log_fn(_: *mut libc::c_void, ty: api::TSLogType, buf: *const libc::c_char) {
  let str = CStr::from_ptr(buf);
  info!(
    "[{}]: {}",
    match ty {
      api::TSLogType::TSLogTypeLex => "lex",
      api::TSLogType::TSLogTypeParse => "parse",
    },
    str.to_string_lossy()
  );
}

impl Parser {
  pub fn new() -> Self {
    unsafe {
      let ptr = api::ts_parser_new();

      // TODO: Control this with a compile time flag or something.
      #[cfg(test)]
      api::ts_parser_set_logger(ptr, api::TSLogger { payload: ptr::null_mut(), log: log_fn });

      Parser { ptr }
    }
  }

  pub fn set_language(&mut self, lang: &Language) {
    unsafe {
      api::ts_parser_set_language(self.ptr, lang.ptr);
    }
  }

  pub fn parse_str(&mut self, tree: Option<&Tree>, src: &str) -> Tree {
    unsafe {
      let ptr = api::ts_parser_parse_string(
        self.ptr,
        match &tree {
          Some(t) => t.ptr,
          None => ptr::null_mut(),
        },
        src.as_ptr() as *const i8,
        src.len() as u32,
      );
      Tree::from_ptr(ptr)
    }
  }
}

impl Drop for Parser {
  fn drop(&mut self) {
    unsafe {
      api::ts_parser_delete(self.ptr);
    }
  }
}

pub struct Tree {
  ptr: *const api::TSTree,
}

impl Tree {
  unsafe fn from_ptr(ptr: *const api::TSTree) -> Self { Tree { ptr } }

  pub fn root_node(&self) -> Node<'_> {
    unsafe { Node::from_ptr(api::ts_tree_root_node(self.ptr)).expect("root node is null") }
  }
}

pub struct Query<'t> {
  src:      String,
  ptr:      *mut api::TSQuery,
  _phantom: PhantomData<&'t Tree>,
}

impl Drop for Query<'_> {
  fn drop(&mut self) {
    unsafe {
      api::ts_query_delete(self.ptr);
    }
  }
}

impl<'t> Query<'t> {
  pub fn new(lang: &Language, src: String) -> Self {
    unsafe {
      let mut error_offset: u32 = 0;
      let mut error_type: MaybeUninit<api::TSQueryError> = MaybeUninit::uninit();
      let ptr = api::ts_query_new(
        lang.ptr,
        src.as_ptr() as *const i8,
        src.len() as u32,
        &mut error_offset,
        error_type.as_mut_ptr(),
      );
      if ptr.is_null() {
        let ty = error_type.assume_init();
        panic!("error at offset {error_offset}: {ty:?}");
      }
      Query { src, ptr, _phantom: PhantomData }
    }
  }
}

pub struct QueryCursor<'t, 'q> {
  query: &'q Query<'t>,
  ptr:   *mut api::TSQueryCursor,
}

impl Drop for QueryCursor<'_, '_> {
  fn drop(&mut self) {
    unsafe {
      api::ts_query_cursor_delete(self.ptr);
    }
  }
}

impl<'t, 'q> QueryCursor<'t, 'q> {
  pub fn new(query: &'q Query<'t>) -> Self {
    unsafe { QueryCursor { query, ptr: api::ts_query_cursor_new() } }
  }

  pub fn set_range(&mut self, start: u32, end: u32) {
    unsafe {
      api::ts_query_cursor_set_byte_range(self.ptr, start, end);
    }
  }

  pub fn exec(&mut self, node: Node) {
    unsafe {
      api::ts_query_cursor_exec(self.ptr, self.query.ptr, node.node);
    }
  }

  pub fn next_match(&mut self) -> Option<QueryMatch<'t, 'q>> {
    unsafe {
      let mut m: MaybeUninit<api::TSQueryMatch> = MaybeUninit::uninit();
      if api::ts_query_cursor_next_match(self.ptr, m.as_mut_ptr()) {
        Some(QueryMatch { query: self.query, m: m.assume_init() })
      } else {
        None
      }
    }
  }

  pub fn next_capture(&mut self) -> Option<(QueryMatch, u32)> {
    unsafe {
      let mut m: MaybeUninit<api::TSQueryMatch> = MaybeUninit::uninit();
      let mut capture_index: u32 = 0;
      if api::ts_query_cursor_next_capture(self.ptr, m.as_mut_ptr(), &mut capture_index) {
        Some((QueryMatch { query: self.query, m: m.assume_init() }, capture_index))
      } else {
        None
      }
    }
  }
}

pub struct QueryMatch<'t, 'q> {
  query: &'q Query<'t>,
  m:     api::TSQueryMatch,
}

impl<'t, 'q> QueryMatch<'t, 'q> {
  pub fn id(&self) -> u32 { self.m.id }
  pub fn pattern_index(&self) -> u16 { self.m.pattern_index }
  pub fn len_captures(&self) -> u16 { self.m.capture_count }

  pub fn captures<'a>(&'a self) -> CaptureIter<'t, 'q, 'a> { CaptureIter { m: self, index: 0 } }
}

pub struct QueryCapture<'t, 'q> {
  query: &'q Query<'t>,
  cap:   api::TSQueryCapture,
}

impl<'t, 'q> QueryCapture<'t, 'q> {
  /// Returns the captured node.
  pub fn node(&self) -> Node<'t> {
    unsafe { Node::from_ptr(self.cap.node) }.expect("capture had no node")
  }

  /// Index into the query this capture is for.
  fn index(&self) -> u32 { self.cap.index }

  /// Returns the name of this capture.
  pub fn name(&self) -> &'q str {
    unsafe {
      let mut length: u32 = 0;
      let ptr = api::ts_query_capture_name_for_id(self.query.ptr, self.index(), &mut length);
      std::str::from_utf8(std::slice::from_raw_parts(ptr as _, length as usize)).unwrap()
    }
  }
}

#[derive(Clone, Copy)]
pub struct Node<'t> {
  node:     api::TSNode,
  _phantom: PhantomData<&'t api::TSTree>,
}

impl<'t> Node<'t> {
  // Wraps a node, and returns a Node without checking if `node` is null.
  unsafe fn from_ptr_unchecked(node: api::TSNode) -> Self { Node { node, _phantom: PhantomData } }
  // Wraps a node, and returns None if the node is null.
  unsafe fn from_ptr(node: api::TSNode) -> Option<Self> {
    if api::ts_node_is_null(node) {
      None
    } else {
      Some(Node::from_ptr_unchecked(node))
    }
  }

  pub fn ty(&self) -> &'static str {
    unsafe {
      let ptr = api::ts_node_type(self.node);
      CStr::from_ptr(ptr).to_str().unwrap()
    }
  }

  pub fn start(&self) -> u32 { unsafe { api::ts_node_start_byte(self.node) } }
  pub fn end(&self) -> u32 { unsafe { api::ts_node_end_byte(self.node) } }

  pub fn is_named(&self) -> bool { unsafe { api::ts_node_is_named(self.node) } }
  pub fn is_missing(&self) -> bool { unsafe { api::ts_node_is_missing(self.node) } }
  pub fn is_extra(&self) -> bool { unsafe { api::ts_node_is_extra(self.node) } }

  pub fn parent(&self) -> Option<Node<'t>> {
    unsafe { Node::from_ptr(api::ts_node_parent(self.node)) }
  }

  pub unsafe fn child_unchecked(&self, index: u32) -> Node<'t> {
    unsafe { Node::from_ptr_unchecked(api::ts_node_child(self.node, index)) }
  }
  pub fn child(&self, index: u32) -> Option<Node<'t>> {
    unsafe { Node::from_ptr(api::ts_node_child(self.node, index)) }
  }
  pub fn len_children(&self) -> u32 { unsafe { api::ts_node_child_count(self.node) } }

  pub fn cursor(&self) -> TreeCursor<'t> { TreeCursor::new(*self) }

  pub fn children(&self) -> SiblingIter<'t> {
    let mut cursor = self.cursor();
    if !cursor.goto_first_child() {
      return SiblingIter::empty();
    }
    cursor.into_siblings()
  }
}

pub struct TreeCursor<'a> {
  cursor:   api::TSTreeCursor,
  _phantom: PhantomData<Node<'a>>,
}

impl<'a> TreeCursor<'a> {
  pub fn new(node: Node<'a>) -> Self {
    TreeCursor { cursor: unsafe { api::ts_tree_cursor_new(node.node) }, _phantom: PhantomData }
  }
}

impl Drop for TreeCursor<'_> {
  fn drop(&mut self) { unsafe { api::ts_tree_cursor_delete(&mut self.cursor) } }
}

impl<'t> TreeCursor<'t> {
  pub fn current(&self) -> Node<'t> {
    unsafe { Node::from_ptr_unchecked(api::ts_tree_cursor_current_node(&self.cursor)) }
  }

  /// Returns `true` if successful, and `false` if there was no parent.
  pub fn goto_parent(&mut self) -> bool {
    unsafe { api::ts_tree_cursor_goto_parent(&mut self.cursor) }
  }

  /// Returns `true` if successful, and `false` if there was no next sibling.
  pub fn goto_next_sibling(&mut self) -> bool {
    unsafe { api::ts_tree_cursor_goto_next_sibling(&mut self.cursor) }
  }

  /// Returns `true` if successful, and `false` if there was no previous
  /// sibling.
  pub fn goto_prev_sibling(&mut self) -> bool {
    unsafe { api::ts_tree_cursor_goto_previous_sibling(&mut self.cursor) }
  }

  /// Returns `true` if successful, and `false` if there was no children.
  pub fn goto_first_child(&mut self) -> bool {
    unsafe { api::ts_tree_cursor_goto_first_child(&mut self.cursor) }
  }

  /// Returns `true` if successful, and `false` if there was no children.
  pub fn goto_last_child(&mut self) -> bool {
    unsafe { api::ts_tree_cursor_goto_last_child(&mut self.cursor) }
  }

  pub fn field_name(&self) -> Option<&'static str> {
    unsafe {
      let ptr = api::ts_tree_cursor_current_field_name(&self.cursor);

      if ptr.is_null() {
        None
      } else {
        let str = CStr::from_ptr(ptr).to_str().unwrap();
        Some(str)
      }
    }
  }

  pub fn into_siblings(self) -> SiblingIter<'t> {
    SiblingIter { is_first: true, cursor: Some(self) }
  }
}

// Iterators

pub struct SiblingIter<'t> {
  is_first: bool,
  cursor:   Option<TreeCursor<'t>>,
}

impl SiblingIter<'_> {
  pub fn empty() -> Self { SiblingIter { is_first: true, cursor: None } }
}

impl<'t> Iterator for SiblingIter<'t> {
  type Item = Node<'t>;

  fn next(&mut self) -> Option<Self::Item> {
    self.cursor.as_mut().and_then(|c| {
      if self.is_first {
        self.is_first = false;
        Some(c.current())
      } else {
        if !c.goto_next_sibling() {
          return None;
        }
        Some(c.current())
      }
    })
  }
}

pub struct CaptureIter<'t, 'q, 'a> {
  m:     &'a QueryMatch<'t, 'q>,
  index: u16,
}

impl<'t, 'q, 'a> Iterator for CaptureIter<'t, 'q, 'a> {
  type Item = QueryCapture<'t, 'q>;

  fn size_hint(&self) -> (usize, Option<usize>) {
    (self.m.len_captures() as usize, Some(self.m.len_captures() as usize))
  }

  fn next(&mut self) -> Option<Self::Item> {
    if self.index >= self.m.len_captures() {
      return None;
    }
    unsafe {
      let cap = *self.m.m.captures.offset(self.index as isize);
      self.index += 1;
      Some(QueryCapture { query: self.m.query, cap })
    }
  }
}

impl fmt::Debug for Node<'_> {
  fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
    unsafe {
      let ptr = api::ts_node_string(self.node);
      let str = CStr::from_ptr(ptr).to_str().unwrap();
      let res = f.write_str(str);
      libc::free(ptr as *mut _);
      res
    }
  }
}

pub struct Language {
  // Doesn't need a drop handler because this references static data from the dynamic library.
  ptr: *const api::TSLanguage,
}

impl Language {
  pub fn from_lib(lib: &Library) -> Self {
    let lang = lib.sym::<unsafe extern "C" fn() -> *const api::TSLanguage>("tree_sitter_rust");
    let lang = unsafe { &*(lang.value)() };
    if lang.version != CURRENT_VERSION {
      panic!("invalid version! expected {}, got {}", CURRENT_VERSION, lang.version);
    }

    Language { ptr: lang }
  }
}
