use super::{Language, Library, Parser, Query};

#[test]
fn parse_string() {
  crate::setup_logger();

  let doc = "foo.bar";

  // build with
  // ```
  // mkdir build
  // gcc -c src/parser.c -o build/parser.o
  // gcc -c src/scanner.c  -o build/scanner.o
  // gcc -shared build/parser.o build/scanner.o -o build/libgrammar.so
  // ```
  let path = crate::data_dir().join("tree-sitter/tree-sitter-rust");

  let so_path = if cfg!(target_os = "macos") {
    path.join("build/libgrammar.dylib")
  } else {
    path.join("build/libgrammar.so")
  };

  let lib = Library::new(so_path.to_str().unwrap());

  let lang = Language::from_lib(&lib);

  let mut parser = Parser::new();
  parser.set_language(&lang);
  let tree = parser.parse_str(None, doc);
  let root = tree.root_node();

  dbg!(root);
}

#[test]
fn query() {
  let path = crate::data_dir().join("tree-sitter/tree-sitter-rust");

  let so_path = if cfg!(target_os = "macos") {
    path.join("build/libgrammar.dylib")
  } else {
    path.join("build/libgrammar.so")
  };

  let lib = Library::new(so_path.to_str().unwrap());

  let lang = Language::from_lib(&lib);

  let mut parser = Parser::new();
  parser.set_language(&lang);

  let query_src = std::fs::read_to_string(path.join("queries/highlights.scm")).unwrap();

  let query = Query::new(&lang, query_src);
}
