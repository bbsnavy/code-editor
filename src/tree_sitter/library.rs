//! Loads a .so file. This wraps `dlopen`, `dlsym`, and `dlclose` in a safe(ish)
//! manner.

use libc::c_void;
use std::{
  ffi::{CStr, CString},
  marker::PhantomData,
};

pub struct Library {
  handle: *mut c_void,
}

pub struct Symbol<'a, T> {
  _phantom:  PhantomData<&'a Library>,
  pub value: T,
}

impl Library {
  pub fn new(path: &str) -> Library {
    let str = CString::new(path).unwrap();

    unsafe {
      let handle = libc::dlopen(str.as_ptr(), libc::RTLD_NOW);
      if handle.is_null() {
        panic!("couldn't load library");
      }
      Library { handle }
    }
  }

  fn check_error(&self) {
    unsafe {
      let error = libc::dlerror();
      if !error.is_null() {
        let msg = CStr::from_ptr(error);
        println!("error: {}", msg.to_str().unwrap());
        panic!("error from dlerror");
      }
    }
  }

  pub fn sym<'a, T>(&'a self, symbol: &str) -> Symbol<'a, T> {
    let str = CString::new(symbol).unwrap();

    unsafe {
      let ptr = libc::dlsym(self.handle, str.as_ptr());
      self.check_error();
      Symbol { _phantom: PhantomData, value: std::mem::transmute_copy::<_, T>(&ptr) }
    }
  }
}

impl Drop for Library {
  fn drop(&mut self) {
    unsafe {
      let code = libc::dlclose(self.handle);
      if code != 0 {
        panic!("couldn't unload library: {code}");
      }
    }
  }
}
