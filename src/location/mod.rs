use std::path::{Path, PathBuf};

use lsp_types::Url;

#[derive(Debug, Clone, PartialEq, Eq, Hash)]
pub enum FileLocation {
  Path(PathBuf),
  // TODO: remote files at some point
}

impl FileLocation {
  pub fn from_path(path: PathBuf) -> Self { FileLocation::Path(path.canonicalize().unwrap()) }

  pub fn from_lsp(url: Url) -> Option<Self> {
    match url.scheme() {
      "file" => Some(FileLocation::Path(url.to_file_path().ok()?)),
      _ => None,
    }
  }

  pub fn to_lsp(&self) -> Url {
    match self {
      FileLocation::Path(path) => Url::from_file_path(path).unwrap(),
    }
  }

  pub fn as_path(&self) -> Option<&Path> {
    match self {
      FileLocation::Path(path) => Some(path.as_path()),
    }
  }
}
