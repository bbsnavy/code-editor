use serde::{de::DeserializeOwned, ser::SerializeStruct, Deserialize, Serialize};

mod read;
mod write;

pub use read::ReadStream;
pub use write::WriteStream;

#[derive(Deserialize, Debug, Clone)]
pub struct Response {
  pub id:     i32,
  pub result: serde_json::Value,
}

#[derive(Deserialize, Debug, Clone)]
pub struct Error {
  pub id:    i32,
  pub error: serde_json::Value,
}

#[derive(Debug, Clone)]
pub struct Request<'a, T> {
  pub id:     i32,
  pub method: &'a str,
  pub params: T,
}

#[derive(Debug, Clone)]
pub struct Notification<'a, T> {
  pub method: &'a str,
  pub params: T,
}

#[derive(Deserialize, Debug, Clone)]
pub struct NotificationOwned {
  pub method: String,
  pub params: serde_json::Value,
}

#[derive(Deserialize, Debug, Clone)]
#[serde(untagged)]
pub enum Message {
  Response(Response),
  Error(Error),
  Notification(NotificationOwned),
}

impl<T: Serialize> Serialize for Request<'_, T> {
  fn serialize<S: serde::Serializer>(&self, serializer: S) -> Result<S::Ok, S::Error> {
    let mut s = serializer.serialize_struct("Request", 4)?;
    s.serialize_field("jsonrpc", "2.0")?;
    s.serialize_field("id", &self.id)?;
    s.serialize_field("method", &self.method)?;
    s.serialize_field("params", &self.params)?;
    s.end()
  }
}

impl<T: Serialize> Serialize for Notification<'_, T> {
  fn serialize<S: serde::Serializer>(&self, serializer: S) -> Result<S::Ok, S::Error> {
    let mut s = serializer.serialize_struct("Request", 3)?;
    s.serialize_field("jsonrpc", "2.0")?;
    s.serialize_field("method", &self.method)?;
    s.serialize_field("params", &self.params)?;
    s.end()
  }
}

impl Response {
  pub fn decode<T: DeserializeOwned>(self) -> Result<T, serde_json::Error> {
    serde_json::from_value(self.result)
  }
}

impl NotificationOwned {
  pub fn decode<T: DeserializeOwned>(self) -> Result<T, serde_json::Error> {
    serde_json::from_value(self.params)
  }
}
