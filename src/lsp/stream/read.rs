use std::io::{self, Error, ErrorKind, Read};

use super::Message;

pub struct ReadStream<R> {
  read:  Vec<u8>,
  pub r: R,
}

impl<R> ReadStream<R> {
  pub fn new(r: R) -> Self { ReadStream { read: Vec::with_capacity(1024), r } }
}

struct Header<'a> {
  name:  &'a str,
  value: &'a str,
}

impl<'a> Header<'a> {
  pub fn parse(s: &'a str) -> io::Result<Self> {
    let mut parts = s.splitn(2, ':');
    let name =
      parts.next().ok_or_else(|| Error::new(ErrorKind::InvalidData, "missing header name"))?;
    let value = parts
      .next()
      .ok_or_else(|| Error::new(ErrorKind::InvalidData, "missing header value"))?
      .trim();

    Ok(Header { name, value })
  }
}

impl<R: Read> ReadStream<R> {
  pub fn poll(&mut self) -> io::Result<Option<()>> {
    let mut tmp = [0; 1024];
    match self.r.read(&mut tmp) {
      Ok(n) => {
        self.read.extend(&tmp[..n]);
        Ok(Some(()))
      }
      Err(e) if e.kind() == io::ErrorKind::WouldBlock => Ok(None),
      Err(e) => Err(e),
    }
  }

  pub fn recv(&mut self) -> io::Result<Option<Message>> {
    if !self.read.is_empty() {
      let mut headers = vec![];
      let mut i = 0;
      loop {
        if let Some(pos) = self.read[i..].windows(2).position(|v| v == b"\r\n") {
          let str = std::str::from_utf8(&self.read[i..i + pos]).map_err(|e| {
            Error::new(ErrorKind::InvalidData, format!("invalid UTF-8 in response: {}", e))
          })?;
          if str.is_empty() {
            i += 2;
            break;
          }
          headers.push(Header::parse(str)?);
          i += pos + 2;
        } else {
          return Ok(None);
        }
      }

      let len_header = headers.iter().find(|h| h.name == "Content-Length").ok_or_else(|| {
        Error::new(ErrorKind::InvalidData, "missing Content-Length header in response")
      })?;
      let len = len_header.value.parse::<usize>().map_err(|e| {
        Error::new(
          ErrorKind::InvalidData,
          format!("invalid Content-Length header in response: {}", e),
        )
      })?;

      if self.read[i..].len() < len {
        return Ok(None);
      }

      let str = std::str::from_utf8(&self.read[i..i + len]).map_err(|e| {
        Error::new(ErrorKind::InvalidData, format!("invalid UTF-8 in response: {}", e))
      })?;
      let value = serde_json::from_str(str).map_err(|e| {
        Error::new(ErrorKind::InvalidData, format!("invalid JSON in response: {}", e))
      })?;

      self.read = self.read.split_off(i + len);

      Ok(Some(value))
    } else {
      Ok(None)
    }
  }
}
