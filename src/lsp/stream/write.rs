use std::{
  collections::VecDeque,
  io::{self, Write},
};

use serde::Serialize;

use super::{Notification, Request};

pub struct WriteStream<W> {
  tmp:      Vec<u8>,
  outgoing: VecDeque<u8>,
  pub w:    W,
}

impl<W> WriteStream<W> {
  pub fn new(w: W) -> Self {
    WriteStream { tmp: Vec::with_capacity(1024), outgoing: VecDeque::with_capacity(1024), w }
  }
}

impl<W: Write> WriteStream<W> {
  fn write(&mut self, req: &impl Serialize) -> io::Result<()> {
    self.tmp.clear();
    serde_json::to_writer(&mut self.tmp, &req)?;

    self.write_raw_fmt(format_args!("Content-Length: {}\r\n\r\n", self.tmp.len()))?;

    if self.outgoing.is_empty() {
      match self.w.write(&self.tmp) {
        Ok(_) => Ok(()),
        Err(e) if e.kind() == io::ErrorKind::WouldBlock => {
          self.outgoing.extend(&self.tmp);
          Ok(())
        }
        Err(e) => Err(e),
      }
    } else {
      self.outgoing.extend(&self.tmp);
      Ok(())
    }
  }

  fn write_raw_fmt(&mut self, v: std::fmt::Arguments) -> io::Result<()> {
    if self.outgoing.is_empty() {
      match self.w.write_fmt(v) {
        Ok(_) => Ok(()),
        Err(e) if e.kind() == io::ErrorKind::WouldBlock => {
          self.outgoing.extend(v.to_string().as_bytes());
          Ok(())
        }
        Err(e) => Err(e),
      }
    } else {
      self.outgoing.extend(v.to_string().as_bytes());
      Ok(())
    }
  }

  pub fn send(&mut self, req: &Request<impl Serialize>) -> io::Result<()> { self.write(req) }
  pub fn notify(&mut self, req: &Notification<impl Serialize>) -> io::Result<()> { self.write(req) }

  pub fn flush(&mut self) -> io::Result<Option<()>> {
    if self.outgoing.is_empty() {
      return Ok(None);
    }

    let slice = if self.outgoing.as_slices().0.is_empty() {
      self.outgoing.as_slices().1
    } else {
      self.outgoing.as_slices().0
    };
    match self.w.write(slice) {
      Ok(n) => {
        self.outgoing.drain(..n);
        let _ = self.w.flush();
        Ok(Some(()))
      }
      Err(e) if e.kind() == io::ErrorKind::WouldBlock => Ok(None),
      Err(e) => Err(e),
    }
  }
}
