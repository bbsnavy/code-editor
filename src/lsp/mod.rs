use std::{
  collections::HashMap,
  io,
  process::{Child, ChildStdin, ChildStdout, Command, Stdio},
};

use lsp_types::{
  ClientCapabilities, ClientInfo, CompletionClientCapabilities, CompletionItemCapability,
  TextDocumentClientCapabilities, WorkspaceFolder,
};

use crate::{
  event::{watch, Event, Events, Token},
  lsp::stream::Request,
  tab::editor::EditorTab,
};

use self::stream::{Notification, NotificationOwned, Response};

mod stream;

pub struct LSPConnection {
  #[allow(unused)]
  proc:   Child,
  reader: stream::ReadStream<ChildStdout>,
  writer: stream::WriteStream<ChildStdin>,

  stdin_token:  Token,
  stdout_token: Token,

  initialized: Option<Initialized>,
  handlers:    HashMap<i32, MsgHandler>,
  next_id:     i32,

  // Listeners for notifications
  listeners: HashMap<String, NotificationHandler>,
}

pub struct Initialized {
  pub server_capabilities: lsp_types::ServerCapabilities,
}

pub struct MsgHandler {
  pub f: Box<dyn FnOnce(Response, &mut EditorTab)>,
}

pub struct NotificationHandler {
  pub f: Box<dyn FnMut(NotificationOwned, &mut EditorTab)>,
}

impl LSPConnection {
  pub fn new(events: &mut Events) -> Self {
    let server = "rust-analyzer";
    // let server = "../lsp-test/target/debug/lsp-test";

    let mut proc =
      Command::new(server).stdin(Stdio::piped()).stdout(Stdio::piped()).spawn().unwrap();

    let stdout = proc.stdout.take().unwrap();
    let stdin = proc.stdin.take().unwrap();

    set_nonblocking(&stdin);
    set_nonblocking(&stdout);
    let stdin_token = events.register(&stdin, watch::READ | watch::WRITE);
    let stdout_token = events.register(&stdout, watch::READ | watch::WRITE);

    let mut writer = stream::WriteStream::new(stdin);
    let reader = stream::ReadStream::new(stdout);

    let params = lsp_types::InitializeParams {
      client_info: Some(ClientInfo { name: "code-editor".into(), version: None }),
      capabilities: ClientCapabilities {
        text_document: Some(TextDocumentClientCapabilities {
          completion: Some(CompletionClientCapabilities {
            completion_item: Some(CompletionItemCapability {
              documentation_format: Some(vec![lsp_types::MarkupKind::Markdown]),
              ..Default::default()
            }),
            ..Default::default()
          }),
          diagnostic: Some(lsp_types::DiagnosticClientCapabilities {
            related_document_support: Some(true),
            ..Default::default()
          }),
          publish_diagnostics: Some(lsp_types::PublishDiagnosticsClientCapabilities {
            related_information: Some(true),
            ..Default::default()
          }),
          ..Default::default()
        }),
        workspace: Some(lsp_types::WorkspaceClientCapabilities {
          workspace_folders: Some(true),
          ..Default::default()
        }),
        ..Default::default()
      },
      workspace_folders: Some(vec![WorkspaceFolder {
        uri:  lsp_types::Url::parse(&format!(
          "file://{}",
          std::env::current_dir().unwrap().display()
        ))
        .unwrap(),
        name: "code-editor".into(),
      }]),
      ..Default::default()
    };

    writer.send(&Request { id: 1, method: "initialize", params: &params }).unwrap();
    writer.flush().unwrap();

    LSPConnection {
      proc,
      reader,
      writer,
      stdin_token,
      stdout_token,
      initialized: None,
      handlers: HashMap::new(),
      next_id: 2,
      listeners: HashMap::new(),
    }
  }

  pub fn send(
    &mut self,
    method: &str,
    params: &impl serde::Serialize,
    handler: impl FnOnce(Response, &mut EditorTab) + 'static,
  ) {
    if self.initialized.is_none() {
      panic!("not initialized yet!");
    }
    let id = self.next_id;
    self.next_id += 1;
    let request = Request { id, method, params };
    self.writer.send(&request).unwrap();
    self.handlers.insert(id, MsgHandler { f: Box::new(handler) });
  }

  pub fn notify(&mut self, method: &str, params: &impl serde::Serialize) {
    if self.initialized.is_none() {
      panic!("not initialized yet!");
    }
    let request = Notification { method, params };
    self.writer.notify(&request).unwrap();
  }

  pub fn listen(
    &mut self,
    method: &str,
    handler: impl FnMut(NotificationOwned, &mut EditorTab) + 'static,
  ) {
    self.listeners.insert(method.into(), NotificationHandler { f: Box::new(handler) });
  }

  fn flush_write(&mut self) -> io::Result<()> {
    info!("flusing writer (their stdin)");
    while self.writer.flush()?.is_some() {}
    Ok(())
  }
  fn flush_read(&mut self, main_window: &mut EditorTab) -> io::Result<()> {
    info!("flusing reader (their stdout)");
    while self.reader.poll()?.is_some() {
      while let Some(msg) = self.reader.recv()? {
        match msg {
          stream::Message::Notification(not) if not.method == "window/logMessage" => {
            let msg = not.decode::<lsp_types::LogMessageParams>().unwrap();
            info!("lsp: {}", msg.message);
            continue;
          }
          _ => {}
        }
        if self.initialized.is_none() {
          // we are not initialized, we must get the initialize response.
          let res = match msg {
            stream::Message::Response(res) => res,
            _ => panic!("expected initialize response"),
          };
          assert_eq!(res.id, 1);

          let msg = res.decode::<lsp_types::InitializeResult>().unwrap();
          self.initialized = Some(Initialized { server_capabilities: msg.capabilities });
          self.writer.notify(&Notification { method: "initialized", params: () }).unwrap();
          info!("initialized!");
        } else {
          // we are initialized, this could be anything else
          match msg {
            stream::Message::Notification(not) => {
              if let Some(handler) = self.listeners.get_mut(&not.method) {
                (handler.f)(not, main_window);
              } else {
                warn!("no listener found for {not:?}");
              }
            }
            stream::Message::Response(res) => match self.handlers.remove(&res.id) {
              Some(handler) => {
                (handler.f)(res, main_window);
              }
              None => {
                warn!("no handler found for {res:?}");
              }
            },
            stream::Message::Error(res) => {
              if self.handlers.remove(&res.id).is_some() {
                warn!("got error: {res:?}");
              } else {
                warn!("no handler found for {res:?}");
              }
            }
          }
        }
      }
    }
    Ok(())
  }

  pub fn handle_event(&mut self, event: &Event, main_window: &mut EditorTab) -> io::Result<bool> {
    if event.token == self.stdin_token || event.token == self.stdout_token {
      if event.kind & watch::READ != 0 {
        self.flush_read(main_window)?;
      }
      if event.kind & watch::WRITE != 0 {
        self.flush_write()?;
      }
      Ok(true)
    } else {
      Ok(false)
    }
  }

  pub fn shutdown(mut self) {
    set_blocking(&self.reader.r);
    set_blocking(&self.writer.w);

    let _ = self.writer.send(&Request { id: self.next_id, method: "shutdown", params: () });
    let _ = self.writer.flush();
    let _ = self.reader.poll();
    // todo: listen for shutdown response

    let _ = self.writer.notify(&Notification { method: "exit", params: () });
    let _ = self.writer.flush();
  }
}

use std::os::unix::io::AsRawFd;

pub fn set_nonblocking(fd: &impl AsRawFd) {
  unsafe {
    let fd = fd.as_raw_fd();
    let fl = libc::fcntl(fd, libc::F_GETFL);
    libc::fcntl(fd, libc::F_SETFL, fl | libc::O_NONBLOCK);
  }
}
pub fn set_blocking(fd: &impl AsRawFd) {
  unsafe {
    let fd = fd.as_raw_fd();
    let fl = libc::fcntl(fd, libc::F_GETFL);
    libc::fcntl(fd, libc::F_SETFL, fl & !libc::O_NONBLOCK);
  }
}
