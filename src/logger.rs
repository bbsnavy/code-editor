use std::{
  fs::{File, OpenOptions},
  io::Write,
  sync::Mutex,
};

pub struct FileLogger {
  file: Mutex<File>,
}

impl FileLogger {
  #[allow(unused)]
  pub fn new() -> Self {
    let path = crate::data_dir().join("log/log.txt");
    std::fs::create_dir_all(path.parent().unwrap()).unwrap();
    FileLogger { file: OpenOptions::new().create(true).append(true).open(path).unwrap().into() }
  }
}

impl log::Log for FileLogger {
  fn enabled(&self, _: &log::Metadata) -> bool { true }
  fn flush(&self) {}
  fn log(&self, record: &log::Record) {
    if self.enabled(record.metadata()) {
      let mut f = self.file.lock().unwrap();
      let _ = writeln!(f, "[{}] {}", record.level(), record.args());
    }
  }
}

pub struct StdoutLogger {}

impl StdoutLogger {
  #[allow(unused)]
  pub fn new() -> Self { StdoutLogger {} }
}

impl log::Log for StdoutLogger {
  fn enabled(&self, _: &log::Metadata) -> bool { true }
  fn flush(&self) {}
  fn log(&self, record: &log::Record) {
    if self.enabled(record.metadata()) {
      println!("[{}] {}", record.level(), record.args());
    }
  }
}
