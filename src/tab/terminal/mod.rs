use std::rc::Rc;

use super::editor::{KeyResponse, ScopedCtx};
use crate::{global::GlobalState, input::KeySequence, terminal::Terminal};

pub struct TerminalTab {
  #[allow(unused)]
  global: Rc<GlobalState>,

  pub terminal: Terminal,
}

impl TerminalTab {
  pub fn new(global: Rc<GlobalState>) -> Self { TerminalTab { global, terminal: Terminal::new() } }

  pub fn render(&mut self, ctx: &mut ScopedCtx) { self.terminal.render(ctx); }

  pub fn input(&mut self, seq: &KeySequence) -> KeyResponse {
    for key in &seq.seq {
      self.terminal.input(key.clone());
    }
    KeyResponse::Consumed
  }
}
