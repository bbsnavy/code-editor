use std::{collections::HashMap, rc::Rc};

use crate::{
  event::{Events, Token},
  global::GlobalState,
  gui::{api::Size, GuiEvent, RenderHandler},
  input::{Event, Key, KeySequence, KeyWithMods, Modifiers},
  theme::Theme,
};

pub mod editor;
pub mod terminal;

use editor::EditorTab;
use terminal::TerminalTab;
use winit::event::ElementState;

use self::editor::{KeyResponse, ScopedCtx};

/// A set of tabs. Each tab could be an editor, terminal, or something else.
pub struct Tabs {
  global:       Rc<GlobalState>,
  needs_render: bool,

  tabs:        Vec<Tab>,
  selected:    Option<usize>,
  user_events: HashMap<Token, usize>,

  seq:  KeySequence,
  mods: Modifiers,

  width:  u32,
  height: u32,
  theme:  Theme,
}

pub enum Tab {
  Editor(EditorTab),
  Terminal(TerminalTab),
}

impl Tabs {
  pub fn new(global: Rc<GlobalState>) -> Self {
    Tabs {
      global,
      needs_render: false,
      seq: KeySequence::default(),
      mods: Modifiers::default(),
      tabs: vec![],
      user_events: HashMap::new(),
      selected: None,
      width: 0,
      height: 0,
      theme: Theme::default_theme(),
    }
  }

  pub fn resize(&mut self, size: Size) {
    self.needs_render = true;
    self.width = size.width;
    self.height = size.height;

    let scoped_size = Size { width: size.width, height: size.height - 40 };

    for tab in &mut self.tabs {
      match tab {
        Tab::Editor(e) => e.resize(scoped_size),
        Tab::Terminal(_) => {}
      }
    }
  }

  fn add_terminal(&mut self, e: &mut Events) {
    let mut terminal = TerminalTab::new(self.global.clone());
    let token = terminal.terminal.register(e);
    self.user_events.insert(token, self.tabs.len());
    self.tabs.push(Tab::Terminal(terminal));
    self.needs_render = true;
  }

  fn add_editor(&mut self) {
    self.tabs.push(Tab::Editor(EditorTab::new(self.global.clone())));
    self.needs_render = true;
  }

  pub fn add_default_tabs(&mut self, e: &mut Events) {
    self.add_terminal(e);
    self.add_editor();
    self.add_terminal(e);

    self.selected = Some(2);
    self.needs_render = true;
  }

  pub fn active(&self) -> Option<&Tab> { self.selected.and_then(|i| self.tabs.get(i)) }
  #[allow(unused)]
  pub fn active_mut(&mut self) -> Option<&mut Tab> {
    self.selected.and_then(|i| self.tabs.get_mut(i))
  }

  pub fn input(&mut self, ev: Event) {
    match ev {
      Event::KeyUp(k) => match k {
        Key::Modifier(m) => {
          self.mods.clear(m);
          return;
        }
        _ => {}
      },
      Event::KeyDown(k) => match k {
        Key::Modifier(m) => {
          self.mods.set(m);
          return;
        }
        Key::Char(c) => {
          if self.mods.has_shift() {
            // TODO: Actually handle shift
            let c = match c {
              '1' => '!',
              '2' => '@',
              '3' => '#',
              '4' => '$',
              '5' => '%',
              '6' => '^',
              '7' => '&',
              '8' => '*',
              '9' => '(',
              '0' => ')',
              '-' => '_',
              '=' => '+',
              '[' => '{',
              ']' => '}',
              '\\' => '|',
              ';' => ':',
              '\'' => '"',
              ',' => '<',
              '.' => '>',
              '/' => '?',
              _ => c.to_ascii_uppercase(),
            };
            self.seq.seq.push(KeyWithMods { key: Key::Char(c), mods: self.mods })
          } else {
            self.seq.seq.push(KeyWithMods { key: Key::Char(c), mods: self.mods })
          }
        }
        key => self.seq.seq.push(KeyWithMods { key, mods: self.mods }),
      },
      Event::Mouse(_) => {
        // self.mouse_event(m);
        return;
      }
    }

    self.needs_render = true;
    let res = self.process_input();

    match res {
      KeyResponse::Waiting => {}
      _ => self.seq.seq.clear(),
    }
  }

  fn process_input(&mut self) -> KeyResponse {
    if self.seq.seq.first().map(|k| k.key == Key::Char('\\')).unwrap_or(false) {
      return match self.seq.seq.get(1) {
        Some(KeyWithMods { key: Key::Char(c @ '0'..='9'), .. }) => {
          let n = c.to_digit(10).unwrap() as usize;
          self.selected = Some(n);
          KeyResponse::Consumed
        }
        Some(KeyWithMods { key: Key::Char('\\'), .. }) => {
          self.seq.seq.pop();
          self.process_input_inner()
        }

        Some(_) => KeyResponse::Unknown,
        None => KeyResponse::Waiting,
      };
    }

    self.process_input_inner()
  }

  fn process_input_inner(&mut self) -> KeyResponse {
    self
      .selected
      .and_then(|i| self.tabs.get_mut(i))
      .map(|tab| match tab {
        Tab::Editor(e) => e.input(&self.seq),
        Tab::Terminal(t) => t.input(&self.seq),
      })
      .unwrap_or(KeyResponse::Consumed)
  }

  fn user_lsp_event(&mut self, event: GuiEvent) {
    // TODO: LSP events should really be handled here, not in the editor. While we
    // only have one editor this will work though.
    for tab in &mut self.tabs {
      match tab {
        Tab::Editor(e) => {
          e.user_event(event);
          break;
        }
        Tab::Terminal(_) => {}
      }
    }
  }
}

impl RenderHandler for Tabs {
  fn keyboard(&mut self, input: winit::event::KeyboardInput) {
    match crate::input::qwerty::key_from_scancode(input.scancode) {
      Some(k) => match input.state {
        ElementState::Pressed => self.input(Event::KeyDown(k)),
        ElementState::Released => self.input(Event::KeyUp(k)),
      },
      None => {}
    }
  }
  fn render(&mut self, ctx: &mut crate::gui::api::Ctx) {
    let mut scoped = ScopedCtx {
      ctx,
      theme: &self.theme,
      active: true,
      x: 0,
      y: 0,
      width: self.width,
      height: self.height,
    };

    self.selected.and_then(|i| self.tabs.get_mut(i)).map(|tab| match tab {
      Tab::Editor(e) => e.render(&mut scoped),
      Tab::Terminal(e) => e.render(&mut scoped),
    });

    self.needs_render = false;
  }
  fn needs_render(&self) -> bool { self.needs_render }
  fn user_event(&mut self, event: GuiEvent) {
    match event {
      GuiEvent::IOEvent(e) => {
        self.needs_render = true;
        if let Some(index) = self.user_events.get(&e.token) {
          self.tabs[*index].user_event(e);
        } else {
          self.user_lsp_event(GuiEvent::IOEvent(e));
        }
      }
      _ => self.user_lsp_event(event),
    }
  }

  fn resize(&mut self, size: Size) { self.resize(size); }
  fn should_exit(&self) -> bool {
    self
      .active()
      .map(|tab| match tab {
        Tab::Editor(e) => e.should_exit(),
        Tab::Terminal(_) => false,
      })
      .unwrap_or(false)
  }
  fn shutdown(&mut self) {
    if let Some(client) = self.global.lsp_client.take() {
      client.shutdown();
    }
  }
}

#[derive(Clone, Copy)]
pub enum CursorMode {
  Block,
  Beam,
}

impl Tab {
  fn user_event(&mut self, e: crate::event::Event) {
    match self {
      Tab::Editor(_) => {}
      Tab::Terminal(t) => t.terminal.user_event(e),
    }
  }
}
