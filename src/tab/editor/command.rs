use crate::{editor::Editor, gui::text::StringWithColors, input::Key};

use super::{CursorMode, InputContext, KeyResponse, ScopedCtx, Window};

pub struct CommandPane {
  /// Content being typed.
  content: String,

  /// Output of last command to show.
  output: String,
}

impl Window for CommandPane {
  fn render(&self, ctx: &mut ScopedCtx) {
    let style = ctx.theme().get("command");

    ctx.draw_rect_px(0, 0, ctx.width, ctx.height, style.background);

    ctx.draw_str(
      0,
      0,
      StringWithColors::from_string_solid_color(self.output.clone(), style.foreground),
    );

    ctx.draw_cursor_px(ctx.col_to_px(self.content.len() as u16 + 1), 0, CursorMode::Beam);
  }
  fn input(&mut self, ctx: &mut InputContext) -> KeyResponse {
    for c in &ctx.seq.seq {
      match c.key {
        Key::Char(c) => self.content.push(c),
        _ => match c.to_string().as_str() {
          "ESC" => {
            self.content.clear();
            ctx.switch_to_editor()
          }
          "ENTER" => {
            ctx.run_command();
            ctx.switch_to_editor();
            return KeyResponse::Consumed;
          }
          "BS" => {
            self.content.pop();
          }
          _ => {}
        },
      }
    }

    self.output = format!(":{}", self.content);

    KeyResponse::Consumed
  }

  fn mouse_event(&mut self, _: crate::input::MouseEvent) {}
}

impl CommandPane {
  pub fn new() -> Self { CommandPane { content: String::new(), output: String::new() } }

  pub fn run_command(&mut self, editor: &mut Editor, should_exit: &mut bool) {
    self.output = match self.content.as_str() {
      "w" => match editor.save() {
        Some((path, Ok(_))) => format!("saved to {}", path.display()),
        Some((path, Err(e))) => format!("failed to save to {}: {e}", path.display()),
        None => "no file open".into(),
      },
      "q" => {
        *should_exit = true;
        "closing".into()
      }
      _ => "no such command".into(),
    };
    self.content.clear();
  }
}
