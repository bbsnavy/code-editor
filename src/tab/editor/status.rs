use super::{editor::EditorPane, ActiveSection, InputContext, KeyResponse, ScopedCtx, Window};
use crate::editor::Mode;

pub struct StatusPane {
  mode:           Mode,
  active_section: ActiveSection,
}

impl StatusPane {
  pub fn new() -> Self { StatusPane { mode: Mode::Normal, active_section: ActiveSection::Editor } }

  pub fn update(&mut self, editor: &EditorPane) { self.mode = editor.editor.mode; }
  pub fn update_active(&mut self, active: ActiveSection) { self.active_section = active; }
}

impl Window for StatusPane {
  fn render(&self, ctx: &mut ScopedCtx) {
    let style = ctx.theme().get("status");

    ctx.draw_rect(0, 0, ctx.rows(), ctx.cols(), style);

    let style = ctx.theme().get(match (self.active_section, self.mode) {
      (ActiveSection::Command, _) => "status.mode.command",
      (_, Mode::Normal) => "status.mode.normal",
      (_, Mode::Insert) => "status.mode.insert",
      (_, Mode::Visual(_)) => "status.mode.visual",
    });
    ctx.draw_rect_px(ctx.width - 50, 0, 50, ctx.height, style.background);
  }

  fn input(&mut self, _: &mut InputContext) -> KeyResponse { KeyResponse::Consumed }
  fn mouse_event(&mut self, _: crate::input::MouseEvent) {}
}
