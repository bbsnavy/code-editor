use crate::{
  editor::{Change, Editor},
  global::GlobalState,
  gui::{
    api::{Color, Size},
    text::StringWithColors,
  },
  icon::Icon,
  input::{KeySequence, MouseEvent},
  location::FileLocation,
  theme::{Style, Theme},
};
use std::{cmp::min, collections::HashMap, path::PathBuf, rc::Rc};

use super::CursorMode;

mod command;
mod editor;
mod file_tree;
mod status;

pub struct Pane<W> {
  pub width:  u32,
  pub height: u32,

  pub imp: W,
}

#[allow(unused)]
#[derive(PartialEq, Eq)]
pub enum VSide {
  Top,
  Bottom,
}

#[allow(unused)]
#[derive(PartialEq, Eq)]
pub enum HSide {
  Left,
  Right,
}

pub enum KeyResponse {
  /// Not enough keys have been entered. This keeps `seq` as-is.
  Waiting,
  /// An unknown sequence has been entered. `seq` will be cleared.
  Unknown,
  /// A valid sequence has been entered. `seq` will be cleared.
  Consumed,
}

pub trait Window {
  fn render(&self, ctx: &mut ScopedCtx);
  fn input(&mut self, seq: &mut InputContext) -> KeyResponse;
  fn mouse_event(&mut self, ev: MouseEvent);
}

impl<W: Window> Pane<W> {
  pub fn new(imp: W) -> Self { Pane { imp, width: 0, height: 0 } }

  pub fn input(&mut self, ctx: &mut InputContext) -> KeyResponse { self.imp.input(ctx) }
}

pub struct ScopedCtx<'a, 'b> {
  pub ctx:    &'a mut crate::gui::api::Ctx<'b>,
  pub theme:  &'a crate::theme::Theme,
  pub active: bool,
  pub x:      u32,
  pub y:      u32,
  pub width:  u32,
  pub height: u32,
}

const CURSOR_WIDTH: u32 = 20;
const CURSOR_HEIGHT: u32 = 50;

impl<'b> ScopedCtx<'_, 'b> {
  pub fn with_active(mut self, active: bool) -> Self {
    self.active = active;
    self
  }

  pub fn theme(&self) -> &Theme { self.theme }

  /// The number of complete rows that will fit in `height`
  pub fn rows(&self) -> u16 { (self.height / CURSOR_HEIGHT) as u16 }
  /// The number of complete columns that will fit in `width`
  pub fn cols(&self) -> u16 { (self.width / CURSOR_WIDTH) as u16 }

  /// The nmber of visible rows that will fit in `height`. This is usually
  /// `rows() + 1`, but if rows fits exactly in `height`, it will be `rows()`.
  pub fn visible_rows(&self) -> u16 { ((self.height + CURSOR_HEIGHT - 1) / CURSOR_HEIGHT) as u16 }
  /// The nmber of visible columns that will fit in `width`. This is usually
  /// `cols() + 1`, but if columns fits exactly in `width`, it will be `cols()`.
  #[allow(unused)]
  pub fn visible_cols(&self) -> u16 { ((self.width + CURSOR_WIDTH - 1) / CURSOR_WIDTH) as u16 }

  pub fn col_to_px(&self, col: u16) -> u32 { u32::from(col) * CURSOR_WIDTH }
  pub fn row_to_px(&self, row: u16) -> u32 { u32::from(row) * CURSOR_HEIGHT }

  pub fn draw_rect(&mut self, row: u16, col: u16, rows: u16, cols: u16, style: Style) {
    self.draw_rect_px(
      self.col_to_px(col),
      self.row_to_px(row),
      self.col_to_px(cols),
      self.row_to_px(rows),
      style.background,
    )
  }
  pub fn draw_char(&mut self, row: u16, col: u16, ch: char, style: Style) {
    self.draw_char_px(self.col_to_px(col), self.row_to_px(row), ch, style.foreground)
  }

  /// Draws an icon at the given row and column. The icon will be centered
  /// vertically. Icons are typically squares, so this will usually take up
  /// about two character widths.
  pub fn draw_icon(&mut self, row: u16, col: u16, icon: Icon, color: impl Into<Color>) {
    self.draw_icon_px(
      self.col_to_px(col),
      // center the icon vertically
      self.row_to_px(row) + ((CURSOR_HEIGHT - 40) / 2),
      icon,
      color,
    )
  }

  pub fn draw_str(&mut self, row: u16, col: u16, str: impl Into<StringWithColors>) {
    self.draw_str_px(self.col_to_px(col), self.row_to_px(row), str);
  }

  pub fn draw_cursor_px(&mut self, x: u32, y: u32, mode: CursorMode) {
    if self.active {
      match mode {
        CursorMode::Block => self.draw_rect_px(x, y, 20, 50, 0xffffff),
        CursorMode::Beam => self.draw_rect_px(x, y, 4, 50, 0xffffff),
      }
    }
  }

  pub fn draw_icon_px(&mut self, x: u32, y: u32, icon: Icon, color: impl Into<Color>) {
    self.ctx.draw_icon(x + self.x, y + self.y, icon, 40, color);
  }
  pub fn draw_str_px(&mut self, x: u32, y: u32, str: impl Into<StringWithColors>) {
    self.ctx.draw_text(x + self.x, y + self.y, str, 40);
  }
  pub fn draw_char_px(&mut self, x: u32, y: u32, ch: char, color: impl Into<Color>) {
    self.ctx.draw_text(
      x + self.x,
      y + self.y,
      StringWithColors::from_string_solid_color(ch.to_string(), color.into()),
      40,
    )
  }

  pub fn draw_rect_px(&mut self, x: u32, y: u32, width: u32, height: u32, color: impl Into<Color>) {
    self.ctx.draw_rectangle(x + self.x, y + self.y, width, height, color);
  }

  fn sub_ctx<'a>(&'a mut self, x: u32, y: u32, width: u32, height: u32) -> ScopedCtx<'a, 'b> {
    ScopedCtx {
      ctx:    self.ctx,
      theme:  self.theme,
      active: self.active,
      x:      self.x + x,
      y:      self.y + y,
      width:  min(width, width + (self.x + self.width) - (self.x + x + width)),
      height: min(height, height + (self.y + self.height) - (self.y + y + height)),
    }
  }
}

// below is the editor tab

#[derive(Clone, Copy, PartialEq)]
pub enum ActiveSection {
  FileTree,
  Editor,
  Command,
}

pub struct EditorTab {
  global: Rc<GlobalState>,

  active: ActiveSection,

  pub file_tree: Pane<file_tree::FileTreePane>,
  pub editor:    Pane<editor::EditorPane>,
  pub status:    Pane<status::StatusPane>,
  pub command:   Pane<command::CommandPane>,

  pub buffers: HashMap<FileLocation, Editor>,

  pub should_exit: bool,
}

impl EditorTab {
  pub fn new(global: Rc<GlobalState>) -> Self {
    let file_tree = file_tree::FileTreePane::new();
    let status = status::StatusPane::new();
    let command = command::CommandPane::new();
    let mut editor = editor::EditorPane::new(global.clone());

    if let Some(path) = std::env::args().nth(1) {
      let content = std::fs::read_to_string(path).unwrap();
      editor.editor.doc.apply_change(&Change {
        start:  0,
        end:    editor.editor.doc.len(),
        insert: content,
      });
    }

    // let window = SizedWindow::h_split(&term, 20, Box::new(file_tree),
    // Box::new(editor));
    EditorTab {
      global,
      active: ActiveSection::Editor,
      file_tree: Pane::new(file_tree),
      editor: Pane::new(editor),
      status: Pane::new(status),
      command: Pane::new(command),
      buffers: HashMap::new(),
      should_exit: false,
    }
  }

  pub fn resize(&mut self, size: Size) {
    const CURSOR_WIDTH: u32 = 20;
    const CURSOR_HEIGHT: u32 = 50;

    self.file_tree.width = 20 * CURSOR_WIDTH;
    self.file_tree.height = size.height - 2 * CURSOR_HEIGHT;
    self.editor.width = size.width - 20 * CURSOR_WIDTH;
    self.editor.height = size.height - 2 * CURSOR_HEIGHT;
    self.status.width = size.width;
    self.status.height = CURSOR_HEIGHT;
    self.command.width = size.width;
    self.command.height = CURSOR_HEIGHT;
  }

  pub fn input(&mut self, seq: &KeySequence) -> KeyResponse {
    let mut ctx = InputContext { seq, active: &mut self.active, ops: vec![], should_exit: false };

    let res = match seq.to_string().as_str() {
      "<C-h>" => {
        ctx.move_left();
        KeyResponse::Consumed
      }
      "<C-l>" => {
        ctx.move_right();
        KeyResponse::Consumed
      }
      "<C-j>" => {
        ctx.move_down();
        KeyResponse::Consumed
      }
      "<C-k>" => {
        ctx.move_up();
        KeyResponse::Consumed
      }
      _ => match ctx.active {
        ActiveSection::FileTree => self.file_tree.input(&mut ctx),
        ActiveSection::Editor => self.editor.input(&mut ctx),
        ActiveSection::Command => self.command.input(&mut ctx),
      },
    };

    self.status.imp.update_active(*ctx.active);

    if ctx.should_exit {
      self.should_exit = true;
    }

    for op in ctx.ops {
      self.process(op);
    }

    self.editor.imp.update_scroll();
    self.status.imp.update(&self.editor.imp);

    res
  }

  // TODO: Bring this back
  #[allow(unused)]
  pub fn mouse_event(&mut self, mut ev: MouseEvent) {
    if ev.col < 20 {
      self.active = ActiveSection::FileTree;
      self.file_tree.imp.mouse_event(ev);
    } else {
      self.active = ActiveSection::Editor;
      ev.col -= 20;
      self.editor.imp.mouse_event(ev);
    }
  }

  pub fn editor_mut(&mut self) -> &mut editor::EditorPane { &mut self.editor.imp }

  fn process(&mut self, op: Op) {
    match op {
      Op::OpenFile { path } => self.open(FileLocation::from_path(path)),
      Op::RunCommand => {
        self.command.imp.run_command(&mut self.editor.imp.editor, &mut self.should_exit);
      }
    }
  }

  pub fn open(&mut self, location: FileLocation) {
    if self.editor.imp.editor.file.as_ref().map(|(l, _)| l) == Some(&location) {
      return;
    }
    let editor = match self.buffers.remove(&location) {
      Some(editor) => editor,
      None => {
        let mut editor = Editor::new(self.global.clone());
        editor.open(location);
        editor
      }
    };
    let prev = std::mem::replace(&mut self.editor.imp.editor, editor);
    // TODO: ask about unsaved files here? maybe elsewhere?
    if let Some((location, _)) = &prev.file {
      self.buffers.insert(location.clone(), prev);
    }
  }

  pub fn render(&mut self, ctx: &mut ScopedCtx) {
    // ctx.draw_rectangle(0, 0, 50, 50, 0x00ff00);

    self.file_tree.imp.render(
      &mut ctx
        .sub_ctx(0, 0, self.file_tree.width, self.file_tree.height)
        .with_active(self.active == ActiveSection::FileTree),
    );

    self.editor.imp.render(
      &mut ctx
        .sub_ctx(20 * 20, 0, self.editor.width, self.editor.height)
        .with_active(self.active == ActiveSection::Editor),
    );

    self.status.imp.render(
      &mut ctx
        .sub_ctx(0, self.editor.height, self.status.width, self.status.height)
        .with_active(false),
    );

    self.command.imp.render(
      &mut ctx
        .sub_ctx(
          0,
          self.editor.height + self.status.height,
          self.command.width,
          self.command.height,
        )
        .with_active(self.active == ActiveSection::Command),
    );
  }
}

pub struct InputContext<'a> {
  pub seq: &'a KeySequence,

  active: &'a mut ActiveSection,
  ops:    Vec<Op>,

  pub should_exit: bool,
}

// An operation which requires fields in `MainWindow` that aren't present in
// `InputContext`.
enum Op {
  OpenFile { path: PathBuf },
  RunCommand,
}

impl InputContext<'_> {
  // TODO: Handle split windows here
  pub fn move_left(&mut self) { *self.active = ActiveSection::FileTree }
  pub fn move_right(&mut self) { *self.active = ActiveSection::Editor }
  pub fn move_down(&mut self) {}
  pub fn move_up(&mut self) {}

  pub fn switch_to_editor(&mut self) { *self.active = ActiveSection::Editor }
  pub fn switch_to_command(&mut self) { *self.active = ActiveSection::Command }

  pub fn open_file(&mut self, path: PathBuf) { self.ops.push(Op::OpenFile { path }) }
  pub fn run_command(&mut self) { self.ops.push(Op::RunCommand) }
}

impl EditorTab {
  pub fn user_event(&mut self, event: crate::gui::GuiEvent) {
    match event {
      crate::gui::GuiEvent::Redraw => {}
      crate::gui::GuiEvent::IOEvent(ev) => {
        if let Some(client) = self.global.clone().lsp_client.borrow_mut().as_mut() {
          match client.handle_event(&ev, self) {
            Ok(true) => {}
            Ok(false) => println!("unknown event: {:?}", ev),
            Err(e) => println!("error: {e:?}"),
          }
        }
      }
    }
  }

  pub fn should_exit(&self) -> bool { self.should_exit }
}
