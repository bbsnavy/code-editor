use std::{cell::Cell, rc::Rc};

use crate::{
  editor::Editor,
  global::GlobalState,
  input::{Button, MouseEvent},
};

use super::{InputContext, KeyResponse, ScopedCtx, Window};

pub struct EditorPane {
  pub editor: Editor,

  /// Editor size on screen.
  rows: Cell<u16>,
  cols: Cell<u16>,

  /// Amount of lines that the editor has scrolled down.
  scroll_row:    u64,
  /// Amount of lines to keep on the edges of the screen.
  scroll_offset: u64,
}

impl Window for EditorPane {
  fn render(&self, ctx: &mut ScopedCtx) {
    self.rows.set(ctx.rows());
    self.cols.set(ctx.cols());

    let theme = ctx.theme();

    let default = theme.default;
    let line_number = theme.get("editor.gutter.line_number");

    let plugins = self.editor.plugins.iter().map(|p| p.borrow()).collect::<Vec<_>>();
    let gutters = plugins
      .iter()
      .filter_map(|p| {
        let width = p.gutter_width();
        if width > 0 {
          Some((width, p))
        } else {
          None
        }
      })
      .collect::<Vec<_>>();

    let margin = gutters.iter().map(|v| v.0).sum::<u32>() + ctx.col_to_px(4);

    ctx.draw_rect_px(0, 0, margin, ctx.height, line_number.background);
    ctx.draw_rect_px(margin, 0, ctx.width - margin, ctx.height, default.background);

    let mut x = 0;
    for (width, plugin) in gutters.iter() {
      let mut ctx = ctx.sub_ctx(x, 0, *width, ctx.height);
      plugin.render_gutter(&self.editor, self.scroll_row, &mut ctx);
      x += *width;
    }

    for row in 0..ctx.visible_rows() {
      if row < self.editor.len_lines() as u16 {
        let line_num = row as u64 + self.scroll_row;
        let line_display_num = line_num + 1;

        let s = line_display_num.to_string();
        let pad = margin - ctx.col_to_px(s.len() as u16 + 1);
        for (i, c) in s.chars().enumerate() {
          ctx.draw_char_px(
            pad + ctx.col_to_px(i as u16),
            ctx.row_to_px(row),
            c,
            line_number.foreground,
          );
        }
      }
    }

    self.editor.render(ctx, margin, self.scroll_row);

    for p in &self.editor.plugins {
      p.borrow().render(&self.editor, ctx, self.scroll_row);
    }

    ctx.draw_cursor_px(
      ctx.col_to_px(self.editor.cursor_col() as u16) + margin,
      ctx.row_to_px(self.editor.cursor_row().saturating_sub(self.scroll_row) as u16),
      self.editor.cursor_mode(),
    );
  }
  fn input(&mut self, ctx: &mut InputContext) -> KeyResponse {
    let ret = self.editor.input(ctx);
    self.update_scroll();
    ret
  }

  fn mouse_event(&mut self, ev: MouseEvent) {
    if ev.button == Button::Left {
      self
        .editor
        .mouse_event(u64::from(ev.row) + self.scroll_row, u64::from(ev.col).saturating_sub(4));
    }
  }
}

impl EditorPane {
  pub fn new(global: Rc<GlobalState>) -> Self {
    EditorPane {
      editor: Editor::new(global),

      rows: Cell::new(0),
      cols: Cell::new(0),

      scroll_row:    0,
      scroll_offset: 5,
    }
  }

  pub fn update_scroll(&mut self) {
    let height = u64::from(self.rows.get());

    let cursor_screen_row = self.editor.cursor_row().saturating_sub(self.scroll_row);
    if cursor_screen_row < self.scroll_offset {
      if self.editor.cursor_row() < self.scroll_offset {
        self.scroll_row = 0;
      } else {
        self.scroll_row = self.editor.cursor_row() - self.scroll_offset;
      }
    } else if cursor_screen_row > height - self.scroll_offset {
      self.scroll_row = self.editor.cursor_row() - (height - self.scroll_offset);
    }

    let len_lines = self.editor.len_lines();
    if len_lines >= height {
      let last_row = len_lines - self.scroll_row;
      if last_row < height {
        self.scroll_row -= height - last_row;
      }
    }
  }
}
