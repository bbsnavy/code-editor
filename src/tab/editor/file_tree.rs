use std::{
  fs::DirEntry,
  path::{Path, PathBuf},
};

use crate::icon::Icon;

use super::{InputContext, KeyResponse, ScopedCtx, Window};

pub struct FileTreePane {
  /// Position of the cursor in the entire list.
  cursor_row: u64,

  /// Amount of lines that the editor has scrolled down.
  scroll_row:    u64,
  /// Amount of lines to keep on the edges of the screen.
  scroll_offset: u64,

  tree: Tree,
}

enum Tree {
  Dir { path: PathBuf, expanded: bool, children: Vec<Tree>, loaded: bool },
  File { path: PathBuf },
}

impl Window for FileTreePane {
  fn render(&self, ctx: &mut ScopedCtx) {
    let style = ctx.theme().get("file_tree");
    ctx.draw_rect(0, 0, ctx.rows(), ctx.cols(), style);

    if ctx.active {
      let style = ctx.theme().get("file_tree.active");
      let y = ctx.row_to_px((self.cursor_row - self.scroll_row) as u16);
      ctx.draw_rect_px(0, y, ctx.col_to_px(ctx.cols()), ctx.row_to_px(1), style.background);
    }

    self.tree.render(ctx, &mut 0, 0);
  }
  fn input(&mut self, ctx: &mut InputContext) -> KeyResponse {
    let ret = self.process(ctx);

    const HEIGHT: u64 = 45;

    let cursor_screen_row = self.cursor_row - self.scroll_row;
    if cursor_screen_row < self.scroll_offset {
      if self.cursor_row < self.scroll_offset {
        self.scroll_row = 0;
      } else {
        self.scroll_row = self.cursor_row - self.scroll_offset;
      }
    } else if cursor_screen_row > HEIGHT - self.scroll_offset {
      self.scroll_row = self.cursor_row - (HEIGHT - self.scroll_offset);
    }

    /*
    let len_lines = self.files.len() as u64;
    if len_lines >= HEIGHT {
      let last_row = len_lines - self.scroll_row;
      if last_row < HEIGHT {
        self.scroll_row -= HEIGHT - last_row;
      }
    }
    */

    ret
  }

  fn mouse_event(&mut self, ev: crate::input::MouseEvent) { self.cursor_row = ev.row.into(); }
}

impl FileTreePane {
  pub fn new() -> Self {
    FileTreePane {
      tree: Tree::dir(".".into()),

      cursor_row:    0,
      scroll_row:    0,
      scroll_offset: 5,
    }
  }

  fn process(&mut self, ctx: &mut InputContext) -> KeyResponse {
    match ctx.seq.to_string().as_str() {
      "k" if self.cursor_row > 0 => self.cursor_row -= 1,
      "j" => self.cursor_row += 1,
      "o" => {
        self.tree.toggle(self.cursor_row as u16, &mut 0, ctx);
      }
      _ => return KeyResponse::Unknown,
    }
    KeyResponse::Consumed
  }
}

impl Tree {
  fn dir(path: PathBuf) -> Self {
    let mut dir = Tree::Dir {
      path:     path.clone(),
      children: std::fs::read_dir(&path)
        .unwrap()
        .map(|f| Tree::from_entry(&path, f.unwrap()))
        .collect(),
      expanded: true,
      loaded:   true,
    };
    dir.sort_children();
    dir
  }

  fn from_entry(path: &Path, entry: DirEntry) -> Self {
    if entry.metadata().unwrap().is_dir() {
      Tree::Dir {
        path:     path.join(entry.file_name()),
        expanded: false,
        children: vec![],
        loaded:   false,
      }
    } else {
      Tree::File { path: path.join(entry.file_name()) }
    }
  }

  fn render(&self, ctx: &mut ScopedCtx, row: &mut u16, col: u16) {
    let style = ctx.theme().get("file_tree");

    match self {
      Tree::Dir { path, expanded, children, .. } => {
        ctx.draw_icon(
          *row,
          col,
          if *expanded { Icon::CarrotDown } else { Icon::CarrotRight },
          style.foreground,
        );
        // ctx.draw_char(*row, col, if *expanded { '|' } else { '>' }, style);
        let name = match path.file_name() {
          Some(p) => p.to_str().unwrap(),
          None => ".",
        };
        for (i, c) in name.chars().enumerate() {
          ctx.draw_char(*row, col + i as u16 + 2, c, style);
        }
        *row += 1;
        if *expanded {
          for child in children.iter()
          // .skip(self.scroll_row as usize)
          // .take(self.files.len() - self.scroll_row as usize)
          {
            child.render(ctx, row, col + 2);
          }
        }
      }
      Tree::File { path } => {
        let name = path.file_name().unwrap().to_str().unwrap();
        for (i, c) in name.chars().enumerate() {
          ctx.draw_char(*row, col + i as u16 + 2, c, style);
        }
        *row += 1;
      }
    }
  }

  fn toggle(&mut self, cursor_row: u16, row: &mut u16, ctx: &mut InputContext) -> bool {
    if *row == cursor_row {
      match self {
        Tree::Dir { path, expanded, children, loaded, .. } => {
          *expanded = !*expanded;
          if !*loaded {
            *loaded = true;
            *children = std::fs::read_dir(&path)
              .unwrap()
              .map(|f| Tree::from_entry(path, f.unwrap()))
              .collect();
            self.sort_children();
          }
        }
        Tree::File { path, .. } => {
          ctx.open_file(path.clone());
        }
      }
      true
    } else {
      match self {
        Tree::Dir { expanded, children, .. } => {
          *row += 1;
          if *expanded {
            for child in children.iter_mut() {
              if child.toggle(cursor_row, row, ctx) {
                return true;
              }
            }
          }
        }
        Tree::File { .. } => {
          *row += 1;
        }
      }
      false
    }
  }

  fn sort_children(&mut self) {
    if let Tree::Dir { children, .. } = self {
      children.sort_unstable_by(|a, b| match (a, b) {
        (Tree::File { .. }, Tree::Dir { .. }) => std::cmp::Ordering::Greater,
        (Tree::Dir { .. }, Tree::File { .. }) => std::cmp::Ordering::Less,
        (Tree::Dir { path: a, .. }, Tree::Dir { path: b, .. }) => a.cmp(b),
        (Tree::File { path: a }, Tree::File { path: b }) => a.cmp(b),
      });
    }
  }
}
