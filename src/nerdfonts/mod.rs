mod icons;

pub use icons::Icon;

impl Icon {
  pub fn to_char(self) -> char { icons::to_char(self) }
}
