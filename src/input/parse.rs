use super::{event::MouseEvent, Button, Event, Input};

pub enum ParseResult {
  Valid(Event, usize),
  Invalid(usize),
  Waiting,
}

enum ParseError {
  Invalid(usize),
  Waiting,
}

struct ParseCtx<'a> {
  input: &'a Input,
  i:     usize,
}

impl Input {
  /// Returns Some() if there was a valid escape code, otherwise returs None.
  pub(super) fn parse_escape(&mut self) -> ParseResult {
    let mut ctx = ParseCtx { input: self, i: 1 };

    match ctx.parse_escape() {
      Ok(ev) => ctx.valid(ev),
      Err(ParseError::Invalid(count)) => ParseResult::Invalid(count),
      Err(ParseError::Waiting) => ParseResult::Waiting,
    }
  }
}

impl ParseCtx<'_> {
  pub fn next(&mut self) -> Result<u8, ParseError> {
    let res = self.input.buf.get(self.i).copied();
    self.i += 1;
    res.ok_or(ParseError::Waiting)
  }

  pub fn valid(&self, ev: Event) -> ParseResult { ParseResult::Valid(ev, self.i) }
  pub fn invalid(&self, b: u8) -> ParseError {
    let _ = b;
    ParseError::Invalid(self.i)
  }

  fn parse_escape(&mut self) -> Result<Event, ParseError> {
    match self.next()? {
      b'[' => match self.next()? {
        b'M' => {
          let button = self.next()? as u16 - 32;

          let mut decode_utf8_num = || {
            let b_0 = self.next()?;
            Ok(if b_0 >= 0xc0 {
              let b_1 = self.next()? as u16;
              (b_0 as u16 - 0xc0) << 6 | (b_1 - 0x80)
            } else {
              b_0.into()
            })
          };

          let col = decode_utf8_num()? - 32 - 1;
          let row = decode_utf8_num()? - 32 - 1;

          Ok(Event::Mouse(MouseEvent {
            row,
            col,
            button: match button & 0b11 {
              0 => Button::Left,
              1 => Button::Middle,
              2 => Button::Right,
              3 => Button::None,
              _ => unreachable!(),
            },
            shift: button & 4 != 0,
            alt: button & 8 != 0,
            control: button & 16 != 0,
          }))
        }
        b => Err(self.invalid(b)),
      },
      b => Err(self.invalid(b)),
    }
  }
}
