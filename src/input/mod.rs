mod event;
mod key;
mod parse;
pub mod qwerty;

use self::parse::ParseResult;
use std::collections::VecDeque;

pub use event::{Button, Event, MouseEvent};
pub use key::{Function, Key, KeySequence, KeyWithMods, Modifier, Modifiers};

#[derive(Default)]
pub struct Input {
  buf: VecDeque<u8>,
}

impl Input {
  pub fn new() -> Self { Input::default() }
  pub fn add_all(&mut self, iter: &[u8]) { self.buf.extend(iter); }

  pub fn parse(&mut self) -> Option<Event> {
    match self.buf.front().copied() {
      // TODO: We should wait here for more input, and after a timeout return an ESC keypress.
      Some(b'\x1b') if self.buf.len() == 1 => {
        self.buf.pop_front();
        Some(Event::KeyDown(Key::Char('\x1b')))
      }

      Some(b'\x1b') => match self.parse_escape() {
        ParseResult::Valid(ev, count) => {
          self.buf.drain(..count);
          Some(ev)
        }
        ParseResult::Invalid(count) => {
          self.buf.drain(..count);
          None
        }
        ParseResult::Waiting => None,
      },

      b => b.map(|b| {
        self.buf.pop_front();
        Event::KeyDown(Key::Char(b as char))
      }),
    }
  }
}

impl Iterator for &mut Input {
  type Item = Event;

  fn next(&mut self) -> Option<Self::Item> { self.parse() }
}
