use super::key::Key;

#[derive(Clone, Copy, Debug, PartialEq, Eq)]
pub enum Event {
  KeyUp(Key),
  KeyDown(Key),
  Mouse(MouseEvent),
}

#[derive(Clone, Copy, Debug, PartialEq, Eq)]
pub struct MouseEvent {
  pub row:     u16,
  pub col:     u16,
  pub button:  Button,
  pub shift:   bool,
  pub alt:     bool,
  pub control: bool,
}

#[derive(Clone, Copy, Debug, PartialEq, Eq)]
pub enum Button {
  Left,
  Middle,
  Right,
  None,
}
