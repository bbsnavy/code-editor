use core::fmt;

#[derive(Clone, Copy, Debug, PartialEq, Eq)]
pub enum Key {
  // Any numbers, lowercase letters, or symbols that can be typed with a single keypress.
  Char(char),

  Modifier(Modifier),
  Function(Function),

  Escape,
  Tab,
  Enter,
  Backspace,
  Delete,
}

#[derive(Clone, Copy, Debug, PartialEq, Eq)]
#[repr(u8)]
pub enum Modifier {
  LeftShift,
  RightShift,
  LeftControl,
  RightControl,
  LeftAlt,
  RightAlt,
}

// Any combination of `Modifier`
#[derive(Clone, Copy, Debug, Default, PartialEq, Eq)]
pub struct Modifiers {
  // NOTE: Keep synced with Modifier
  mask: [bool; 6],
}

#[derive(Clone, Copy, Debug, PartialEq, Eq)]
#[repr(u8)]
pub enum Function {
  F1,
  F2,
  F3,
  F4,
  F5,
  F6,
  F7,
  F8,
  F9,
  F10,
  F11,
  F12,
}

#[derive(Clone, Copy, Debug, PartialEq, Eq)]
pub struct KeyWithMods {
  pub key:  Key,
  pub mods: Modifiers,
}

impl Modifiers {
  pub fn set(&mut self, key: Modifier) { self.mask[key as usize] = true; }
  pub fn clear(&mut self, key: Modifier) { self.mask[key as usize] = false; }
  pub fn get(&self, key: Modifier) -> bool { self.mask[key as usize] }

  pub fn has_shift(&self) -> bool {
    self.get(Modifier::LeftShift) || self.get(Modifier::RightShift)
  }
  pub fn has_ctrl(&self) -> bool {
    self.get(Modifier::LeftControl) || self.get(Modifier::RightControl)
  }
  pub fn has_alt(&self) -> bool { self.get(Modifier::LeftAlt) || self.get(Modifier::RightAlt) }
}

#[derive(Default)]
pub struct KeySequence {
  pub seq: Vec<KeyWithMods>,
}

impl fmt::Display for KeySequence {
  fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
    for key in self.seq.iter() {
      write!(f, "{}", key)?;
    }
    Ok(())
  }
}

impl fmt::Display for KeyWithMods {
  fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
    if self.mods.has_ctrl() || self.mods.has_alt() {
      write!(f, "<")?;
    }
    if self.mods.has_ctrl() {
      write!(f, "C-")?;
    }
    if self.mods.has_alt() {
      write!(f, "A-")?;
    }
    write!(f, "{}", self.key)?;
    if self.mods.has_ctrl() || self.mods.has_alt() {
      write!(f, ">")?;
    }
    Ok(())
  }
}

impl fmt::Display for Key {
  fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
    match self {
      Key::Char(c) => write!(f, "{c}"),
      Key::Modifier(m) => write!(f, "{m}"),
      Key::Function(k) => write!(f, "{k}"),

      Key::Escape => write!(f, "ESC"),
      Key::Tab => write!(f, "TAB"),
      Key::Enter => write!(f, "ENTER"),
      Key::Backspace => write!(f, "BS"),
      Key::Delete => write!(f, "DEL"),
    }
  }
}

impl fmt::Display for Modifier {
  fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
    match self {
      Modifier::LeftShift => write!(f, "LSHIFT"),
      Modifier::RightShift => write!(f, "RSHIFT"),
      Modifier::LeftControl => write!(f, "LCTRL"),
      Modifier::RightControl => write!(f, "RCTRL"),
      Modifier::LeftAlt => write!(f, "LALT"),
      Modifier::RightAlt => write!(f, "RALT"),
    }
  }
}

impl fmt::Display for Function {
  fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
    match self {
      Function::F1 => write!(f, "F1"),
      Function::F2 => write!(f, "F2"),
      Function::F3 => write!(f, "F3"),
      Function::F4 => write!(f, "F4"),
      Function::F5 => write!(f, "F5"),
      Function::F6 => write!(f, "F6"),
      Function::F7 => write!(f, "F7"),
      Function::F8 => write!(f, "F8"),
      Function::F9 => write!(f, "F9"),
      Function::F10 => write!(f, "F10"),
      Function::F11 => write!(f, "F11"),
      Function::F12 => write!(f, "F12"),
    }
  }
}
