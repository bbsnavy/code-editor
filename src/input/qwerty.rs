use super::{Function, Key, Modifier};

pub fn key_from_scancode(scancode: u32) -> Option<Key> {
  Some(match scancode {
    1 => Key::Escape,

    59 => Key::Function(Function::F1),
    60 => Key::Function(Function::F2),
    61 => Key::Function(Function::F3),
    62 => Key::Function(Function::F4),
    63 => Key::Function(Function::F5),
    64 => Key::Function(Function::F6),
    65 => Key::Function(Function::F7),
    66 => Key::Function(Function::F8),
    67 => Key::Function(Function::F9),
    68 => Key::Function(Function::F10),
    87 => Key::Function(Function::F11),
    88 => Key::Function(Function::F12),

    41 => Key::Char('`'),
    2 => Key::Char('1'),
    3 => Key::Char('2'),
    4 => Key::Char('3'),
    5 => Key::Char('4'),
    6 => Key::Char('5'),
    7 => Key::Char('6'),
    8 => Key::Char('7'),
    9 => Key::Char('8'),
    10 => Key::Char('9'),
    11 => Key::Char('0'),
    12 => Key::Char('-'),
    13 => Key::Char('='),
    14 => Key::Backspace,

    15 => Key::Tab,
    16 => Key::Char('q'),
    17 => Key::Char('w'),
    18 => Key::Char('e'),
    19 => Key::Char('r'),
    20 => Key::Char('t'),
    21 => Key::Char('y'),
    22 => Key::Char('u'),
    23 => Key::Char('i'),
    24 => Key::Char('o'),
    25 => Key::Char('p'),
    26 => Key::Char('['),
    27 => Key::Char(']'),
    43 => Key::Char('\\'),

    30 => Key::Char('a'),
    31 => Key::Char('s'),
    32 => Key::Char('d'),
    33 => Key::Char('f'),
    34 => Key::Char('g'),
    35 => Key::Char('h'),
    36 => Key::Char('j'),
    37 => Key::Char('k'),
    38 => Key::Char('l'),
    39 => Key::Char(';'),
    40 => Key::Char('\''),
    28 => Key::Enter,

    42 => Key::Modifier(Modifier::LeftShift),
    44 => Key::Char('z'),
    45 => Key::Char('x'),
    46 => Key::Char('c'),
    47 => Key::Char('v'),
    48 => Key::Char('b'),
    49 => Key::Char('n'),
    50 => Key::Char('m'),
    51 => Key::Char(','),
    52 => Key::Char('.'),
    53 => Key::Char('/'),
    54 => Key::Modifier(Modifier::RightShift),

    29 => Key::Modifier(Modifier::LeftControl),
    56 => Key::Modifier(Modifier::LeftAlt),
    57 => Key::Char(' '),
    100 => Key::Modifier(Modifier::RightAlt),
    97 => Key::Modifier(Modifier::RightControl),

    _ => {
      dbg!(scancode);
      return None;
    }
  })
}
