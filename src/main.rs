// I prefer to not write an `as_any` function for every plugin :P
#![feature(trait_upcasting)]
// I would prefer to disallow this, but I can't use `type` aliases for traits,
// so I can't really simplify my types much.
#![allow(clippy::type_complexity)]

use std::{path::PathBuf, rc::Rc};

use event::Events;
use global::GlobalState;
use gui::GuiEvent;
use tab::Tabs;

mod editor;
mod event;
mod git;
mod global;
mod gui;
mod icon;
mod input;
mod location;
mod logger;
mod lsp;
mod nerdfonts;
mod tab;
mod terminal;
mod theme;
mod tree_sitter;

#[macro_use]
extern crate log;

fn setup_logger() {
  #[cfg(not(test))]
  log::set_boxed_logger(Box::new(logger::FileLogger::new())).unwrap();
  #[cfg(test)]
  log::set_boxed_logger(Box::new(logger::StdoutLogger::new())).unwrap();

  log::set_max_level(log::LevelFilter::Info);
}

fn main() {
  setup_logger();

  let mut gui = gui::Gui::new();
  // TODO: This can be used with multithreading to notify the main thread.
  let channel = gui.create_channel();

  let mut e = Events::new();

  let global = Rc::new(GlobalState::new());
  let mut tabs = Tabs::new(global.clone());
  tabs.add_default_tabs(&mut e);
  tabs.resize(gui.window_size());

  *global.lsp_client.borrow_mut() = Some(lsp::LSPConnection::new(&mut e));

  std::thread::spawn(move || {
    let mut events = [event::NONE; 10];

    loop {
      let n = e.poll(&mut events);

      for ev in &events[..n] {
        channel.send(GuiEvent::IOEvent(ev.clone()));
      }
    }
  });

  gui.run(tabs);

  // match res {
  //   Ok(()) => {}
  //   Err(e) => {
  //     println!("exiting due to {e}");
  //     std::process::exit(1);
  //   }
  // }
}

pub fn data_dir() -> PathBuf {
  let data = match std::env::var("XDG_DATA_HOME") {
    Ok(dir) => PathBuf::from(dir),
    Err(_) => {
      let home = std::env::var("HOME").unwrap_or_else(|_| panic!("couldn't find home directory"));
      PathBuf::from(home).join(".local").join("share")
    }
  };

  data.join("code-editor")
}

pub fn config_dir() -> PathBuf {
  let config = match std::env::var("XDG_CONFIG_HOME") {
    Ok(dir) => PathBuf::from(dir),
    Err(_) => {
      let home = std::env::var("HOME").unwrap_or_else(|_| panic!("couldn't find home directory"));
      PathBuf::from(home).join(".config")
    }
  };

  config.join("code-editor")
}
