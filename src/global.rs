use std::cell::RefCell;

use crate::lsp::LSPConnection;

/// Stores state for the entire editor, such as CWD, LSP connections, and
/// editors that aren't visible.
#[derive(Default)]
pub struct GlobalState {
  // TODO: This should really be a map of language id to clients.
  pub lsp_client: RefCell<Option<LSPConnection>>,
}

impl GlobalState {
  pub fn new() -> Self { GlobalState::default() }
}
