use crate::icon::Icon;

use super::{
  api::{Pos, Rect, Size},
  op::SetupOp,
  VulkanAPI,
};
use etagere::{size2, AllocId, AtlasAllocator};
use resvg::{usvg, usvg::TreeParsing, Tree};
use std::{collections::HashMap, sync::Arc};
use vulkano::{
  device::Device,
  format::Format,
  image::{ImageDimensions, StorageImage},
  memory::allocator::StandardMemoryAllocator,
};

pub struct IconRender {
  pub image: Arc<StorageImage>,

  atlas: AtlasAllocator,
  chars: HashMap<(Icon, u32), (AllocId, IconGlyph)>,
}

#[derive(Clone, Copy, Debug, PartialEq)]
pub struct IconGlyph {
  pub uv: Rect,
}

impl IconRender {
  pub fn new(device: Arc<Device>) -> Self {
    let alloc = StandardMemoryAllocator::new_default(device.clone());
    let image = StorageImage::new(
      &alloc,
      ImageDimensions::Dim2d { width: 2048, height: 2048, array_layers: 1 },
      Format::R8_UNORM,
      None,
    )
    .unwrap();

    Self { atlas: AtlasAllocator::new(size2(2048, 2048)), chars: HashMap::new(), image }
  }

  pub fn rasterize(
    &mut self,
    setup_ops: &mut Vec<SetupOp>,
    api: &mut VulkanAPI,
    icon: Icon,
    size: u32,
  ) -> IconGlyph {
    if let Some((_, g)) = self.chars.get(&(icon, size)) {
      return *g;
    }

    let width = 40;
    let height = 40;

    /*
    let (metrics, bitmap) = self.rasterize(icon, size as f32);
    */
    let bitmap = rasterize_icon(icon, size);

    // TODO: What happens when we run out of space?
    let alloc = self
      .atlas
      .allocate(size2(width as i32 + 1, height as i32 + 1))
      .expect("no space left in text atlas");

    // The rectangle returned from `alloc` is the entire region we got. This may be
    // larged than we requested, so we make this exact-sized rectangle for the
    // glyph.
    let uv = Rect {
      pos:  Pos { x: alloc.rectangle.min.x as u32, y: alloc.rectangle.min.y as u32 },
      size: Size { width: width as u32, height: height as u32 },
    };
    let glyph = IconGlyph { uv };

    self.chars.insert((icon, size), (alloc.id, glyph));
    if bitmap.len() > 0 {
      api.write_to_image(setup_ops, &self.image, glyph.uv, &bitmap);
    }

    glyph
  }
}

fn rasterize_icon(icon: Icon, size: u32) -> Vec<u8> {
  let tree = usvg::Tree::from_str(icon.svg(), &usvg::Options { ..Default::default() }).unwrap();
  let tree = Tree::from_usvg(&tree);

  let scale = size as f32 / tree.size.width();

  let size = size as usize;

  let mut buf = vec![0; size * size * 4];
  let mut pixmap =
    resvg::tiny_skia::PixmapMut::from_bytes(&mut buf, size as u32, size as u32).unwrap();

  tree.render(resvg::tiny_skia::Transform::from_scale(scale, scale), &mut pixmap);

  // We only care about the alpha channel, so pull that out.
  let mut grayscale = vec![0; size * size];
  for y in 0..size {
    for x in 0..size {
      let i = y * size + x;
      grayscale[i] = buf[i * 4 + 3];
    }
  }

  grayscale
}
