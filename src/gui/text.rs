use super::{
  api::{Color, Pos, Rect, Size},
  op::SetupOp,
  VulkanAPI,
};
use etagere::{euclid::Box2D, size2, AllocId, AtlasAllocator};
use fontdue::{Font, FontSettings, Metrics};
use std::{collections::HashMap, ops::RangeBounds, sync::Arc};
use vulkano::{
  device::Device,
  format::Format,
  image::{ImageDimensions, StorageImage},
  memory::allocator::StandardMemoryAllocator,
};

pub struct TextRender {
  pub font:      Font,
  pub font_bold: Font,
  pub image:     Arc<StorageImage>,

  atlas: AtlasAllocator,
  chars: HashMap<CharKey, (AllocId, Glyph)>,
}

#[derive(PartialEq, Eq, Hash)]
struct CharKey {
  ch:   char,
  bold: bool,
  size: u32,
}

#[derive(Clone, Copy, Debug, PartialEq)]
pub struct Glyph {
  pub uv:      Rect,
  pub metrics: Metrics,
}

#[derive(Debug, Clone, PartialEq, Eq)]
pub struct StringWithColors {
  cells: Vec<Cell>,
}

#[derive(Debug, Clone, Copy, PartialEq, Eq)]
pub struct Cell {
  pub ch:    char,
  pub color: Color,
  pub bold:  bool,
}

impl TextRender {
  pub fn new_from_file(file: &str, bold_file: &str, device: Arc<Device>) -> Self {
    let bytes = std::fs::read(file).unwrap();
    let font = Font::from_bytes(bytes, FontSettings::default()).unwrap();

    let bytes = std::fs::read(bold_file).unwrap();
    let font_bold = Font::from_bytes(bytes, FontSettings::default()).unwrap();

    let alloc = StandardMemoryAllocator::new_default(device.clone());
    let image = StorageImage::new(
      &alloc,
      ImageDimensions::Dim2d { width: 2048, height: 2048, array_layers: 1 },
      Format::R8_UNORM,
      None,
    )
    .unwrap();

    Self {
      font,
      font_bold,
      atlas: AtlasAllocator::new(size2(2048, 2048)),
      chars: HashMap::new(),
      image,
    }
  }

  pub fn rasterize(
    &mut self,
    setup_ops: &mut Vec<SetupOp>,
    api: &mut VulkanAPI,
    c: char,
    bold: bool,
    size: u32,
  ) -> Glyph {
    let key = CharKey { ch: c, bold, size };
    if let Some((_, g)) = self.chars.get(&key) {
      return *g;
    }

    let font = if bold { &self.font_bold } else { &self.font };
    let (metrics, bitmap) = font.rasterize(c, size as f32);
    let _bitmap = rasterize_funny(&metrics);

    if metrics.width == 0 || metrics.height == 0 {
      return Glyph {
        uv: Rect { pos: Pos { x: 0, y: 0 }, size: Size { width: 0, height: 0 } },
        metrics,
      };
    }

    // TODO: What happens when we run out of space?
    let alloc = self
      .atlas
      .allocate(size2(metrics.width as i32 + 1, metrics.height as i32 + 1))
      .expect("no space left in text atlas");

    // The rectangle returned from `alloc` is the entire region we got. This may be
    // larged than we requested, so we make this exact-sized rectangle for the
    // glyph.
    let uv = Rect {
      pos:  Pos { x: alloc.rectangle.min.x as u32, y: alloc.rectangle.min.y as u32 },
      size: Size { width: metrics.width as u32, height: metrics.height as u32 },
    };
    let glyph = Glyph { uv, metrics };

    self.chars.insert(key, (alloc.id, glyph));
    if bitmap.len() > 0 {
      api.write_to_image(setup_ops, &self.image, glyph.uv, &bitmap);
    }

    glyph
  }
}

fn rasterize_funny(metrics: &Metrics) -> Vec<u8> {
  let mut buf = vec![0; metrics.width * metrics.height];

  for y in 0..metrics.height {
    for x in 0..metrics.width {
      buf[y * metrics.width + x] = if (x + y) % 2 == 0 { 255 } else { 0 };
    }
  }

  buf
}

impl<T> From<Box2D<i32, T>> for Rect {
  fn from(rect: Box2D<i32, T>) -> Self {
    return Rect {
      pos:  Pos { x: rect.min.x as u32, y: rect.min.y as u32 },
      size: Size { width: rect.width() as u32, height: rect.height() as u32 },
    };
  }
}

impl StringWithColors {
  pub fn new() -> Self { StringWithColors::from_string(String::new()) }

  pub fn from_string(str: String) -> Self {
    StringWithColors { cells: str.chars().map(|c| Cell::from_char(c)).collect() }
  }

  pub fn from_string_solid_color(str: String, color: Color) -> Self {
    StringWithColors { cells: str.chars().map(|c| Cell::new(c, color)).collect() }
  }

  /// Clears this string, but keeps the length the same.
  pub fn clear(&mut self) { self.clear_range(..); }

  /// Clears the given range. If the range is larger than the string, this
  /// function will panic.
  #[track_caller]
  pub fn clear_range<R>(&mut self, range: R)
  where
    R: RangeBounds<usize>,
  {
    if self.len() == 0 {
      return;
    }

    let start = match range.start_bound() {
      std::ops::Bound::Included(&n) => n,
      std::ops::Bound::Excluded(&n) => n + 1,
      std::ops::Bound::Unbounded => 0,
    };
    if start >= self.len() {
      panic!("range start {start} is outside of 0..len = {}", self.len());
    }
    let end = match range.end_bound() {
      std::ops::Bound::Included(&n) => {
        if n + 1 >= self.len() {
          panic!("range end {} is outside of 0..len = {}", n + 1, self.len());
        }
        n + 1
      }
      std::ops::Bound::Excluded(&n) => {
        if n >= self.len() {
          panic!("range end {n} is outside of 0..len = {}", self.len());
        }
        n
      }
      std::ops::Bound::Unbounded => self.len(),
    };
    for cell in &mut self.cells[start..end] {
      *cell = Cell::default();
    }
  }

  pub fn replace_cell(&mut self, index: usize, cell: Cell) { self.cells[index] = cell; }

  pub fn len(&self) -> usize { self.cells.len() }
  pub fn is_empty(&self) -> bool { self.cells.is_empty() }

  pub fn chars(&self) -> impl Iterator<Item = Cell> + '_ { self.cells.iter().copied() }

  // The below functions change a StringWithColors length.

  pub fn push(&mut self, c: char, color: Color) { self.cells.push(Cell::new(c, color)); }
  pub fn resize(&mut self, len: usize) { self.cells.resize_with(len, || Cell::default()); }
  pub fn empty(&mut self) { self.cells.clear() }
}

impl From<char> for StringWithColors {
  fn from(value: char) -> Self { Self::from(value.to_string()) }
}
impl From<String> for StringWithColors {
  fn from(value: String) -> Self { Self::from_string(value) }
}

impl Default for Cell {
  fn default() -> Self { Cell { ch: ' ', color: 0xffffff.into(), bold: false } }
}

impl Cell {
  pub fn new(ch: char, color: Color) -> Self { Cell { ch, color, bold: false } }
  pub fn from_char(ch: char) -> Self { Cell { ch, color: 0xffffff.into(), bold: false } }
}
