//! This file defines `Op` and `GpuOp`, which stores and caches vertex buffers
//! for each draw call.

use std::fmt;
use vulkano::{
  buffer::{subbuffer::BufferWriteGuard, BufferContents, Subbuffer},
  command_buffer::CopyBufferToImageInfo,
  pipeline::graphics::vertex_input::Vertex,
};

use crate::icon::Icon;

use super::{
  api::{Color, Pos, Rect, Size},
  text::StringWithColors,
  RenderAPI, VulkanAPI,
};

#[derive(Debug, BufferContents, Vertex)]
#[repr(C)]
pub struct Vert {
  #[format(R32G32_UINT)]
  position: [u32; 2],
  #[format(R8G8B8_UINT)]
  color:    [u8; 3],
}

#[derive(Debug, BufferContents, Vertex)]
#[repr(C)]
pub struct VertUV {
  #[format(R32G32_UINT)]
  position: [u32; 2],
  #[format(R32G32_UINT)]
  uv:       [u32; 2],
  #[format(R8G8B8_UINT)]
  color:    [u8; 3],
}

macro_rules! ops {
  ( $($name:ident($gpu_value:ty) { $($field:ident: $ty:ty,)* };)* ) => {

    pub enum Op {
      $( $name($name), )*
    }

    pub enum GpuOp {
      $( $name($name, $gpu_value), )*
    }

    impl fmt::Debug for GpuOp {
      fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        match self {
          $( GpuOp::$name(_, _) => write!(f, stringify!($name)), )*
        }
      }
    }

    $(
      #[derive(Debug, PartialEq, Eq)]
      pub struct $name {
        $( pub $field: $ty, )*
      }
    )*

    impl Op {
      pub(super) fn render(self, api: &mut RenderAPI<'_>, p: Option<GpuOp>) -> GpuOp {
        match (self, p) {
          $(
            (Op::$name(op), Some(GpuOp::$name(prev_op, buf))) if op == prev_op => {
              GpuOp::$name(op, buf)
            }

            (Op::$name(op), prev) => {
              let buf = op.render(api, prev);

              GpuOp::$name(op, buf)
            }
          )*
        }
      }
    }
  };
}

ops! {
  DrawRectangle(Subbuffer<[Vert]>) {
    rect:  Rect,
    color: Color,
  };

  DrawText(GpuTextOp) {
    pos:  Pos,
    size: u32,
    text: StringWithColors,
  };

  DrawIcon(GpuTextOp) {
    pos:   Pos,
    size:  u32,
    icon:  Icon,
    color: Color,
  };
}

pub enum SetupOp {
  CopyBufferToImage(CopyBufferToImageInfo),
}

impl fmt::Debug for SetupOp {
  fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
    match self {
      SetupOp::CopyBufferToImage(_) => write!(f, "CopyBufferToImage"),
    }
  }
}

impl DrawRectangle {
  fn render(&self, api: &mut RenderAPI, prev: Option<GpuOp>) -> Subbuffer<[Vert]> {
    let mut buf: Subbuffer<[Vert]> = match prev {
      Some(GpuOp::DrawRectangle(_, buf)) => buf,
      _ => api.vulkan.vertex_allocator.allocate_unsized(4).unwrap(),
    };

    let x0 = self.rect.pos.x;
    let x1 = self.rect.pos.x + self.rect.size.width;
    let y0 = self.rect.pos.y;
    let y1 = self.rect.pos.y + self.rect.size.height;
    let color = [self.color.r, self.color.g, self.color.b];

    {
      let mut data = match buf.write() {
        Ok(d) => d,
        // If we couldn't lock the buffer for writing, it means that it is currently in use by
        // the GPU. Just allocate a new buffer in this case. Its around ~3x slower (from 500ns
        // to 1.5us) to do this allocation, but thats acceptable given how rarely it shows up
        // in practice.
        res @ Err(_) => {
          drop(res);
          buf = api.vulkan.vertex_allocator.allocate_unsized(4).unwrap();
          buf.write().unwrap()
        }
      };
      data[0] = Vert { position: [x0, y0], color };
      data[1] = Vert { position: [x1, y0], color };
      data[2] = Vert { position: [x1, y1], color };
      data[3] = Vert { position: [x0, y1], color };
    }

    buf
  }
}

#[derive(Debug, PartialEq, Eq)]
pub struct GpuTextOp {
  len:            u64,
  pub vertex_buf: Subbuffer<[VertUV]>,
  pub index_buf:  Subbuffer<[u32]>,
}

impl GpuTextOp {
  fn new(api: &mut VulkanAPI, len: u64) -> Self {
    GpuTextOp {
      len,
      vertex_buf: api.vertex_allocator.allocate_unsized(len * 4).unwrap(),
      index_buf: api.index_allocator.allocate_unsized(len * 6).unwrap(),
    }
  }

  pub fn resize(&mut self, api: &mut VulkanAPI, len: u64) {
    self.len = len;

    let needs_grow = self.vertex_len() > self.vertex_buf.len() as u64
      || self.index_len() > self.index_buf.len() as u64;

    if needs_grow {
      self.realloc(api);
    }
  }

  pub fn vertex_len(&self) -> u64 { self.len * 4 }
  /// The length of the index buffer that should be drawn. The actual index
  /// buffer may be longer.
  pub fn index_len(&self) -> u64 { self.len * 6 }

  fn write<'a>(&'a mut self, api: &mut VulkanAPI) -> TextData<'a> {
    let err = {
      let vertex_res = self.vertex_buf.write();
      let index_res = self.index_buf.write();
      vertex_res.is_err() || index_res.is_err()
    };

    if err {
      self.realloc(api);
    }

    TextData {
      len:         self.len,
      vertex_data: self.vertex_buf.write().unwrap(),
      index_data:  self.index_buf.write().unwrap(),
    }
  }

  fn realloc(&mut self, api: &mut VulkanAPI) {
    self.vertex_buf = api.vertex_allocator.allocate_unsized(self.vertex_len()).unwrap();
    self.index_buf = api.index_allocator.allocate_unsized(self.index_len()).unwrap();
  }
}

struct TextData<'a> {
  len:         u64,
  vertex_data: BufferWriteGuard<'a, [VertUV]>,
  index_data:  BufferWriteGuard<'a, [u32]>,
}

impl TextData<'_> {
  fn rect(&mut self, i: usize, pos: Rect, uv: Rect, color: [u8; 3]) {
    assert!(i < self.len as usize, "i: {}, len: {}", i, self.len);

    self.vertex_data[i * 4] =
      VertUV { position: pos.top_left().into(), uv: uv.top_left().into(), color };
    self.vertex_data[i * 4 + 1] =
      VertUV { position: pos.top_right().into(), uv: uv.top_right().into(), color };
    self.vertex_data[i * 4 + 2] =
      VertUV { position: pos.bot_left().into(), uv: uv.bot_left().into(), color };
    self.vertex_data[i * 4 + 3] =
      VertUV { position: pos.bot_right().into(), uv: uv.bot_right().into(), color };

    self.index_data[i * 6] = i as u32 * 4;
    self.index_data[i * 6 + 1] = i as u32 * 4 + 1;
    self.index_data[i * 6 + 2] = i as u32 * 4 + 3;
    self.index_data[i * 6 + 3] = i as u32 * 4;
    self.index_data[i * 6 + 4] = i as u32 * 4 + 2;
    self.index_data[i * 6 + 5] = i as u32 * 4 + 3;
  }
}

impl DrawText {
  fn render(&self, api: &mut RenderAPI, prev: Option<GpuOp>) -> GpuTextOp {
    let len = self.text.len() as u64;

    let mut gpu_op = match prev {
      Some(GpuOp::DrawText(_, gpu_op)) => gpu_op,
      _ => GpuTextOp::new(api.vulkan, len),
    };

    gpu_op.resize(api.vulkan, len);

    // if no horizontal metrics were found in the font, it cannot be rendered.
    let h_metrics = api.text.font.horizontal_line_metrics(self.size as f32).unwrap();

    let mut i = 0;
    let mut x = self.pos.x as f32;
    let baseline = self.pos.y as f32 + h_metrics.ascent;

    {
      let mut text_data = gpu_op.write(api.vulkan);

      for cell in self.text.chars() {
        let color = [cell.color.r, cell.color.g, cell.color.b];

        let glyph = api.rasterize_char(cell.ch, cell.bold, self.size);

        // The floor() is so that we don't anti-alias things twice, which can cause some
        // artifacting. We might want to remove if we want to position letters between
        // pixels.
        let x0 = (x + glyph.metrics.xmin as f32).floor() as u32;
        let x1 = x0 + glyph.metrics.width as u32;
        let y1 = (baseline - glyph.metrics.ymin as f32).floor() as u32;
        let y0 = y1 - glyph.metrics.height as u32;

        text_data.rect(
          i,
          Rect { pos: Pos { x: x0, y: y0 }, size: Size { width: x1 - x0, height: y1 - y0 } },
          glyph.uv,
          color,
        );

        x += glyph.metrics.advance_width;
        i += 1;
      }
    }

    gpu_op
  }
}

impl DrawIcon {
  fn render(&self, api: &mut RenderAPI, prev: Option<GpuOp>) -> GpuTextOp {
    let mut gpu_op = match prev {
      Some(GpuOp::DrawIcon(_, gpu_op)) => gpu_op,
      _ => GpuTextOp::new(api.vulkan, 1),
    };

    gpu_op.resize(api.vulkan, 1);

    {
      let mut text_data = gpu_op.write(api.vulkan);

      let color = [self.color.r, self.color.g, self.color.b];

      let glyph = api.rasterize_icon(self.icon, self.size);

      text_data.rect(
        0,
        Rect {
          pos:  self.pos,
          size: Size { width: glyph.uv.size.width, height: glyph.uv.size.height },
        },
        glyph.uv,
        color,
      );
    }

    gpu_op
  }
}
