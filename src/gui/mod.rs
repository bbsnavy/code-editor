use std::sync::Arc;
use std::env;
use vulkano::{
  buffer::{
    allocator::{SubbufferAllocator, SubbufferAllocatorCreateInfo},
    Buffer, BufferCreateInfo, BufferUsage,
  },
  command_buffer::{
    allocator::StandardCommandBufferAllocator, AutoCommandBufferBuilder, BufferImageCopy,
    CommandBufferUsage, CopyBufferToImageInfo, PrimaryAutoCommandBuffer, RenderPassBeginInfo,
    SubpassContents,
  },
  descriptor_set::{
    allocator::StandardDescriptorSetAllocator, PersistentDescriptorSet, WriteDescriptorSet,
  },
  device::{
    physical::PhysicalDeviceType, Device, DeviceCreateInfo, DeviceExtensions, Queue,
    QueueCreateInfo, QueueFlags,
  },
  image::{view::ImageView, ImageAccess, ImageUsage, StorageImage, SwapchainImage},
  instance::{Instance, InstanceCreateInfo},
  memory::allocator::{AllocationCreateInfo, MemoryUsage, StandardMemoryAllocator},
  pipeline::{
    graphics::{
      color_blend::ColorBlendState,
      input_assembly::{InputAssemblyState, PrimitiveTopology},
      vertex_input::Vertex,
      viewport::{Scissor, Viewport, ViewportState},
    },
    GraphicsPipeline, Pipeline,
  },
  render_pass::{Framebuffer, FramebufferCreateInfo, RenderPass, Subpass},
  sampler::{Filter, Sampler, SamplerAddressMode, SamplerCreateInfo},
  swapchain::{
    acquire_next_image, AcquireError, PresentMode, Surface, Swapchain, SwapchainCreateInfo,
    SwapchainCreationError, SwapchainPresentInfo,
  },
  sync::{self, FlushError, GpuFuture},
  VulkanLibrary,
};
use vulkano_win::VkSurfaceBuild;
use winit::{
  dpi::PhysicalSize,
  event::{Event, KeyboardInput, WindowEvent},
  event_loop::{ControlFlow, EventLoop, EventLoopBuilder, EventLoopProxy},
  window::{Window, WindowBuilder},
};

use crate::icon::Icon;

pub mod api;
pub mod icon;
pub mod text;

mod gpu_op_builder;
mod op;

use api::{Rect, Size};
use gpu_op_builder::GpuOpBuilder;
use icon::{IconGlyph, IconRender};
use op::{GpuOp, SetupOp};
use text::{Glyph, TextRender};

pub struct Gui {
  event_loop: Option<EventLoop<GuiEvent>>,
  api:        VulkanAPI,
  text:       TextRender,
  icon:       IconRender,

  setup_ops: Vec<SetupOp>,
  gpu_ops:   Vec<Option<GpuOp>>,
}

pub struct VulkanAPI {
  surface:             Arc<Surface>,
  device:              Arc<Device>,
  queue:               Arc<Queue>,
  swapchain:           Arc<Swapchain>,
  render_pass:         Arc<RenderPass>,
  pipeline_rect:       Arc<GraphicsPipeline>,
  pipeline_text:       Arc<GraphicsPipeline>,
  text_descriptor_set: Arc<PersistentDescriptorSet>,
  icon_descriptor_set: Arc<PersistentDescriptorSet>,
  viewport:            Viewport,
  framebuffers:        Vec<Arc<Framebuffer>>,

  #[allow(unused)]
  memory_allocator:         Arc<StandardMemoryAllocator>,
  command_buffer_allocator: StandardCommandBufferAllocator,
  vertex_allocator:         SubbufferAllocator,
  index_allocator:          SubbufferAllocator,
}

pub struct RenderAPI<'a> {
  /// A list of ops to do before a render pass starts, such as uploading image
  /// data to the GPU.
  ///
  /// This is cleared every frame.
  setup_ops: &'a mut Vec<SetupOp>,

  /// A list of operations to perform within a render pass. This is kept between
  /// frames, to avoid re-creating vertex buffers and such.
  gpu_ops: &'a mut Vec<Option<GpuOp>>,

  vulkan: &'a mut VulkanAPI,
  text:   &'a mut TextRender,
  icon:   &'a mut IconRender,
}

/// A channel to send events to the render thread.
#[allow(unused)]
pub struct Channel {
  proxy: EventLoopProxy<GuiEvent>,
}

#[allow(unused)]
pub enum GuiEvent {
  /// Re-draws the window.
  Redraw,

  // Ideally, we would just use one thread. But winit doesn't let us listen on arbitrary streams
  // in their event loop, so we just use a second thread to send these events over.
  IOEvent(crate::event::Event),
}

impl Gui {
  pub fn new() -> Self {
    let library = VulkanLibrary::new().unwrap();
    let required_extensions = vulkano_win::required_extensions(&library);
    let instance = Instance::new(
      library,
      InstanceCreateInfo {
        enabled_extensions: required_extensions,
        // Enable enumerating devices that use non-conformant Vulkan implementations. (e.g.
        // MoltenVK)
        enumerate_portability: true,
        ..Default::default()
      },
    )
    .unwrap();

    let event_loop = EventLoopBuilder::with_user_event().build();
    let surface = WindowBuilder::new()
      .with_inner_size(winit::dpi::Size::Physical(PhysicalSize { width: 1920, height: 1080 }))
      .build_vk_surface(&event_loop, instance.clone())
      .unwrap();

    let (device, queue) = find_device(&instance, &surface);

    let (swapchain, images) = {
      let surface_capabilities =
        device.physical_device().surface_capabilities(&surface, Default::default()).unwrap();

      let image_format =
        Some(device.physical_device().surface_formats(&surface, Default::default()).unwrap()[0].0);
      let window = surface.object().unwrap().downcast_ref::<Window>().unwrap();

      // Prefer immediate, and fallback to fifo if not supported.
      let present_mode = device
        .physical_device()
        .surface_present_modes(&surface)
        .unwrap()
        .find(|v| *v == PresentMode::Immediate)
        .unwrap_or(PresentMode::Fifo);
      println!("Using present mode {present_mode:?}");

      Swapchain::new(
        device.clone(),
        surface.clone(),
        SwapchainCreateInfo {
          min_image_count: surface_capabilities.min_image_count,
          image_format,
          image_extent: window.inner_size().into(),
          image_usage: ImageUsage::COLOR_ATTACHMENT,
          present_mode,
          composite_alpha: surface_capabilities
            .supported_composite_alpha
            .into_iter()
            .next()
            .unwrap(),
          ..Default::default()
        },
      )
      .unwrap()
    };

    let rect_vs = rect_vs::load(device.clone()).unwrap();
    let rect_fs = rect_fs::load(device.clone()).unwrap();
    let text_vs = text_vs::load(device.clone()).unwrap();
    let text_fs = text_fs::load(device.clone()).unwrap();

    let render_pass = vulkano::single_pass_renderpass!(
      device.clone(),
      attachments: {
        color: {
          load: Clear,
          store: Store,
          format: swapchain.image_format(),
          samples: 1,
        },
      },
      pass: {
        color: [color],
        depth_stencil: {},
      },
    )
    .unwrap();

    // Pipeline setup

    let descriptor_set_allocator = StandardDescriptorSetAllocator::new(device.clone());

    let pipeline_rect = GraphicsPipeline::start()
      .render_pass(Subpass::from(render_pass.clone(), 0).unwrap())
      .vertex_input_state(op::Vert::per_vertex())
      .input_assembly_state(InputAssemblyState::new().topology(PrimitiveTopology::TriangleFan))
      .vertex_shader(rect_vs.entry_point("main").unwrap(), ())
      .viewport_state(ViewportState::viewport_dynamic_scissor_dynamic(1))
      .fragment_shader(rect_fs.entry_point("main").unwrap(), ())
      .build(device.clone())
      .unwrap();

    let subpass = Subpass::from(render_pass.clone(), 0).unwrap();
    let pipeline_text = GraphicsPipeline::start()
      .vertex_input_state(op::VertUV::per_vertex())
      .input_assembly_state(InputAssemblyState::new().topology(PrimitiveTopology::TriangleList))
      .vertex_shader(text_vs.entry_point("main").unwrap(), ())
      .viewport_state(ViewportState::viewport_dynamic_scissor_dynamic(1))
      .fragment_shader(text_fs.entry_point("main").unwrap(), ())
      .color_blend_state(ColorBlendState::new(subpass.num_color_attachments()).blend_alpha())
      .render_pass(subpass)
      .build(device.clone())
      .unwrap();

    // Text setup

    let sampler = Sampler::new(
      device.clone(),
      SamplerCreateInfo {
        mag_filter: Filter::Nearest,
        min_filter: Filter::Nearest,
        address_mode: [SamplerAddressMode::Repeat; 3],
        ..Default::default()
      },
    )
    .unwrap();

    let text = TextRender::new_from_file(
      env::var("FONT").unwrap().as_str(),
      env::var("FONT_BOLD").unwrap().as_str(),
      device.clone(),
    );
    let image_view = ImageView::new_default(text.image.clone()).unwrap();

    let layout = pipeline_text.layout().set_layouts().get(0).unwrap();
    let text_descriptor_set = PersistentDescriptorSet::new(
      &descriptor_set_allocator,
      layout.clone(),
      [WriteDescriptorSet::image_view_sampler(0, image_view, sampler.clone())],
    )
    .unwrap();

    // Icon setup

    let sampler = Sampler::new(
      device.clone(),
      SamplerCreateInfo {
        mag_filter: Filter::Nearest,
        min_filter: Filter::Nearest,
        address_mode: [SamplerAddressMode::Repeat; 3],
        ..Default::default()
      },
    )
    .unwrap();

    let icon = IconRender::new(device.clone());
    let image_view = ImageView::new_default(icon.image.clone()).unwrap();

    let icon_descriptor_set = PersistentDescriptorSet::new(
      &descriptor_set_allocator,
      layout.clone(),
      [WriteDescriptorSet::image_view_sampler(0, image_view, sampler)],
    )
    .unwrap();

    let mut viewport =
      Viewport { origin: [0.0, 0.0], dimensions: [0.0, 0.0], depth_range: 0.0..1.0 };

    let framebuffers = make_framebuffers(&images, render_pass.clone(), &mut viewport);

    let memory_allocator = Arc::new(StandardMemoryAllocator::new_default(device.clone()));
    let command_buffer_allocator =
      StandardCommandBufferAllocator::new(device.clone(), Default::default());
    let vertex_allocator = SubbufferAllocator::new(
      memory_allocator.clone(),
      SubbufferAllocatorCreateInfo {
        buffer_usage: BufferUsage::VERTEX_BUFFER,
        memory_usage: MemoryUsage::Upload,
        ..Default::default()
      },
    );
    let index_allocator = SubbufferAllocator::new(
      memory_allocator.clone(),
      SubbufferAllocatorCreateInfo {
        buffer_usage: BufferUsage::INDEX_BUFFER,
        memory_usage: MemoryUsage::Upload,
        ..Default::default()
      },
    );

    Gui {
      event_loop: Some(event_loop),
      text,
      icon,
      setup_ops: vec![],
      gpu_ops: vec![],

      api: VulkanAPI {
        surface,
        device,
        queue,
        swapchain,
        render_pass,
        pipeline_rect,
        pipeline_text,
        text_descriptor_set,
        icon_descriptor_set,
        viewport,
        framebuffers,
        memory_allocator,
        command_buffer_allocator,
        vertex_allocator,
        index_allocator,
      },
    }
  }

  pub fn create_channel(&mut self) -> Channel {
    let proxy = self.event_loop.as_mut().unwrap().create_proxy();
    Channel { proxy }
  }

  pub fn run(mut self, mut handler: impl RenderHandler + 'static) -> ! {
    let mut recreate_swapchain = false;

    let mut previous_frame_end = Some(sync::now(self.api.device.clone()).boxed());

    self.event_loop.take().unwrap().run(move |event, _, control_flow| match event {
      Event::WindowEvent { event: WindowEvent::CloseRequested, .. } => {
        *control_flow = ControlFlow::Exit;
      }
      Event::WindowEvent { event: WindowEvent::Resized(_), .. } => {
        handler.resize(self.window_size());
        recreate_swapchain = true;
      }
      Event::WindowEvent { event: WindowEvent::KeyboardInput { input, .. }, .. } => {
        handler.keyboard(input);
      }
      Event::RedrawEventsCleared => {
        if handler.needs_render() {
          self.draw(&mut handler, &mut recreate_swapchain, &mut previous_frame_end);
        }
        match handler.should_exit() {
          true => *control_flow = ControlFlow::Exit,
          false => *control_flow = ControlFlow::Wait,
        }
      }
      Event::UserEvent(e) => {
        handler.user_event(e);
      }
      Event::LoopDestroyed => {
        handler.shutdown();
      }
      _ => {}
    });
  }

  pub fn window_size(&self) -> Size {
    let window = self.api.surface.object().unwrap().downcast_ref::<Window>().unwrap();
    let dimensions = window.inner_size();
    Size { width: dimensions.width, height: dimensions.height }
  }

  fn draw(
    &mut self,
    handler: &mut impl RenderHandler,
    recreate_swapchain: &mut bool,
    previous_frame_end: &mut Option<Box<dyn GpuFuture>>,
  ) {
    // Do not draw the frame when the screen dimensions are zero. On Windows, this
    // can occur when minimizing the application.
    let dimensions = self.window_size();
    if dimensions.width == 0 || dimensions.height == 0 {
      return;
    }

    // It is important to call this function from time to time, otherwise resources
    // will keep accumulating and you will eventually reach an out of memory error.
    // Calling this function polls various fences in order to determine what the GPU
    // has already processed, and frees the resources that are no longer needed.
    previous_frame_end.as_mut().unwrap().cleanup_finished();

    if *recreate_swapchain {
      if self.api.recreate_swapchain(dimensions) {
        return;
      }

      *recreate_swapchain = false;
    }

    let (framebuffer_index, suboptimal, acquire_future) =
      match acquire_next_image(self.api.swapchain.clone(), None) {
        Ok(r) => r,
        Err(AcquireError::OutOfDate) => {
          *recreate_swapchain = true;
          return;
        }
        Err(e) => panic!("failed to acquire next image: {e}"),
      };

    // `acquire_next_image` can be successful, but suboptimal. This means that the
    // swapchain image will still work, but it may not display correctly. With some
    // drivers this can be when the window resizes, but it may not cause the
    // swapchain to become out of date.
    if suboptimal {
      *recreate_swapchain = true;
    }

    // do layout and rendering
    let render_api = RenderAPI {
      setup_ops: &mut self.setup_ops,
      gpu_ops:   &mut self.gpu_ops,
      vulkan:    &mut self.api,
      text:      &mut self.text,
      icon:      &mut self.icon,
    };
    let mut ctx = api::Ctx::new(render_api);
    handler.render(&mut ctx);
    let mut render_api = ctx.finish();

    // If setup ops are non empty, we need to upload some data to an image on the
    // GPU. This image is likely in use by the current frame, so lets wait for the
    // last frame to render.
    if !render_api.setup_ops.is_empty() {
      // droping this future blocks the curret thread until the frame finishes.
      previous_frame_end.take();
    }

    let command_buffer = render_api.build_command_buffer(framebuffer_index);

    let future = match previous_frame_end.take() {
      Some(fut) => fut.join(acquire_future).boxed(),
      None => acquire_future.boxed(),
    };
    let future = future.then_execute(self.api.queue.clone(), command_buffer).unwrap();
    let future = future.then_swapchain_present(
      self.api.queue.clone(),
      SwapchainPresentInfo::swapchain_image_index(self.api.swapchain.clone(), framebuffer_index),
    );

    match future.then_signal_fence_and_flush() {
      Ok(future) => {
        *previous_frame_end = Some(future.boxed());
        self.setup_ops.clear();
      }

      Err(e) => {
        match e {
          FlushError::OutOfDate => {}
          _ => println!("failed to flush future: {e}"),
        }
        *recreate_swapchain = true;
        *previous_frame_end = Some(sync::now(self.api.device.clone()).boxed());
      }
    }
  }
}

fn find_device(instance: &Arc<Instance>, surface: &Arc<Surface>) -> (Arc<Device>, Arc<Queue>) {
  let device_extensions = DeviceExtensions { khr_swapchain: true, ..DeviceExtensions::empty() };

  let (physical_device, queue_family_index) = instance
    .enumerate_physical_devices()
    .unwrap()
    .filter(|p| p.supported_extensions().contains(&device_extensions))
    .filter_map(|p| {
      p.queue_family_properties()
        .iter()
        .enumerate()
        .position(|(i, q)| {
          q.queue_flags.intersects(QueueFlags::GRAPHICS)
            && p.surface_support(i as u32, &surface).unwrap_or(false)
        })
        .map(|i| (p, i as u32))
    })
    .min_by_key(|(p, _)| {
      // We assign a lower score to device types that are likely to be faster/better.
      match p.properties().device_type {
        PhysicalDeviceType::DiscreteGpu => 0,
        PhysicalDeviceType::IntegratedGpu => 1,
        PhysicalDeviceType::VirtualGpu => 2,
        PhysicalDeviceType::Cpu => 3,
        PhysicalDeviceType::Other => 4,
        _ => 5,
      }
    })
    .expect("no suitable physical device found");

  // Some little debug infos.
  println!(
    "Using device: {} (type: {:?})",
    physical_device.properties().device_name,
    physical_device.properties().device_type,
  );

  let (device, mut queues) = Device::new(
    physical_device,
    DeviceCreateInfo {
      enabled_extensions: device_extensions,
      queue_create_infos: vec![QueueCreateInfo { queue_family_index, ..Default::default() }],
      ..Default::default()
    },
  )
  .unwrap();

  let queue = queues.next().unwrap();

  (device, queue)
}

mod rect_vs {
  vulkano_shaders::shader! {
    ty: "vertex",
    src: r"
      #version 450

      layout(push_constant) uniform PushConstants {
        uvec2 screen_size;
      } pc;

      layout(location = 0) in uvec2 position;
      layout(location = 1) in uvec3 color;

      layout(location = 0) out vec3 f_color;

      void main() {
        f_color = color / 255.0;
        gl_Position = vec4(vec2(position) / pc.screen_size * 2 - vec2(1.0, 1.0), 0.0, 1.0);
      }
    ",
  }
}

mod rect_fs {
  vulkano_shaders::shader! {
    ty: "fragment",
    src: r"
      #version 450

      layout(location = 0) in vec3 color;

      layout(location = 0) out vec4 f_color;

      void main() {
        f_color = vec4(color, 1.0);
      }
    ",
  }
}

mod text_vs {
  vulkano_shaders::shader! {
    ty: "vertex",
    src: r"
      #version 450

      layout(push_constant) uniform PushConstants {
        uvec2 screen_size;
      } pc;

      // these are in pixel coordates, it just includes subpixel positions.
      layout(location = 0) in uvec2 position;
      layout(location = 1) in uvec2 uv;
      layout(location = 2) in uvec3 color;

      layout(location = 0) out vec2 f_uv;
      layout(location = 1) out vec3 f_color;

      void main() {
        f_uv = uv / 2048.0;
        f_color = color / 255.0;
        gl_Position = vec4(vec2(position) / pc.screen_size * 2 - vec2(1.0, 1.0), 0.0, 1.0);
      }
    ",
  }
}

mod text_fs {
  vulkano_shaders::shader! {
    ty: "fragment",
    src: r"
      #version 450

      layout(location = 0) in vec2 uv;
      layout(location = 1) in vec3 color;

      layout(location = 0) out vec4 f_color;

      layout(set = 0, binding = 0) uniform sampler2D tex;

      void main() {
        float alpha = texture(tex, uv).r;
        f_color = vec4(color, alpha);
      }
    ",
  }
}

/// This function is called once during initialization, then again whenever the
/// window is resized.
fn make_framebuffers(
  images: &[Arc<SwapchainImage>],
  render_pass: Arc<RenderPass>,
  viewport: &mut Viewport,
) -> Vec<Arc<Framebuffer>> {
  let dimensions = images[0].dimensions().width_height();
  viewport.dimensions = [dimensions[0] as f32, dimensions[1] as f32];

  images
    .iter()
    .map(|image| {
      let view = ImageView::new_default(image.clone()).unwrap();
      Framebuffer::new(
        render_pass.clone(),
        FramebufferCreateInfo { attachments: vec![view], ..Default::default() },
      )
      .unwrap()
    })
    .collect::<Vec<_>>()
}

impl VulkanAPI {
  /// Returns true if we need to re-render.
  pub fn recreate_swapchain(&mut self, dimensions: Size) -> bool {
    let (new_swapchain, new_images) = match self.swapchain.recreate(SwapchainCreateInfo {
      image_extent: dimensions.into(),
      ..self.swapchain.create_info()
    }) {
      Ok(r) => r,
      // This error tends to happen when the user is manually resizing the
      // window. Simply restarting the loop is the easiest way to fix this
      // issue.
      Err(SwapchainCreationError::ImageExtentNotSupported { .. }) => return true,
      Err(e) => panic!("failed to recreate swapchain: {e}"),
    };

    self.swapchain = new_swapchain;

    self.framebuffers =
      make_framebuffers(&new_images, self.render_pass.clone(), &mut self.viewport);

    false
  }

  pub fn create_auto_command_buffer(
    &mut self,
  ) -> AutoCommandBufferBuilder<PrimaryAutoCommandBuffer> {
    AutoCommandBufferBuilder::primary(
      &self.command_buffer_allocator,
      self.queue.queue_family_index(),
      CommandBufferUsage::OneTimeSubmit,
    )
    .unwrap()
  }

  pub fn write_to_image(
    &mut self,
    setup_ops: &mut Vec<op::SetupOp>,
    image: &Arc<StorageImage>,
    rect: Rect,
    image_data: &[u8],
  ) {
    let buffer = Buffer::from_iter(
      &self.memory_allocator,
      BufferCreateInfo { usage: BufferUsage::TRANSFER_SRC, ..Default::default() },
      AllocationCreateInfo { usage: MemoryUsage::Upload, ..Default::default() },
      image_data.into_iter().copied(),
    )
    .unwrap();

    setup_ops.push(op::SetupOp::CopyBufferToImage(CopyBufferToImageInfo {
      regions: [BufferImageCopy {
        image_subresource: image.subresource_layers(),
        image_extent: [rect.size.width, rect.size.height, 1],
        image_offset: [rect.pos.x, rect.pos.y, 0],
        ..Default::default()
      }]
      .into(),
      ..CopyBufferToImageInfo::buffer_image(buffer, image.clone())
    }));
  }
}

impl RenderAPI<'_> {
  pub fn grow_to(&mut self, size: usize) {
    if size > self.gpu_ops.len() {
      self.gpu_ops.resize_with(size, || None);
    }
  }
  pub fn truncate_to(&mut self, size: usize) { self.gpu_ops.truncate(size); }

  pub fn rasterize_char(&mut self, c: char, bold: bool, size: u32) -> Glyph {
    self.text.rasterize(&mut self.setup_ops, self.vulkan, c, bold, size)
  }

  pub fn rasterize_icon(&mut self, icon: Icon, size: u32) -> IconGlyph {
    self.icon.rasterize(&mut self.setup_ops, self.vulkan, icon, size)
  }

  fn build_command_buffer(&mut self, framebuffer_index: u32) -> PrimaryAutoCommandBuffer {
    let mut builder = self.vulkan.create_auto_command_buffer();

    for op in self.setup_ops.iter() {
      match op {
        op::SetupOp::CopyBufferToImage(info) => {
          builder.copy_buffer_to_image(info.clone()).unwrap();
        }
      }
    }

    // begin render pass
    builder
      .begin_render_pass(
        RenderPassBeginInfo {
          // matches the `Clear` arg to `load` in the render pass
          clear_values: vec![Some([0.1, 0.1, 0.1, 1.0].into())],
          ..RenderPassBeginInfo::framebuffer(
            self.vulkan.framebuffers[framebuffer_index as usize].clone(),
          )
        },
        SubpassContents::Inline,
      )
      .unwrap();

    // setup
    builder
      .set_viewport(0, [self.vulkan.viewport.clone()])
      .set_scissor(
        0,
        [Scissor {
          origin:     [0, 0],
          dimensions: [
            self.vulkan.viewport.dimensions[0] as u32,
            self.vulkan.viewport.dimensions[1] as u32,
          ],
        }],
      )
      .push_constants(
        self.vulkan.pipeline_rect.layout().clone(),
        0,
        [self.vulkan.viewport.dimensions[0] as u32, self.vulkan.viewport.dimensions[1] as u32],
      );

    let mut gpu_op_builder = GpuOpBuilder::new(self.vulkan, &mut builder);
    for op in self.gpu_ops.iter().flatten() {
      gpu_op_builder.build(op);
    }

    // end
    builder.end_render_pass().unwrap();

    builder.build().unwrap()
  }
}

#[allow(unused)]
impl Channel {
  pub fn send(&self, event: GuiEvent) { self.proxy.send_event(event); }
}

pub trait RenderHandler {
  fn keyboard(&mut self, input: KeyboardInput);
  fn render(&mut self, ctx: &mut api::Ctx);
  fn needs_render(&self) -> bool;
  fn user_event(&mut self, event: GuiEvent);
  fn resize(&mut self, size: Size);
  fn should_exit(&self) -> bool;
  fn shutdown(&mut self);
}
