use crate::icon::Icon;

use super::{
  op::{DrawIcon, DrawRectangle, DrawText, Op},
  text::StringWithColors,
  RenderAPI,
};

pub struct Ctx<'a> {
  api: RenderAPI<'a>,

  current_op: usize,
}

// Util things

#[derive(Clone, Copy, Debug, PartialEq, Eq)]
pub struct Pos {
  pub x: u32,
  pub y: u32,
}

#[derive(Clone, Copy, Debug, PartialEq, Eq)]
pub struct Size {
  pub width:  u32,
  pub height: u32,
}

#[derive(Clone, Copy, Debug, PartialEq, Eq)]
pub struct Rect {
  pub pos:  Pos,
  pub size: Size,
}

#[derive(Clone, Copy, Debug, PartialEq, Eq)]
pub struct Color {
  pub r: u8,
  pub g: u8,
  pub b: u8,
}

impl Color {
  pub const fn from_hex(hex: u32) -> Self {
    Color { r: (hex >> 16) as u8, g: (hex >> 8) as u8, b: hex as u8 }
  }
}

impl From<u32> for Color {
  fn from(value: u32) -> Self { Color::from_hex(value) }
}

impl From<Pos> for [u32; 2] {
  fn from(pos: Pos) -> Self { [pos.x, pos.y] }
}
impl From<Size> for [u32; 2] {
  fn from(size: Size) -> Self { [size.width, size.height] }
}

impl<'a> Ctx<'a> {
  pub fn new(api: RenderAPI<'a>) -> Self { Self { api, current_op: 0 } }

  pub fn draw_rectangle(
    &mut self,
    x: u32,
    y: u32,
    width: u32,
    height: u32,
    color: impl Into<Color>,
  ) {
    self.op(Op::DrawRectangle(DrawRectangle {
      rect:  Rect { pos: Pos { x, y }, size: Size { width, height } },
      color: color.into(),
    }));
  }

  pub fn draw_text(&mut self, x: u32, y: u32, text: impl Into<StringWithColors>, size: u32) {
    let text = text.into();
    if !text.is_empty() {
      self.op(Op::DrawText(DrawText { pos: Pos { x, y }, text, size }));
    }
  }

  /// Draws an icon, width the top-left corner at (x, y). The width and height
  /// will be `size` pixels.
  pub fn draw_icon(&mut self, x: u32, y: u32, icon: Icon, size: u32, color: impl Into<Color>) {
    self.op(Op::DrawIcon(DrawIcon { pos: Pos { x, y }, icon, size, color: color.into() }));
  }

  fn op(&mut self, op: Op) {
    self.api.grow_to(self.current_op + 1);
    let p = self.api.gpu_ops[self.current_op].take();

    let gpu_op = op.render(&mut self.api, p);

    self.api.gpu_ops[self.current_op] = Some(gpu_op);
    self.current_op += 1;
  }

  pub fn finish(mut self) -> RenderAPI<'a> {
    self.api.truncate_to(self.current_op);
    self.api
  }
}

impl Rect {
  pub fn top_left(&self) -> Pos { self.pos }
  pub fn top_right(&self) -> Pos { Pos { x: self.pos.x + self.size.width, y: self.pos.y } }
  pub fn bot_left(&self) -> Pos { Pos { x: self.pos.x, y: self.pos.y + self.size.height } }
  pub fn bot_right(&self) -> Pos {
    Pos { x: self.pos.x + self.size.width, y: self.pos.y + self.size.height }
  }
}
