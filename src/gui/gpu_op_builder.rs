//! This file converts `op::GpuOp` into vulkan commands.

use vulkano::{
  command_buffer::{AutoCommandBufferBuilder, PrimaryAutoCommandBuffer},
  pipeline::{Pipeline, PipelineBindPoint},
};

use super::{op::GpuOp, VulkanAPI};

pub struct GpuOpBuilder<'a> {
  vulkan:  &'a mut VulkanAPI,
  builder: &'a mut AutoCommandBufferBuilder<PrimaryAutoCommandBuffer>,

  pipeline:       Option<PipelineVariant>,
  descriptor_set: Option<DescriptorSetVariant>,
}

#[derive(Copy, Clone, PartialEq, Eq)]
enum PipelineVariant {
  Rect,
  Text,
}

#[derive(Copy, Clone, PartialEq, Eq)]
enum DescriptorSetVariant {
  Text,
  Icon,
}

impl<'a> GpuOpBuilder<'a> {
  pub fn new(
    vulkan: &'a mut VulkanAPI,
    builder: &'a mut AutoCommandBufferBuilder<PrimaryAutoCommandBuffer>,
  ) -> Self {
    Self { builder, vulkan, pipeline: None, descriptor_set: None }
  }

  fn set_pipeline(&mut self, pipeline: PipelineVariant) {
    if self.pipeline != Some(pipeline) {
      self.builder.bind_pipeline_graphics(match pipeline {
        PipelineVariant::Rect => self.vulkan.pipeline_rect.clone(),
        PipelineVariant::Text => self.vulkan.pipeline_text.clone(),
      });
      self.pipeline = Some(pipeline);
    }
  }
  fn set_descriptor_set(&mut self, descriptor_set: DescriptorSetVariant) {
    if self.descriptor_set != Some(descriptor_set) {
      self.builder.bind_descriptor_sets(
        PipelineBindPoint::Graphics,
        self.vulkan.pipeline_text.layout().clone(),
        0,
        match descriptor_set {
          DescriptorSetVariant::Text => self.vulkan.text_descriptor_set.clone(),
          DescriptorSetVariant::Icon => self.vulkan.icon_descriptor_set.clone(),
        },
      );
      self.descriptor_set = Some(descriptor_set);
    }
  }

  pub fn build(&mut self, op: &GpuOp) {
    match op {
      GpuOp::DrawRectangle(_, buf) => {
        self.set_pipeline(PipelineVariant::Rect);
        self.descriptor_set = None;

        self.builder.bind_vertex_buffers(0, buf.clone()).draw(buf.len() as u32, 1, 0, 0).unwrap();
      }
      GpuOp::DrawText(_, gpu_op) => {
        self.set_pipeline(PipelineVariant::Text);
        self.set_descriptor_set(DescriptorSetVariant::Text);

        self
          .builder
          .bind_vertex_buffers(0, gpu_op.vertex_buf.clone())
          .bind_index_buffer(gpu_op.index_buf.clone())
          .draw_indexed(gpu_op.index_len() as u32, 1, 0, 0, 0)
          .unwrap();
      }
      GpuOp::DrawIcon(_, gpu_op) => {
        self.set_pipeline(PipelineVariant::Text);
        self.set_descriptor_set(DescriptorSetVariant::Icon);

        self
          .builder
          .bind_vertex_buffers(0, gpu_op.vertex_buf.clone())
          .bind_index_buffer(gpu_op.index_buf.clone())
          .draw_indexed(gpu_op.index_len() as u32, 1, 0, 0, 0)
          .unwrap();
      }
    }
  }
}
