#[derive(Debug, PartialEq, Eq)]
pub struct Diff {
  pub lines: Vec<(u64, LineDiff)>,
}

#[derive(Debug, PartialEq, Eq)]
pub enum LineDiff {
  Add,
  Remove,
  Change,
}

const MAX_SEARCH: usize = 50;

pub fn diff(a: &str, b: &str) -> Diff {
  let mut lines = vec![];

  let mut a_lines = a.lines();
  let mut b_lines = b.lines().enumerate();

  let mut last_line = 0;

  loop {
    let a_line = a_lines.clone().next();
    let b_line = b_lines.clone().next();

    if let Some((line_num, _)) = b_line {
      last_line = line_num as u64;
    }

    let diff = match (a_line, b_line) {
      (None, None) => break,
      (Some(_), None) => {
        // insert one remove at the end
        lines.push((last_line + 1, LineDiff::Remove));
        break;
      }
      (None, Some((line_num, _))) => {
        b_lines.next();
        (line_num as u64, LineDiff::Add)
      }
      (Some(a), Some((_, b))) if a == b => {
        a_lines.next();
        b_lines.next();
        continue;
      }
      (Some(_), Some((line_num, _))) => {
        // mismatch found, try to reconcile
        let mut found = (0, 0);

        // compare look aheads like so:
        // 0-0 lookahead = 0
        //
        // 0-1
        // 1-0
        // 1-1 lookahead = 1
        //
        // 2-0
        // 0-2
        // 2-1
        // 1-2
        // 2-2 lookahead = 2
        'outer: for lookahead in 0..MAX_SEARCH {
          for i in 0..lookahead {
            let a_line = a_lines.clone().nth(lookahead).map(|v| v.trim());
            let b_line = b_lines.clone().nth(i).map(|v| v.1.trim());
            if a_line == b_line && a_line != Some("") && b_line != Some("") {
              found = (lookahead, i);
              break 'outer;
            }
            let a_line = a_lines.clone().nth(i).map(|v| v.trim());
            let b_line = b_lines.clone().nth(lookahead).map(|v| v.1.trim());
            if a_line == b_line && a_line != Some("") && b_line != Some("") {
              found = (i, lookahead);
              break 'outer;
            }
          }
          let a_line = a_lines.clone().nth(lookahead).map(|v| v.trim());
          let b_line = b_lines.clone().nth(lookahead).map(|v| v.1.trim());
          if a_line == b_line && a_line != Some("") && b_line != Some("") {
            found = (lookahead, lookahead);
            break 'outer;
          }
        }
        let a_found = found.0;
        let b_found = found.1;

        if found == (0, 0) {
          // never reconciled, just give up
          lines.push((line_num as u64, LineDiff::Change));
          for (line_num, _) in b_lines.by_ref() {
            lines.push((line_num as u64, LineDiff::Change));
            last_line = line_num as u64;
          }
        } else if b_found == 0 {
          // Only removed lines
          lines.push((line_num as u64 - 1, LineDiff::Remove));

          for _ in 0..a_found {
            if a_lines.next().is_none() {
              break;
            }
          }
        } else {
          // consume a_found lines
          for _ in 0..a_found {
            if a_lines.next().is_none() {
              break;
            }
          }
          // consume b_found lines
          for i in 0..b_found {
            match b_lines.next() {
              Some((line_num, _)) => {
                lines.push((
                  line_num as u64,
                  if i < a_found { LineDiff::Change } else { LineDiff::Add },
                ));
                last_line = line_num as u64;
              }
              None => {
                lines.push((last_line, if i < a_found { LineDiff::Change } else { LineDiff::Add }));
                break;
              }
            }
          }
        }

        continue;
      }
    };

    lines.push(diff);
  }

  Diff { lines }
}

#[cfg(test)]
mod tests {
  use super::*;

  #[test]
  fn adds() {
    assert_eq!(diff("hello", "hello"), Diff { lines: vec![] });
    assert_eq!(diff("hello", "hello\nworld"), Diff { lines: vec![(1, LineDiff::Add)] });
    assert_eq!(
      diff("", "hello\nworld"),
      Diff { lines: vec![(0, LineDiff::Add), (1, LineDiff::Add)] }
    );

    assert_eq!(diff("foo\nbar", "foo\nbar\nbaz"), Diff { lines: vec![(2, LineDiff::Add)] });
    assert_eq!(diff("foo\nbar", "foo\nxxx\nbar"), Diff { lines: vec![(1, LineDiff::Add)] });
  }

  #[test]
  fn changes() {
    assert_eq!(diff("foo", "hello"), Diff { lines: vec![(0, LineDiff::Change)] });
    assert_eq!(
      diff("foo", "hello\nworld"),
      Diff { lines: vec![(0, LineDiff::Change), (1, LineDiff::Add)] }
    );

    assert_eq!(diff("a\nd", "z\nd"), Diff { lines: vec![(0, LineDiff::Change)] });
    assert_eq!(diff("a\nb\nd", "z\nd"), Diff { lines: vec![(0, LineDiff::Change)] });

    assert_eq!(
      diff("a\nd", "z\nb\nd"),
      Diff { lines: vec![(0, LineDiff::Change), (1, LineDiff::Add)] }
    );
  }

  #[test]
  fn removes() {
    assert_eq!(diff("foo\nbar", "foo"), Diff { lines: vec![(1, LineDiff::Remove)] });
  }

  #[test]
  fn reconcile_diffs() {
    assert_eq!(diff("a\nb\nc\nd", "a\nc\nd"), Diff { lines: vec![(0, LineDiff::Remove)] });
    assert_eq!(diff("a\nb\nc\nd", "a\nd"), Diff { lines: vec![(0, LineDiff::Remove)] });
    assert_eq!(
      diff("a\nb\nc\nd", "a\nd\ne"),
      Diff { lines: vec![(0, LineDiff::Remove), (2, LineDiff::Add)] }
    );
  }

  #[test]
  fn multiple_diffs() {
    assert_eq!(
      diff("a\nd\ne", "z\nb\nd\nf"),
      Diff { lines: vec![(0, LineDiff::Change), (1, LineDiff::Add), (3, LineDiff::Change)] }
    );

    assert_eq!(
      diff("a\nd\ne", "z\nb\nd"),
      Diff { lines: vec![(0, LineDiff::Change), (1, LineDiff::Add), (3, LineDiff::Remove)] }
    );

    assert_eq!(
      diff("a\nd\ne", "z\nb\ne\nf"),
      Diff { lines: vec![(0, LineDiff::Change), (1, LineDiff::Change), (3, LineDiff::Add)] }
    );
  }

  #[test]
  fn clear_line() {
    assert_eq!(diff("a\nb\nc\n\n", "a\n\nc\n\n"), Diff { lines: vec![(1, LineDiff::Change)] });
  }

  #[test]
  fn add_line() {
    assert_eq!(diff("a\nb\nc\nd\n", "a\nb\n\nc\nd\n"), Diff { lines: vec![(2, LineDiff::Add)] });
    assert_eq!(
      diff("a\nb\nc\n\n", "a\nb\n\n\nc\n\n"),
      Diff { lines: vec![(2, LineDiff::Add), (3, LineDiff::Add)] }
    );
  }
}
