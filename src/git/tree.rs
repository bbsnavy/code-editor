use std::io;

use super::{Object, ObjectKind, SHA1};

#[derive(Debug)]
pub struct Tree {
  pub items: Vec<TreeItem>,
}

#[derive(Debug)]
pub enum Perms {
  NormalFile,
  ExecutableFile,
  Directory,
}

#[derive(Debug)]
pub struct TreeItem {
  pub perms: Perms,
  pub name:  String,
  pub sha:   SHA1,
}

impl Tree {
  pub fn read_object(sha: SHA1) -> io::Result<Self> {
    let object = Object::read(sha)?;
    assert_eq!(object.kind, ObjectKind::Tree);

    let mut body = &object.content[..];

    let mut items = vec![];

    while !body.is_empty() {
      let idx = body
        .iter()
        .position(|&b| b == b'\0')
        .ok_or_else(|| io::Error::new(io::ErrorKind::InvalidData, "invalid tree"))?;

      let item = std::str::from_utf8(&body[..idx])
        .map_err(|_| io::Error::new(io::ErrorKind::InvalidData, "invalid tree"))?;
      let sha = SHA1(body[idx + 1..idx + 21].try_into().unwrap());

      items.push(TreeItem::parse(item, sha));

      body = &body[idx + 21..];
    }

    Ok(Tree { items })
  }
}

impl TreeItem {
  pub fn parse(header: &str, sha: SHA1) -> Self {
    let mut sections = header.splitn(2, ' ');
    let perms = Perms::parse(sections.next().unwrap());
    let name = sections.next().unwrap().to_string();

    TreeItem { perms, name, sha }
  }
}

impl Perms {
  pub fn parse(permissions: &str) -> Self {
    match permissions {
      "100644" => Perms::NormalFile,
      "100755" => Perms::ExecutableFile,
      "40000" => Perms::Directory,
      _ => panic!("unknown permissions: {permissions}"),
    }
  }
}
