use std::{io, str::FromStr};

use super::{Object, ObjectKind, SHA1};

#[derive(Debug)]
pub struct Commit {
  pub tree:   SHA1,
  pub parent: SHA1,

  pub author:    Option<String>,
  pub committer: Option<String>,
}

impl Commit {
  pub fn read_object(sha: SHA1) -> io::Result<Self> {
    let object = Object::read(sha)?;
    assert_eq!(object.kind, ObjectKind::Commit);
    let content = String::from_utf8(object.content)
      .map_err(|_| io::Error::new(io::ErrorKind::InvalidData, "object contains invalid UTF8"))?;

    let mut tree = None;
    let mut parent = None;
    let mut author = None;
    let mut committer = None;

    for item in content.lines() {
      if item.is_empty() {
        break;
      }

      let (key, value) = item.split_once(' ').unwrap();

      match key {
        "tree" => tree = Some(SHA1::from_str(value).unwrap()),
        "parent" => parent = Some(SHA1::from_str(value).unwrap()),
        "author" => author = Some(value.to_string()),
        "committer" => committer = Some(value.to_string()),
        _ => {}
      }
    }

    Ok(Commit { tree: tree.unwrap(), parent: parent.unwrap(), author, committer })
  }
}
