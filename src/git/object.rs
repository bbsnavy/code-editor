use std::io::{self, Read};

use super::SHA1;

#[derive(Debug, Clone, Copy, PartialEq, Eq)]
pub enum ObjectKind {
  Blob,
  Tree,
  Commit,
  Tag,
}

pub struct Object {
  pub kind:    ObjectKind,
  pub content: Vec<u8>,
}

impl Object {
  pub fn read(sha: SHA1) -> io::Result<Object> {
    let content = std::fs::read(format!(".git/objects/{}", sha.as_path()))?;

    let mut output = vec![];

    let mut reader = flate2::read::ZlibDecoder::new(&content[..]);
    reader.read_to_end(&mut output)?;

    let mut parts = output.splitn(2, |&b| b == b'\0');
    let kind = ObjectKind::parse(std::str::from_utf8(parts.next().unwrap()).unwrap());
    let content = parts.next().unwrap().to_vec();

    Ok(Object { kind, content })
  }
}

impl ObjectKind {
  pub fn parse(header: &str) -> ObjectKind {
    match header.split(' ').next().unwrap() {
      "blob" => ObjectKind::Blob,
      "tree" => ObjectKind::Tree,
      "commit" => ObjectKind::Commit,
      "tag" => ObjectKind::Tag,
      _ => panic!("invalid header {header}"),
    }
  }
}
