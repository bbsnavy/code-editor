use core::fmt;
use std::{
  num::ParseIntError,
  path::{Path, PathBuf},
  str::FromStr,
};

mod commit;
mod diff;
mod object;
mod tree;

pub use commit::Commit;
pub use diff::{diff, Diff, LineDiff};
pub use object::{Object, ObjectKind};
pub use tree::{Perms, Tree};

#[test]
fn test_foo() {
  let main = std::fs::read_to_string(".git/HEAD").unwrap();
  let branch = main.strip_prefix("ref: refs/heads/").unwrap().trim();

  let sha =
    SHA1::from_str(std::fs::read_to_string(format!(".git/refs/heads/{branch}")).unwrap().trim())
      .unwrap();
  let commit = Commit::read_object(sha).unwrap();

  let tree = Tree::read_object(commit.tree).unwrap();

  let item = tree.items.iter().find(|item| item.name == "Cargo.toml").unwrap();
  println!("{}", String::from_utf8(Object::read(item.sha).unwrap().content).unwrap());

  panic!();
}

fn git_dir_from(path: impl AsRef<Path>) -> Option<PathBuf> {
  let mut path = path.as_ref();
  loop {
    if path.join(".git").exists() {
      return Some(path.to_path_buf());
    }
    match path.parent() {
      Some(parent) => path = parent,
      None => return None,
    }
  }
}

pub fn head_file(path: impl AsRef<Path>) -> Option<String> {
  let git_dir = git_dir_from(&path)?;

  let main = std::fs::read_to_string(git_dir.join(".git").join("HEAD")).ok()?;
  let branch = main.strip_prefix("ref: refs/heads/").unwrap().trim();

  let sha =
    SHA1::from_str(std::fs::read_to_string(format!(".git/refs/heads/{branch}")).unwrap().trim())
      .unwrap();
  let commit = Commit::read_object(sha).ok()?;

  let mut tree = Tree::read_object(commit.tree).ok()?;

  let rel = path.as_ref().strip_prefix(git_dir).unwrap();
  for component in rel.components() {
    match component {
      std::path::Component::CurDir => {}
      std::path::Component::RootDir => {}
      std::path::Component::ParentDir => todo!(),
      std::path::Component::Prefix(_) => todo!(),
      std::path::Component::Normal(section) => {
        match tree.items.iter().find(|item| item.name == section.to_str().unwrap()) {
          Some(item) => match item.perms {
            Perms::NormalFile => {
              return String::from_utf8(Object::read(item.sha).ok()?.content).ok();
            }
            Perms::Directory => {
              tree = Tree::read_object(item.sha).ok()?;
            }
            _ => todo!(),
          },
          None => break,
        }
      }
    }
  }
  None
}

#[derive(Clone, Copy)]
pub struct SHA1([u8; 20]);

impl SHA1 {
  pub fn as_path(&self) -> SHA1Path { SHA1Path(*self) }
}

#[derive(Clone, Copy)]
pub struct SHA1Path(SHA1);

#[derive(Debug)]
pub enum ParseSHA1Error {
  InvalidLength,
  ParseIntError(ParseIntError),
}

impl From<ParseIntError> for ParseSHA1Error {
  fn from(e: ParseIntError) -> Self { ParseSHA1Error::ParseIntError(e) }
}

impl FromStr for SHA1 {
  type Err = ParseSHA1Error;

  fn from_str(s: &str) -> Result<Self, Self::Err> {
    if s.len() != 40 {
      return Err(ParseSHA1Error::InvalidLength);
    }
    let mut bytes = [0; 20];
    for i in 0..20 {
      bytes[i] = u8::from_str_radix(&s[i * 2..i * 2 + 2], 16)?;
    }
    Ok(SHA1(bytes))
  }
}

impl fmt::Display for SHA1 {
  fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
    for b in self.0.iter() {
      write!(f, "{:02x}", b)?;
    }
    Ok(())
  }
}

impl fmt::Debug for SHA1 {
  fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result { write!(f, "SHA1({self})") }
}

impl fmt::Display for SHA1Path {
  fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
    for (i, b) in self.0 .0.iter().enumerate() {
      write!(f, "{:02x}", b)?;
      if i == 0 {
        write!(f, "/")?;
      }
    }
    Ok(())
  }
}

impl fmt::Debug for SHA1Path {
  fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result { write!(f, "SHA1Path({self})") }
}
