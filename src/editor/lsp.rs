//! Editor specific LSP implementation.

use std::{
  cell::{Cell, RefMut},
  rc::Rc,
};

use lsp_types::Url;

use crate::{global::GlobalState, location::FileLocation, lsp::LSPConnection};

use super::{
  completion::{Completion, CompletionsPlugin},
  diagnostic::{Diagnostic, DiagnosticsPlugin, Severity},
  Editor, Plugin, Pos, Transaction,
};

pub struct LspPlugin {
  pub global: Rc<GlobalState>,

  /// Version of the file for LSP. Increments on any change.
  pub version: Cell<i32>,
}

impl LspPlugin {
  pub fn new(global: Rc<GlobalState>) -> LspPlugin { LspPlugin { global, version: Cell::new(0) } }

  #[track_caller]
  fn lsp_info(&self, editor: &Editor) -> Option<(RefMut<LSPConnection>, Url)> {
    let borrow = self.global.lsp_client.borrow_mut();
    if borrow.is_some() {
      let client = RefMut::map(borrow, |b| b.as_mut().unwrap());
      if let Some((location, _)) = editor.file.as_ref() {
        return Some((client, location.to_lsp()));
      }
    }
    None
  }

  pub fn request_completions(&self, editor: &Editor) {
    if let Some((mut client, url)) = self.lsp_info(editor) {
      client.send(
        "textDocument/completion",
        &lsp_types::CompletionParams {
          text_document_position:    lsp_types::TextDocumentPositionParams {
            text_document: lsp_types::TextDocumentIdentifier { uri: url },
            position:      editor.selection.head.pos.into(),
          },
          context:                   None,
          partial_result_params:     lsp_types::PartialResultParams { partial_result_token: None },
          work_done_progress_params: lsp_types::WorkDoneProgressParams { work_done_token: None },
        },
        |res, window| {
          if let Ok(res) = res.decode::<lsp_types::CompletionResponse>() {
            let mut items = match res {
              lsp_types::CompletionResponse::Array(items) => items,
              lsp_types::CompletionResponse::List(list) => list.items,
            };
            items.sort_by(|a, b| a.sort_text.cmp(&b.sort_text));
            let items = items.into_iter().map(|item| Completion::from_lsp(item)).collect();
            window
              .editor_mut()
              .editor
              .plugin_mut::<CompletionsPlugin>()
              .map(|mut p| p.completions = items);
          }
        },
      );
    }
  }

  pub fn goto_definition(&self, editor: &Editor) {
    if let Some((mut client, url)) = self.lsp_info(editor) {
      client.send(
        "textDocument/definition",
        &lsp_types::GotoDefinitionParams {
          text_document_position_params: lsp_types::TextDocumentPositionParams {
            position:      editor.selection.head.pos.into(),
            text_document: lsp_types::TextDocumentIdentifier { uri: url },
          },
          partial_result_params:         lsp_types::PartialResultParams {
            partial_result_token: None,
          },
          work_done_progress_params:     lsp_types::WorkDoneProgressParams {
            work_done_token: None,
          },
        },
        |res, window| {
          if let Ok(res) = res.decode::<lsp_types::GotoDefinitionResponse>() {
            let (uri, pos) = match res {
              lsp_types::GotoDefinitionResponse::Scalar(ref location) => {
                (&location.uri, location.range.start)
              }
              lsp_types::GotoDefinitionResponse::Array(ref locations) => {
                (&locations[0].uri, locations[0].range.start)
              }
              lsp_types::GotoDefinitionResponse::Link(ref links) => {
                (&links[0].target_uri, links[0].target_range.start)
              }
            };
            match FileLocation::from_lsp(uri.clone()) {
              Some(location) => {
                window.open(location);
                window.editor_mut().editor.selection.head.pos = pos.into();
              }
              None => {}
            }
          }
        },
      );
    }
  }
}

impl Plugin for LspPlugin {
  fn on_open(&mut self, editor: &Editor) {
    if let Some((mut client, url)) = self.lsp_info(editor) {
      client.notify(
        "textDocument/didOpen",
        &lsp_types::DidOpenTextDocumentParams {
          text_document: lsp_types::TextDocumentItem {
            uri:         url.clone(),
            version:     self.version.get(),
            language_id: "rust".into(),
            text:        std::fs::read_to_string(url.path()).unwrap(),
          },
        },
      );

      client.listen("textDocument/publishDiagnostics", move |res, window| {
        if let Ok(res) = res.decode::<lsp_types::PublishDiagnosticsParams>() {
          if res.uri == url {
            let editor = &mut window.editor_mut().editor;
            let diagnostics =
              res.diagnostics.into_iter().map(|d| Diagnostic::from_lsp(editor, d)).collect();
            editor
              .plugin_mut::<DiagnosticsPlugin>()
              .map(|mut p| p.set_diagnostics(editor, diagnostics));
          }
        }
      });
    }
  }

  fn on_pre_change(&mut self, editor: &Editor, t: &Transaction) {
    if let Some((mut client, url)) = self.lsp_info(editor) {
      client.notify(
        "textDocument/didChange",
        &lsp_types::DidChangeTextDocumentParams {
          text_document:   lsp_types::VersionedTextDocumentIdentifier {
            uri:     url,
            version: self.version.get(),
          },
          content_changes: t
            .changes
            .iter()
            .map(|c| lsp_types::TextDocumentContentChangeEvent {
              range:        Some(lsp_types::Range {
                start: editor.doc.index_to_pos(c.start).into(),
                end:   editor.doc.index_to_pos(c.end).into(),
              }),
              range_length: Some((c.end - c.start) as u32),
              text:         c.insert.clone(),
            })
            .collect(),
        },
      );
      self.version.set(self.version.get() + 1);
    }
  }

  fn on_save(&mut self, editor: &Editor) {
    if let Some((mut client, url)) = self.lsp_info(editor) {
      client.notify(
        "textDocument/didSave",
        &lsp_types::DidSaveTextDocumentParams {
          text_document: lsp_types::TextDocumentIdentifier { uri: url },
          // TODO: Read server properties to see if we should send the text or not.
          text:          None,
        },
      );
    }
  }
}

impl Diagnostic {
  pub fn from_lsp(editor: &Editor, lsp: lsp_types::Diagnostic) -> Self {
    Diagnostic {
      start:    editor.doc.pos_to_index(lsp.range.start.into()),
      end:      editor.doc.pos_to_index(lsp.range.end.into()),
      message:  lsp.message,
      severity: match lsp.severity {
        Some(lsp) => Severity::from_lsp(lsp),
        None => Severity::Error,
      },
    }
  }
}

impl Severity {
  pub fn from_lsp(lsp: lsp_types::DiagnosticSeverity) -> Self {
    match lsp {
      lsp_types::DiagnosticSeverity::ERROR => Severity::Error,
      lsp_types::DiagnosticSeverity::WARNING => Severity::Warning,
      _ => Severity::Info,
    }
  }
}

impl From<lsp_types::Position> for Pos {
  fn from(value: lsp_types::Position) -> Self {
    Pos { row: value.line.into(), col: value.character.into() }
  }
}
impl From<Pos> for lsp_types::Position {
  fn from(value: Pos) -> Self {
    lsp_types::Position { line: value.row as u32, character: value.col as u32 }
  }
}
