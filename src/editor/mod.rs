mod completion;
mod diagnostic;
mod doc;
mod edit;
mod file;
mod git;
mod highlight;
mod input;
mod lsp;
mod render;
mod selection;

use std::{
  any::Any,
  cell::{Ref, RefCell, RefMut},
  fs::File,
  rc::Rc,
};

use completion::CompletionsPlugin;
pub use diagnostic::DiagnosticsPlugin;
use git::GitPlugin;
use highlight::HighlighterPlugin;
use lsp::LspPlugin;

pub use doc::{Change, Document, Transaction};

use crate::{global::GlobalState, location::FileLocation, tab::editor::ScopedCtx};

/// Implements the editor logic, including keybinds, parsing, syntax
/// highlighting, etc.
pub struct Editor {
  pub global: Rc<GlobalState>,

  pub doc:            Document,
  /// A list of (undo, redo) pairs.
  pub history:        Vec<(Transaction, Transaction)>,
  pub history_cursor: usize,

  pub mode:           Mode,
  /// While in insert mode, this gets updated instead of history. Once leaving
  /// edit mode, this is inserted into the history.
  pub current_change: (Transaction, Transaction),

  pub selection: Selection,

  pub plugins: Vec<Box<RefCell<dyn Plugin>>>,

  /// The current file that is open.
  pub file: Option<(FileLocation, File)>,
}

#[derive(Debug)]
pub struct Decoration {
  pub start: u64,
  pub end:   u64,
  pub style: String,
}

pub trait Plugin: Any {
  fn on_open(&mut self, editor: &Editor) { let _ = editor; }
  fn on_save(&mut self, editor: &Editor) { let _ = editor; }
  fn on_pre_change(&mut self, editor: &Editor, t: &Transaction) { let _ = (editor, t); }
  fn on_post_change(&mut self, editor: &Editor, t: &Transaction) { let _ = (editor, t); }

  fn render(&self, editor: &Editor, ctx: &mut ScopedCtx, scroll_row: u64) {
    let _ = (editor, ctx, scroll_row);
  }

  /// Width of the gutter in pixels. If this returns `0` (the default),
  /// `render_gutter` will not be called.
  fn gutter_width(&self) -> u32 { 0 }

  /// Called if `gutter_width` returns a number larger than zero.
  fn render_gutter(&self, editor: &Editor, scroll_row: u64, ctx: &mut ScopedCtx) {
    let _ = (editor, scroll_row, ctx);
  }

  fn decorate<'a>(
    &'a self,
    editor: &Editor,
    first_row: u64,
    last_row: u64,
  ) -> Option<Box<dyn Iterator<Item = Decoration> + 'a>> {
    let _ = (editor, first_row, last_row);
    None
  }
}

#[derive(Clone, Copy, PartialEq)]
pub enum Mode {
  Normal,
  Insert,
  Visual(VisualMode),
}

#[derive(Clone, Copy, PartialEq)]
pub enum VisualMode {
  /// Selection spans from anchor to cursor head.
  Char,
  /// Selection contains the entire line of the anchor, to the entire line of
  /// the head.
  Line,
}

#[derive(Default, Clone, Copy)]
pub struct Selection {
  anchor: Option<CursorPos>,
  head:   CursorPos,
}

#[derive(Default, Clone, Copy, PartialEq, PartialOrd)]
pub struct CursorPos {
  pub pos: Pos,

  /// The target column to keep your cursor at when moving up/down.
  pub target_col: u64,
}

/// Stores a cursor position, starting from 0.
#[derive(Default, Debug, Clone, Copy, PartialEq, PartialOrd)]
pub struct Pos {
  pub row: u64,
  pub col: u64,
}

impl Editor {
  #[cfg(test)]
  pub fn empty() -> Self { Self::new(Rc::new(GlobalState::default())) }

  pub fn new(global: Rc<GlobalState>) -> Self {
    let mut editor = Editor {
      global:         global.clone(),
      doc:            Document::from("\n"),
      history:        vec![],
      history_cursor: 0,
      mode:           Mode::Normal,
      current_change: (Transaction::default(), Transaction::default()),
      selection:      Selection { anchor: None, head: CursorPos::default() },
      plugins:        vec![],
      file:           None,
    };

    editor.add_plugin(GitPlugin::new());
    editor.add_plugin(LspPlugin::new(global));
    editor.add_plugin(CompletionsPlugin::new());
    HighlighterPlugin::new("").map(|p| editor.add_plugin(p));
    editor.add_plugin(DiagnosticsPlugin::new());

    editor
  }

  pub fn add_plugin(&mut self, plugin: impl Plugin + 'static) {
    self.plugins.push(Box::new(RefCell::new(plugin)));
  }

  pub fn plugin<T: Plugin>(&self) -> Option<Ref<T>> {
    for p in &self.plugins {
      let r = Ref::map(p.borrow(), |v| v as &dyn Any);
      if r.downcast_ref::<T>().is_some() {
        return Some(Ref::map(r, |v| v.downcast_ref::<T>().unwrap()));
      }
    }
    None
  }

  pub fn plugin_mut<T: Plugin>(&self) -> Option<RefMut<T>> {
    for p in &self.plugins {
      let mut r = RefMut::map(p.borrow_mut(), |v| v as &mut dyn Any);
      if r.downcast_mut::<T>().is_some() {
        return Some(RefMut::map(r, |v| v.downcast_mut::<T>().unwrap()));
      }
    }
    None
  }

  pub fn set_mode(&mut self, new: Mode) {
    match new {
      Mode::Normal => {
        self.selection.anchor = None;
        if self.selection.head.pos.col > 0 {
          self.selection.head.pos.col -= 1;
        }
        self.selection.head.target_col = self.selection.head.pos.col;
        self.plugin_mut::<CompletionsPlugin>().map(|mut p| p.hide());
        self.move_col_to_target();

        let (mut undo, mut t) = std::mem::take(&mut self.current_change);
        t.simplify();
        undo.simplify();
        self.history.truncate(self.history_cursor);
        self.history.push((undo, t));
        self.history_cursor = self.history.len();
      }
      Mode::Insert => {}
      Mode::Visual(_) => {}
    }
    self.mode = new;
  }
}
