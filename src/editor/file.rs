//! Handles file management. Because each `Editor` is always tied to a single
//! file, this just includes reading and writing a single file.

use std::{
  fs::{File, OpenOptions},
  io::{self, Read, Seek},
  path::Path,
};

use crate::location::FileLocation;

use super::{Change, Document, Editor};

impl Editor {
  /// Opens the given file, no questions asked. This will remove all history,
  /// and delete the old file without saving it.
  pub fn open(&mut self, location: FileLocation) {
    // TODO: handle remote files
    let path = match location {
      FileLocation::Path(ref path) => path,
    };
    let mut file = OpenOptions::new().read(true).write(true).open(&path).unwrap();
    let mut content = String::new();
    file.read_to_string(&mut content).unwrap();
    self.file = Some((location, file));

    self.doc.apply_change(&Change { start: 0, end: self.doc.len(), insert: content });
    self.selection.anchor = None;
    self.selection.head.pos.row = 0;
    self.selection.head.pos.col = 0;
    self.selection.head.target_col = 0;
    self.mode = crate::editor::Mode::Normal;
    self.current_change = Default::default();
    self.history_cursor = 0;
    self.history.clear();

    for p in &self.plugins {
      p.borrow_mut().on_open(&self);
    }
  }

  pub fn save(&mut self) -> Option<(&Path, io::Result<()>)> {
    let res = match &mut self.file {
      Some((_, file)) => Some(self.doc.save_to(file)),
      None => None,
    };

    if let Some(Ok(())) = res {
      for p in &self.plugins {
        p.borrow_mut().on_save(&self);
      }
    }

    res.and_then(|res| Some((self.file.as_ref().unwrap().0.as_path()?, res)))
  }
}

impl Document {
  pub fn save_to(&mut self, file: &mut File) -> io::Result<()> {
    file.seek(io::SeekFrom::Start(0))?;
    file.set_len(0)?;
    self.rope().write_to(file)?;
    Ok(())
  }
}
