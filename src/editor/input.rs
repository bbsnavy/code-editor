use crate::{
  input::Key,
  tab::editor::{InputContext, KeyResponse},
};

use super::{
  completion::CompletionsPlugin, lsp::LspPlugin, DiagnosticsPlugin, Editor, Mode, VisualMode,
};

impl Editor {
  pub fn input(&mut self, ctx: &mut InputContext) -> KeyResponse {
    match self.mode {
      Mode::Insert => {
        for c in &ctx.seq.seq {
          match c.key {
            // Escape
            Key::Escape => self.set_mode(Mode::Normal),
            // ctrl+space
            Key::Char(' ') if c.mods.has_ctrl() => {
              self.plugin_mut::<CompletionsPlugin>().map(|mut p| p.show());
              self.plugin::<LspPlugin>().map(|p| p.request_completions(&self));
            }
            Key::Char('n') if c.mods.has_ctrl() => {
              self.plugin_mut::<CompletionsPlugin>().map(|mut p| p.next());
            }
            Key::Char('p') if c.mods.has_ctrl() => {
              self.plugin_mut::<CompletionsPlugin>().map(|mut p| p.prev());
            }
            Key::Tab => {
              let completed = match self.plugin_mut::<CompletionsPlugin>() {
                Some(mut p) if p.visible => {
                  p.next();
                  true
                }
                _ => false,
              };
              if !completed {
                self.insert_str("  ")
              }
            }
            key => self.insert(key),
          }
        }
        KeyResponse::Consumed
      }
      Mode::Normal => match self.input_movement(ctx) {
        KeyResponse::Unknown => {
          match ctx.seq.to_string().as_str() {
            ":" => ctx.switch_to_command(),

            "x" => self.delete_char(),
            "i" => self.mode = Mode::Insert,
            "a" => {
              self.mode = Mode::Insert;
              self.right();
            }
            "o" => {
              self.mode = Mode::Insert;
              self.add_newline();
            }
            "r" => return KeyResponse::Waiting,
            s if s.starts_with("r") && ctx.seq.seq.len() == 2 => match ctx.seq.seq[1].key {
              Key::Char(c) => self.replace_char(c),
              _ => return KeyResponse::Unknown,
            },
            s if s.starts_with("r") => return KeyResponse::Unknown,

            "u" => self.undo(),
            "<C-r>" => self.redo(),

            "v" => {
              self.selection.anchor = Some(self.selection.head);
              self.mode = Mode::Visual(VisualMode::Char);
            }
            "V" => {
              self.mode = Mode::Visual(VisualMode::Line);
            }

            "d" => return KeyResponse::Waiting,
            "dd" => self.delete_line(),
            s if s.starts_with("d") => return KeyResponse::Unknown,

            "D" => self.delete_rest_of_line(),

            " " => return KeyResponse::Waiting,
            " a" => {
              self.plugin_mut::<DiagnosticsPlugin>().map(|mut p| p.show_popup(self));
            }
            s if s.starts_with(' ') => return KeyResponse::Unknown,

            _ => return KeyResponse::Unknown,
          }
          KeyResponse::Consumed
        }
        other => other,
      },
      Mode::Visual(_) => match self.input_movement(ctx) {
        KeyResponse::Unknown => {
          match ctx.seq.to_string().as_str() {
            "x" => {
              self.delete_char();
              self.set_mode(Mode::Normal);
            }

            _ => return KeyResponse::Unknown,
          }
          KeyResponse::Consumed
        }
        other => other,
      },
    }
  }

  fn input_movement(&mut self, ctx: &mut InputContext) -> KeyResponse {
    match ctx.seq.to_string().as_str() {
      "h" => self.left(),
      "j" => self.down(),
      "k" => self.up(),
      "l" => self.right(),

      "0" => self.goto_start_of_line(),
      "$" => self.goto_end_of_line(),

      "g" => return KeyResponse::Waiting,
      "gg" => self.goto_start(),
      "gd" => {
        self.plugin::<LspPlugin>().map(|p| p.goto_definition(&self));
      }
      s if s.starts_with("g") => return KeyResponse::Unknown,

      "G" => self.goto_end(),

      "ESC" => self.set_mode(Mode::Normal),

      _ => return KeyResponse::Unknown,
    }
    KeyResponse::Consumed
  }
}
