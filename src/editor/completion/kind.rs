use lsp_types::CompletionItemKind;

use crate::nerdfonts::Icon;

pub enum CompletionKind {
  Text,
  Method,
  Function,
  Constructor,
  Field,
  Variable,
  Class,
  Interface,
  Module,
  Property,
  Unit,
  Value,
  Enum,
  Keyword,
  Snippet,
  Color,
  File,
  Reference,
  Folder,
  EnumMember,
  Constant,
  Struct,
  Event,
  Operator,
  TypeParameter,
}

impl CompletionKind {
  pub fn from_lsp(kind: CompletionItemKind) -> Self {
    use CompletionKind::*;
    match kind {
      CompletionItemKind::TEXT => Text,
      CompletionItemKind::METHOD => Method,
      CompletionItemKind::FUNCTION => Function,
      CompletionItemKind::CONSTRUCTOR => Constructor,
      CompletionItemKind::FIELD => Field,
      CompletionItemKind::VARIABLE => Variable,
      CompletionItemKind::CLASS => Class,
      CompletionItemKind::INTERFACE => Interface,
      CompletionItemKind::MODULE => Module,
      CompletionItemKind::PROPERTY => Property,
      CompletionItemKind::UNIT => Unit,
      CompletionItemKind::VALUE => Value,
      CompletionItemKind::ENUM => Enum,
      CompletionItemKind::KEYWORD => Keyword,
      CompletionItemKind::SNIPPET => Snippet,
      CompletionItemKind::COLOR => Color,
      CompletionItemKind::FILE => File,
      CompletionItemKind::REFERENCE => Reference,
      CompletionItemKind::FOLDER => Folder,
      CompletionItemKind::ENUM_MEMBER => EnumMember,
      CompletionItemKind::CONSTANT => Constant,
      CompletionItemKind::STRUCT => Struct,
      CompletionItemKind::EVENT => Event,
      CompletionItemKind::OPERATOR => Operator,
      CompletionItemKind::TYPE_PARAMETER => TypeParameter,
      _ => Text,
    }
  }

  pub fn icon(&self) -> Icon {
    use CompletionKind::*;
    match self {
      Text => Icon::MdText,
      Method => Icon::CodSymbolMethod,
      Function => Icon::MdFunction,
      Constructor => Icon::MdFunction,
      Field => Icon::CodSymbolField,
      Variable => Icon::CodSymbolVariable,
      Class => Icon::CodSymbolClass,
      Interface => Icon::CodSymbolInterface,
      Module => Icon::CodSymbolNamespace,
      Property => Icon::CodSymbolProperty,
      Unit => Icon::MdCodeParentheses,
      Value => Icon::CodSymbolVariable,
      Enum => Icon::CodSymbolEnum,
      Keyword => Icon::CodSymbolKeyword,
      Snippet => Icon::CodSymbolSnippet,
      Color => Icon::CodSymbolColor,
      File => Icon::CodSymbolFile,
      Reference => Icon::CodReferences,
      Folder => Icon::OctFileDirectory,
      EnumMember => Icon::CodSymbolEnumMember,
      Constant => Icon::CodSymbolConstant,
      Struct => Icon::CodSymbolStructure,
      Event => Icon::CodSymbolEvent,
      Operator => Icon::CodSymbolOperator,
      TypeParameter => Icon::CodTypeHierarchy,
    }
  }

  pub fn style(&self) -> &'static str {
    use CompletionKind::*;
    match self {
      Text => "completion.item.icon.text",
      Method => "completion.item.icon.method",
      Function => "completion.item.icon.function",
      Constructor => "completion.item.icon.constructor",
      Field => "completion.item.icon.field",
      Variable => "completion.item.icon.variable",
      Class => "completion.item.icon.class",
      Interface => "completion.item.icon.interface",
      Module => "completion.item.icon.module",
      Property => "completion.item.icon.property",
      Unit => "completion.item.icon.unit",
      Value => "completion.item.icon.value",
      Enum => "completion.item.icon.enum",
      Keyword => "completion.item.icon.keyword",
      Snippet => "completion.item.icon.snippet",
      Color => "completion.item.icon.color",
      File => "completion.item.icon.file",
      Reference => "completion.item.icon.reference",
      Folder => "completion.item.icon.folder",
      EnumMember => "completion.item.icon.enum-member",
      Constant => "completion.item.icon.constant",
      Struct => "completion.item.icon.struct",
      Event => "completion.item.icon.event",
      Operator => "completion.item.icon.operator",
      TypeParameter => "completion.item.icon.type-parameter",
    }
  }
}
