mod kind;

pub use kind::CompletionKind;

use crate::tab::editor::ScopedCtx;

use super::{Editor, Plugin};

pub struct CompletionsPlugin {
  pub completions: Vec<Completion>,
  pub visible:     bool,
  pub active:      Option<usize>,

  scroll: usize,
}

pub struct Completion {
  pub label: String,
  pub kind:  CompletionKind,
}

impl Completion {
  pub fn from_lsp(item: lsp_types::CompletionItem) -> Self {
    Completion {
      label: item.label,
      kind:  item.kind.map(CompletionKind::from_lsp).unwrap_or(CompletionKind::Text),
    }
  }
}

const ROWS: u16 = 10;
const COLS: u16 = 30;

impl CompletionsPlugin {
  pub fn new() -> Self {
    CompletionsPlugin { completions: vec![], visible: false, active: None, scroll: 0 }
  }

  pub fn hide(&mut self) {
    self.visible = false;
    self.active = None;
  }
  pub fn show(&mut self) {
    self.visible = true;
    self.active = None;
  }

  pub fn next(&mut self) {
    match self.active {
      None if !self.completions.is_empty() => self.active = Some(0),
      Some(a) if a + 1 < self.completions.len() => self.active = Some(a + 1),
      _ => {}
    }

    self.update_scroll();
  }

  pub fn prev(&mut self) {
    match self.active {
      Some(0) => self.active = None,
      Some(a) => self.active = Some(a - 1),
      None => {}
    }

    self.update_scroll();
  }

  fn update_scroll(&mut self) {
    if let Some(a) = self.active {
      if a < self.scroll {
        self.scroll = a;
      } else if a > self.scroll + ROWS as usize - 1 {
        self.scroll = a - ROWS as usize + 1;
      }
    }
  }
}

impl Plugin for CompletionsPlugin {
  fn render(&self, editor: &Editor, ctx: &mut ScopedCtx, scroll_row: u64) {
    let cursor_row = (editor.cursor_row().saturating_sub(scroll_row)) as u16;
    let cursor_col = (editor.cursor_col() + 4) as u16;

    if !self.visible || self.completions.is_empty() {
      return;
    }

    let background_style = ctx.theme().get("completion.background");
    let item_style = ctx.theme().get("completion.item");
    let active_style = ctx.theme().get("completion.active");

    let row = cursor_row + 1;
    let col = cursor_col + 3;

    let x = ctx.col_to_px(col);
    let y = ctx.row_to_px(row);
    let width = ctx.col_to_px(COLS);
    let height = ctx.row_to_px(ROWS);

    // draw the actual background
    ctx.draw_rect_px(x, y, width, height, background_style.background);

    // draw a 1 pixel border
    ctx.draw_rect_px(x, y, width, 1, background_style.foreground);
    ctx.draw_rect_px(x, y + height - 1, width, 1, background_style.foreground);
    ctx.draw_rect_px(x, y, 1, height, background_style.foreground);
    ctx.draw_rect_px(x + width - 1, y, 1, height, background_style.foreground);

    let active_line = self.active.map(|a| a - self.scroll);
    for (i, compl) in self.completions.iter().skip(self.scroll).take(ROWS as usize).enumerate() {
      if active_line == Some(i) {
        ctx.draw_rect_px(x, y + i as u32 * 50, width, 50, active_style.background);
      }

      let icon_style = ctx.theme().get(compl.kind.style());
      ctx.draw_char(row + i as u16, col, compl.kind.icon().to_char(), icon_style);

      for (j, c) in compl.label.chars().enumerate() {
        ctx.draw_char(row + i as u16, col + j as u16 + 3, c, item_style);
      }
    }
  }
}
