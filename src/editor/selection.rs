//! Implements moving the cursor and selecting things.

use ropey::RopeSlice;

use crate::tab::CursorMode;

use super::{Editor, Mode};

/// Wraps a rope line. The rope line must include a trailing newline, and
/// functions on this line will ignore that newline.
pub struct EditorLine<'a> {
  slice: RopeSlice<'a>,
}

impl Editor {
  pub fn cursor_row(&self) -> u64 { self.selection.head.pos.row }
  pub fn cursor_col(&self) -> u64 { self.selection.head.pos.col }
  pub fn cursor_mode(&self) -> CursorMode {
    match self.mode {
      Mode::Insert => CursorMode::Beam,
      _ => CursorMode::Block,
    }
  }

  pub fn left(&mut self) {
    if self.selection.head.pos.col > 0 {
      self.selection.head.pos.col -= 1;
      self.selection.head.target_col = self.selection.head.pos.col;
    }
  }
  pub fn right(&mut self) {
    if self.selection.head.pos.col < self.max_col() {
      self.selection.head.pos.col += 1;
      self.selection.head.target_col = self.selection.head.pos.col;
    }
  }
  pub fn up(&mut self) {
    if self.selection.head.pos.row > 0 {
      self.selection.head.pos.row -= 1;
      self.move_col_to_target();
    }
  }
  pub fn down(&mut self) {
    if self.selection.head.pos.row < self.doc.len_lines().saturating_sub(1) {
      self.selection.head.pos.row += 1;
      self.move_col_to_target();
    }
  }

  pub fn goto_start_of_line(&mut self) {
    self.selection.head.pos.col = 0;
    self.selection.head.target_col = 0;
  }

  pub fn goto_end_of_line(&mut self) {
    self.selection.head.pos.col = self.max_col();
    self.selection.head.target_col = u64::MAX;
  }

  pub fn goto_start(&mut self) {
    self.selection.head.pos.row = 0;
    self.selection.head.pos.col = 0;
    self.selection.head.target_col = 0;
  }

  pub fn goto_end(&mut self) {
    self.selection.head.pos.row = self.doc.len_lines() - 1;
    let max = self.max_col();
    self.selection.head.pos.col = max;
    self.selection.head.target_col = max;
  }

  pub(super) fn max_col(&mut self) -> u64 {
    let max = self.line(self.selection.head.pos.row).len();
    match self.mode {
      Mode::Normal => max.saturating_sub(1),
      Mode::Insert | Mode::Visual(_) => max,
    }
  }
  pub(super) fn move_col_to_target(&mut self) {
    let max = self.max_col();
    self.selection.head.pos.col = self.selection.head.target_col.min(max);
  }

  pub fn line(&self, line: u64) -> EditorLine { EditorLine { slice: self.doc.line(line).slice } }

  pub fn len_lines(&self) -> u64 { self.doc.len_lines() }

  pub fn mouse_event(&mut self, row: u64, col: u64) {
    self.selection.head.pos.row = (self.len_lines() - 1).min(row);
    self.selection.head.pos.col = self.max_col().min(col);
    self.selection.head.target_col = self.selection.head.pos.col;
  }
}

impl EditorLine<'_> {
  pub fn len(&self) -> u64 {
    // Don't count the trailing newline
    self.slice.len_chars().saturating_sub(1) as u64
  }
}
