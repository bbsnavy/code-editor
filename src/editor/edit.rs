//! Handles updates to the rope. The rope must always contain a '\n' at the end
//! of every line (including the last line). This makes cursor management much
//! simpler.

use crate::input::Key;

use super::{doc::Change, Editor, Mode, Transaction};

impl Editor {
  pub fn insert(&mut self, key: Key) {
    match key {
      Key::Enter => self.insert_newline(),
      Key::Backspace => self.backspace(),
      Key::Char(c) => self.insert_char(c),
      _ => {}
    }
  }

  pub fn cursor_index(&mut self) -> u64 {
    let start = self.doc.line(self.selection.head.pos.row).start();
    start + self.selection.head.pos.col
  }

  pub fn apply(&mut self, t: impl Into<Transaction>) {
    let t = t.into();
    let mut undo = t.clone();
    undo.invert(&self.doc);
    for p in &self.plugins {
      p.borrow_mut().on_pre_change(&self, &t);
    }
    self.doc.apply(&t);
    for p in &self.plugins {
      p.borrow_mut().on_post_change(&self, &t);
    }

    if self.mode == Mode::Insert {
      self.current_change.0.prepend(undo);
      self.current_change.1.append(t);
    } else {
      self.history.truncate(self.history_cursor);
      self.history.push((undo, t));
      self.history_cursor = self.history.len();
    }
  }

  pub fn undo(&mut self) {
    if self.history_cursor > 0 {
      self.history_cursor -= 1;
      let (undo, _) = &self.history[self.history_cursor];
      for p in &self.plugins {
        p.borrow_mut().on_pre_change(&self, undo);
      }
      self.doc.apply(undo);
      for p in &self.plugins {
        p.borrow_mut().on_post_change(&self, undo);
      }

      if let Some(c) = undo.changes.last() {
        let pos = self.doc.index_to_pos(c.start());
        self.selection.head.pos = pos;
        self.selection.head.target_col = pos.col;
      }
    }
  }

  pub fn redo(&mut self) {
    if self.history_cursor < self.history.len() {
      let (_, redo) = &self.history[self.history_cursor];
      self.history_cursor += 1;
      for p in &self.plugins {
        p.borrow_mut().on_pre_change(&self, redo);
      }
      self.doc.apply(redo);
      for p in &self.plugins {
        p.borrow_mut().on_post_change(&self, redo);
      }

      if let Some(c) = redo.changes.last() {
        let pos = self.doc.index_to_pos(c.start());
        self.selection.head.pos = pos;
        self.selection.head.target_col = pos.col;
      }
    }
  }

  // Appends a newline after the current line, and moves the cursor to it.
  pub fn add_newline(&mut self) {
    let line = self.doc.line(self.selection.head.pos.row);

    self.apply(Change::insert(line.end(), "\n"));
    self.selection.head.pos.row += 1;
    self.selection.head.pos.col = 0;
    self.selection.head.target_col = self.selection.head.pos.col;
  }

  fn backspace(&mut self) {
    let index = self.cursor_index();

    if index == 0 {
      return;
    }

    self.apply(Change::remove(index - 1..index));
    if self.selection.head.pos.col == 0 {
      self.selection.head.pos.row -= 1;
      self.selection.head.pos.col = self.line(self.selection.head.pos.row).len();
    } else {
      self.selection.head.pos.col -= 1;
    }
    self.selection.head.target_col = self.selection.head.pos.col;
  }

  pub fn delete_char(&mut self) {
    let index = self.cursor_index();

    if self.selection.head.pos.col == self.max_col() {
      if self.line(self.selection.head.pos.row).len() != 0 {
        self.apply(Change::remove(index..index + 1));
        self.move_col_to_target();
      }
    } else {
      self.apply(Change::remove(index..index + 1));
    }
  }

  pub fn delete_line(&mut self) {
    let line = self.doc.line(self.selection.head.pos.row);
    self.apply(Change::remove(line.start()..line.end()));

    self.move_col_to_target();
  }

  pub fn delete_rest_of_line(&mut self) {
    let index = self.cursor_index();
    let line = self.doc.line(self.selection.head.pos.row);
    self.apply(Change::remove(index..line.end() - 1));

    self.move_col_to_target();
  }

  pub fn replace_char(&mut self, c: char) {
    let index = self.cursor_index();

    if self.selection.head.pos.col == self.max_col()
      && self.line(self.selection.head.pos.row).len() == 0
    {
      self.apply(Change::insert(index, c));
    } else {
      self.apply(Change::replace(index..index + 1, c));
    }
  }

  fn insert_newline(&mut self) {
    let index = self.cursor_index();

    self.apply(Change::insert(index, "\n"));
    self.selection.head.pos.row += 1;
    self.selection.head.pos.col = 0;
    self.selection.head.target_col = self.selection.head.pos.col;
  }

  fn insert_char(&mut self, c: char) {
    let index = self.cursor_index();

    self.apply(Change::insert(index, c));
    self.selection.head.pos.col += 1;
    self.selection.head.target_col = self.selection.head.pos.col;
  }

  // Only valid for strings with no newlines.
  pub(super) fn insert_str(&mut self, s: &str) {
    let index = self.cursor_index();

    self.apply(Change::insert(index, s));
    self.selection.head.pos.col += s.len() as u64;
    self.selection.head.target_col = self.selection.head.pos.col;
  }
}

#[cfg(test)]
mod tests {
  use super::*;

  #[test]
  fn editor_undo() {
    let mut editor = Editor::empty();
    editor.insert_str("hello ");
    editor.insert_str("world");

    assert_eq!(editor.doc.to_string(), "hello world\n");
    editor.undo();
    assert_eq!(editor.doc.to_string(), "hello \n");
    editor.undo();
    assert_eq!(editor.doc.to_string(), "\n");
    editor.undo();
    assert_eq!(editor.doc.to_string(), "\n");
  }

  #[test]
  fn editor_redo() {
    let mut editor = Editor::empty();
    editor.insert_str("hello ");
    editor.insert_str("world");

    assert_eq!(editor.doc.to_string(), "hello world\n");
    editor.undo();
    assert_eq!(editor.doc.to_string(), "hello \n");
    editor.redo();
    assert_eq!(editor.doc.to_string(), "hello world\n");
    editor.undo();
    assert_eq!(editor.doc.to_string(), "hello \n");
    editor.undo();
    assert_eq!(editor.doc.to_string(), "\n");
  }

  // I'm skeptical that this works so well. The invert() algorithm is so simple
  // that I expected it to have some bugs, but these tests passed on the first
  // try.
  #[test]
  fn insert_mode_undo() {
    let mut editor = Editor::empty();
    editor.insert_str("hello");
    editor.set_mode(Mode::Insert);
    editor.insert(Key::Char(' '));
    editor.insert(Key::Char('h'));
    editor.insert(Key::Char('i'));
    editor.set_mode(Mode::Normal);

    assert_eq!(editor.doc.to_string(), "hello hi\n");
    editor.undo();
    assert_eq!(editor.doc.to_string(), "hello\n");
  }

  #[test]
  fn insert_mode_undo_backwards() {
    let mut editor = Editor::empty();
    editor.insert_str("hello");
    editor.set_mode(Mode::Insert);
    editor.insert(Key::Char('i'));
    editor.selection.head.pos.col -= 1;
    editor.insert(Key::Char('h'));
    editor.selection.head.pos.col -= 2;
    editor.insert(Key::Char(' '));
    editor.set_mode(Mode::Normal);

    assert_eq!(editor.doc.to_string(), "hell ohi\n");
    editor.undo();
    assert_eq!(editor.doc.to_string(), "hello\n");
  }
}
