use ropey::{Rope, RopeSlice};
use std::{fmt, ops::Range};

use super::Pos;

/// A text document.
pub struct Document {
  rope: Rope,
}

#[derive(Debug, Default, Clone)]
pub struct Transaction {
  pub changes: Vec<Change>,
}

#[derive(Debug, Clone, PartialEq, Eq)]
pub struct Change {
  pub start:  u64,
  pub end:    u64,
  pub insert: String,
}

impl Transaction {
  pub fn invert(&mut self, doc: &Document) {
    self.changes.iter_mut().rev().for_each(|c| c.invert(doc));
    self.changes.reverse();
  }

  pub fn prepend(&mut self, other: Transaction) { self.changes.splice(0..0, other.changes); }
  pub fn append(&mut self, mut other: Transaction) { self.changes.append(&mut other.changes); }

  /// Simplifies the list of changes into as short a list as possible.
  pub fn simplify(&mut self) {
    while self.changes.len() >= 2 {
      let mut second = self.changes.pop().unwrap();
      let mut first = self.changes.pop().unwrap();

      if second.start >= first.start
        && second.start <= first.start + first.insert.len() as u64
        && first.insert.len() >= (first.end - first.start) as usize
      {
        first.insert.replace_range(
          (second.start - first.start) as usize..(second.end - first.start) as usize,
          "",
        );
        first.insert.insert_str((second.start - first.start) as usize, &second.insert);
        self.changes.push(first);
        continue;
      }

      if first.insert.is_empty()
        && second.insert.is_empty()
        && second.end >= first.start
        && second.end <= first.end
      {
        second.end += first.end - first.start;
        self.changes.push(second);
        continue;
      }

      self.changes.push(first);
      self.changes.push(second);
      break;
    }
  }
}

impl Change {
  pub fn insert(start: u64, insert: impl Into<String>) -> Self {
    Change { start, end: start, insert: insert.into() }
  }
  pub fn remove(range: Range<u64>) -> Self {
    Change { start: range.start, end: range.end, insert: String::new() }
  }
  pub fn replace(range: Range<u64>, insert: impl Into<String>) -> Self {
    Change { start: range.start, end: range.end, insert: insert.into() }
  }

  pub fn start(&self) -> u64 { self.start }

  /// Number of characters this change adds. Will return a negative number if
  /// this change removes characters.
  pub fn len(&self) -> i64 { self.insert.len() as i64 + (self.start as i64 - self.end as i64) }

  /// Given a document _before_ this change is applied, find the change that
  /// would revert this change.
  pub fn invert(&mut self, doc: &Document) {
    let insert = doc.slice(self.start..self.end).into();
    self.end = self.start + self.insert.len() as u64;
    self.insert = insert;
  }
}

impl From<Change> for Transaction {
  fn from(value: Change) -> Self { Transaction { changes: vec![value] } }
}

impl From<&str> for Document {
  fn from(value: &str) -> Self { Document { rope: Rope::from_str(value) } }
}

pub struct Line<'a> {
  start:     u64,
  end:       u64,
  pub slice: RopeSlice<'a>,
}

impl Line<'_> {
  pub fn start(&self) -> u64 { self.start }
  pub fn end(&self) -> u64 { self.end }
}

impl Document {
  pub fn rope(&self) -> &Rope { &self.rope }

  pub fn line(&self, line_num: u64) -> Line {
    let start = self.rope.line_to_char(line_num as usize);
    let slice = self.rope.line(line_num as usize);
    let end = start + slice.len_chars();
    Line { start: start as u64, end: end as u64, slice }
  }

  pub fn len_lines(&self) -> u64 { (self.rope.len_lines() - 1) as u64 }
  pub fn len(&self) -> u64 { self.rope.len_chars() as u64 }

  pub fn slice(&self, range: Range<u64>) -> &str {
    // todo: figure out this unwrap
    self.rope.slice(range.start as usize..range.end as usize).as_str().unwrap()
  }

  pub fn apply(&mut self, t: &Transaction) {
    for change in &t.changes {
      self.apply_change(change);
    }
  }

  pub fn apply_change(&mut self, change: &Change) {
    self.rope.remove(change.start as usize..change.end as usize);
    self.rope.insert(change.start as usize, &change.insert);
  }

  pub fn index_to_pos(&self, index: u64) -> Pos {
    let row = self.rope().char_to_line(index as usize) as u64;
    let col = index - self.line(row).start();
    Pos { row, col }
  }

  pub fn pos_to_index(&self, pos: Pos) -> u64 { self.line(pos.row).start() + pos.col }
}

impl fmt::Display for Document {
  fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result { write!(f, "{}", self.rope) }
}

#[cfg(test)]
mod tests {
  use super::*;

  #[test]
  fn change_invert() {
    // add
    let mut doc = Document::from("world");
    let change = Change::insert(0, "hello ");

    let mut inverse = change.clone();
    inverse.invert(&doc);

    doc.apply_change(&change);
    assert_eq!(doc.to_string(), "hello world");
    doc.apply_change(&inverse);
    assert_eq!(doc.to_string(), "world");

    // remove
    let mut doc = Document::from("hello world");
    let change = Change::remove(0..6);

    let mut inverse = change.clone();
    inverse.invert(&doc);

    doc.apply_change(&change);
    assert_eq!(doc.to_string(), "world");
    doc.apply_change(&inverse);
    assert_eq!(doc.to_string(), "hello world");

    // replace
    let mut doc = Document::from("world");
    let change = Change::replace(0..1, "hello ");

    let mut inverse = change.clone();
    inverse.invert(&doc);

    doc.apply_change(&change);
    assert_eq!(doc.to_string(), "hello orld");
    doc.apply_change(&inverse);
    assert_eq!(doc.to_string(), "world");
  }

  #[test]
  fn change_len() {
    let change = Change::insert(3, "hi");
    assert_eq!(change.len(), 2);

    let change = Change::remove(4..7);
    assert_eq!(change.len(), -3);

    let change = Change::replace(0..1, "hello");
    assert_eq!(change.len(), 4);

    let change = Change::replace(0..5, "");
    assert_eq!(change.len(), -5);
  }

  #[test]
  fn simplify() {
    let mut t = Transaction::default();
    t.changes.push(Change::insert(0, "hi"));
    t.changes.push(Change::insert(0, "sup"));

    t.simplify();

    assert_eq!(t.changes, vec![Change::insert(0, "suphi")]);

    let mut t = Transaction::default();
    t.changes.push(Change::insert(0, "hi"));
    t.changes.push(Change::insert(2, "sup"));

    t.simplify();

    assert_eq!(t.changes, vec![Change::insert(0, "hisup")]);

    let mut t = Transaction::default();
    t.changes.push(Change::insert(0, "hello"));
    t.changes.push(Change::replace(1..2, "a"));

    t.simplify();

    assert_eq!(t.changes, vec![Change::insert(0, "hallo")]);

    let mut t = Transaction::default();
    t.changes.push(Change::remove(1..2));
    t.changes.push(Change::remove(0..1));

    t.simplify();

    assert_eq!(t.changes, vec![Change::remove(0..2)]);

    let mut t = Transaction::default();
    t.changes.push(Change::remove(0..1));
    t.changes.push(Change::remove(0..1));

    t.simplify();

    assert_eq!(t.changes, vec![Change::remove(0..2)]);
  }
}
