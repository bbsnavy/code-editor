use crate::tree_sitter::{Language, Library, Parser, Query, QueryCursor, QueryMatch};
use std::cell::RefCell;

use super::{Decoration, Editor, Plugin};

/// Provides highlighting to an editor. All updates to the editor must be synced
/// to this highlighter.
#[allow(unused)]
pub struct HighlighterPlugin {
  // These fields all need to be here so they don't get dropped.
  lib:    Library,
  lang:   Language,
  parser: RefCell<Parser>,
  query:  Query<'static>,
}

impl HighlighterPlugin {
  /// Creates a new highlighter. This will load a tree-sitter context at the
  /// given path, assuming its downloaded and compiled.
  pub fn new(lang: &str) -> Option<Self> {
    let _ = lang;

    // build with
    // ```
    // mkdir build
    // gcc -c src/parser.c -o build/parser.o
    // gcc -c src/scanner.c  -o build/scanner.o
    // gcc -shared build/parser.o build/scanner.o -o build/libgrammar.so
    // ```
    let path = crate::data_dir().join("tree-sitter/tree-sitter-rust");

    if !path.exists() {
      return None;
    }

    let so_path = if cfg!(target_os = "macos") {
      path.join("build/libgrammar.dylib")
    } else {
      path.join("build/libgrammar.so")
    };

    let lib = Library::new(so_path.to_str().unwrap());

    let lang = Language::from_lib(&lib);

    let mut parser = Parser::new();
    parser.set_language(&lang);

    let query_src = std::fs::read_to_string(path.join("queries/highlights.scm")).unwrap();

    let query = Query::new(&lang, query_src);

    Some(HighlighterPlugin { lib, lang, parser: RefCell::new(parser), query })
  }
}

impl Plugin for HighlighterPlugin {
  fn decorate<'a>(
    &'a self,
    editor: &Editor,
    first_row: u64,
    last_row: u64,
  ) -> Option<Box<dyn Iterator<Item = Decoration> + 'a>> {
    let mut parser = self.parser.borrow_mut();

    let first_byte = editor.doc.line(first_row).start() as u32;
    let last_byte = editor.doc.line(last_row).end() as u32;

    parser.set_language(&self.lang);
    let tree = parser.parse_str(None, &editor.doc.to_string());
    let root = tree.root_node();

    let mut cursor = QueryCursor::new(&self.query);
    cursor.set_range(first_byte, last_byte);
    cursor.exec(root);

    Some(Box::new(QueryIterator::new(cursor)))
  }
}

struct QueryIterator<'t, 'q> {
  cursor:    QueryCursor<'t, 'q>,
  cap_index: u16,
  m:         Option<QueryMatch<'t, 'q>>,
}

impl<'t, 'q> QueryIterator<'t, 'q> {
  fn new(mut cursor: QueryCursor<'t, 'q>) -> Self {
    Self { m: cursor.next_match(), cursor, cap_index: 0 }
  }
}

impl Iterator for QueryIterator<'_, '_> {
  type Item = Decoration;

  fn next(&mut self) -> Option<Self::Item> {
    match self.m.as_ref()?.captures().nth(self.cap_index as usize) {
      Some(capture) => {
        self.cap_index += 1;

        let node = capture.node();
        let style = format!("highlight.{}", capture.name());

        Some(Decoration { start: node.start() as u64, end: node.end() as u64, style })
      }
      None => {
        self.m = self.cursor.next_match();
        self.cap_index = 0;
        self.next()
      }
    }
  }
}
