use crate::{icon::Icon, tab::editor::ScopedCtx};

use super::{Decoration, Editor, Plugin, Transaction};

pub struct DiagnosticsPlugin {
  diagnostics: Vec<Diagnostic>,

  popup: Option<(u64, u64, Diagnostic)>,
}

#[derive(Clone)]
pub struct Diagnostic {
  pub start: u64,
  pub end:   u64,

  pub message: String,

  pub severity: Severity,
}

#[derive(Clone, Copy)]
pub enum Severity {
  Error,
  Warning,
  Info,
}

impl DiagnosticsPlugin {
  pub fn new() -> Self { DiagnosticsPlugin { diagnostics: vec![], popup: None } }

  pub fn set_diagnostics(&mut self, _editor: &Editor, diagnostics: Vec<Diagnostic>) {
    self.diagnostics = diagnostics;
  }

  pub fn show_popup(&mut self, editor: &Editor) {
    let index = editor.doc.pos_to_index(editor.selection.head.pos);
    self.popup = self
      .diagnostics
      .iter()
      .find(|d| d.start <= index && d.end >= index)
      .cloned()
      .map(|d| (editor.cursor_row(), editor.cursor_col(), d));
  }
}

impl Plugin for DiagnosticsPlugin {
  fn on_pre_change(&mut self, _editor: &Editor, t: &Transaction) {
    for change in &t.changes {
      let start = change.start();
      let len = change.len();
      for d in self.diagnostics.iter_mut() {
        if d.start >= start {
          d.start = d.start.wrapping_add_signed(len);
          d.end = d.end.wrapping_add_signed(len);
        }
      }
    }
  }

  fn gutter_width(&self) -> u32 { 40 }

  fn render_gutter(&self, editor: &Editor, scroll_row: u64, ctx: &mut ScopedCtx) {
    // Diagnostics may be in any order, so we just render them all.
    for d in &self.diagnostics {
      let line = editor.doc.rope().char_to_line(d.start as usize) as u64;

      ctx.draw_icon_px(
        0,
        ctx.row_to_px((line - scroll_row) as u16),
        d.severity.to_icon(),
        ctx.theme().get(d.severity.gutter_key()).foreground,
      );
    }
  }

  fn decorate<'a>(
    &'a self,
    _: &Editor,
    _: u64,
    _: u64,
  ) -> Option<Box<dyn Iterator<Item = super::Decoration> + 'a>> {
    Some(Box::new(self.diagnostics.iter().map(|diag| Decoration {
      start: diag.start,
      end:   diag.end,
      style: diag.severity.highlight_key().to_string(),
    })))
  }

  // TODO: Add popup concept to editor.
  fn render(&self, _: &Editor, ctx: &mut ScopedCtx, scroll_row: u64) {
    if let Some((row, col, diagnostic)) = &self.popup {
      let cursor_row = (row - scroll_row) as u16;
      let cursor_col = (col + 4) as u16;

      const ROWS: u16 = 3;
      const COLS: u16 = 100;

      let border_style = ctx.theme().get("completion.border");
      let item_style = ctx.theme().get("completion.item");

      let row = cursor_row + 1;
      let col = cursor_col - 1;

      ctx.draw_char(row, col, '┌', border_style);
      ctx.draw_char(row + ROWS + 1, col, '└', border_style);
      ctx.draw_char(row, col + COLS + 1, '┐', border_style);
      ctx.draw_char(row + ROWS + 1, col + COLS + 1, '┘', border_style);

      for r in 1..ROWS + 1 {
        ctx.draw_char(row + r, col, '│', border_style);
        ctx.draw_char(row + r, col + COLS + 1, '│', border_style);
      }
      for c in 1..COLS + 1 {
        ctx.draw_char(row, col + c, '─', border_style);
        ctx.draw_char(row + ROWS + 1, col + c, '─', border_style);
      }
      for r in 1..ROWS + 1 {
        for c in 1..COLS + 1 {
          ctx.draw_char(row + r, col + c, ' ', border_style);
        }
      }

      let mut r = 0;
      let mut c = 0;
      for word in diagnostic.message.split(' ') {
        if c + word.len() as u16 > COLS {
          c = 0;
          r += 1;
        }
        for ch in word.chars() {
          ctx.draw_char(row + r + 1, col + c + 1, ch, item_style);
          c += 1;
        }
        c += 1;
      }

      /*
      let active_line = self.active.map(|a| a - self.scroll);
      for (i, compl) in self.completions.iter().skip(self.scroll).take(ROWS as usize).enumerate() {
        let icon_style = ctx.theme().get(compl.kind.style());
        ctx.put(row + i as u16 + 1, col + 1, compl.kind.icon().to_char(), icon_style);

        let style = if active_line == Some(i) { highlight_style } else { item_style };
        for (j, c) in compl.label.chars().enumerate() {
          ctx.put(row + i as u16 + 1, col + j as u16 + 3, c, style);
        }
      }
      */
    }
  }
}

impl Severity {
  pub fn to_icon(&self) -> Icon {
    match self {
      Severity::Error => Icon::CircleX,
      Severity::Warning => Icon::Warning,
      Severity::Info => Icon::CarrotRight,
    }
  }

  pub fn gutter_key(&self) -> &'static str {
    match self {
      Severity::Error => "editor.gutter.diagnostic.error",
      Severity::Warning => "editor.gutter.diagnostic.warning",
      Severity::Info => "editor.gutter.diagnostic.info",
    }
  }

  pub fn highlight_key(&self) -> &'static str {
    match self {
      Severity::Error => "highlight.diagnostic.error",
      Severity::Warning => "highlight.diagnostic.warning",
      Severity::Info => "highlight.diagnostic.info",
    }
  }
}
