use super::{Decoration, Editor};
use crate::{gui::text::StringWithColors, tab::editor::ScopedCtx, theme::ThemeStyle};
use std::cmp::min;

impl Editor {
  pub fn render(&self, ctx: &mut ScopedCtx, margin: u32, scroll_row: u64) {
    let first_row: u64 = scroll_row;
    let last_row: u64 = min(self.doc.len_lines(), first_row + u64::from(ctx.visible_rows()) - 1);

    let borrows = self.plugins.iter().map(|plugin| plugin.borrow()).collect::<Vec<_>>();

    let mut iters = borrows
      .iter()
      .filter_map(|plugin| plugin.decorate(self, first_row, last_row).map(|iter| iter.peekable()))
      .collect::<Vec<_>>();

    let mut col: u16 = 0;
    let mut row: u16 = 0;

    // Iterate through all decoration iterators in parallel. At any given character,
    // some number of decorations will be active. This list stores which styles are
    // active. The final style is built by taking the default style, and applying
    // all active styles in order onto it.
    //
    // This means that two conflicting decorations will choose the later style.
    let mut style_stack: Vec<Option<ThemeStyle>> = vec![None; iters.len()];
    let mut cursor = self.doc.line(first_row).start();

    for iter in iters.iter_mut() {
      chomp(iter, cursor);
    }

    let mut string = StringWithColors::new();

    'outer: while cursor < self.doc.len() {
      let mut next_change = u64::MAX;
      for (i, iter) in iters.iter_mut().enumerate() {
        if let Some(decoration) = iter.peek() {
          // All decorations should be ahead of the cursor, unless the iterator is
          // returning this in the wrong order. Decorations behind the cursor mean that
          // `chomp` is bugged.
          assert!(decoration.end >= cursor);

          if decoration.start > cursor {
            next_change = next_change.min(decoration.start);
          }
          if decoration.end > cursor {
            next_change = next_change.min(decoration.end);
          }

          if decoration.start == cursor {
            style_stack[i] = Some(ctx.theme().get_no_default(&decoration.style));
          }
          if decoration.end == cursor {
            style_stack[i] = None;
            chomp(iter, cursor + 1);

            if let Some(next) = iter.peek() {
              if next.start > cursor {
                next_change = next_change.min(next.start);
              }
            }
          }
        }
      }

      let mut style = ctx.theme().default;
      for s in style_stack.iter().flatten() {
        style.merge_with(s);
      }

      let chars =
        self.doc.rope().chars_at(cursor as usize).take(next_change as usize - cursor as usize);
      for c in chars {
        if row >= ctx.visible_rows() {
          break 'outer;
        }
        if c == '\n' {
          ctx.draw_str_px(margin, ctx.row_to_px(row), string.clone());
          string.empty();
          col = 0;
          row += 1;
        } else {
          string.push(c, style.foreground);
          col += 1;
        }
        cursor += 1;
      }
    }
    ctx.draw_str_px(ctx.col_to_px(col) + margin, ctx.row_to_px(row), string);
  }
}

fn chomp(iter: &mut std::iter::Peekable<impl Iterator<Item = Decoration>>, cursor: u64) {
  while let Some(decoration) = iter.peek() {
    if decoration.start >= cursor {
      break;
    } else {
      iter.next();
    }
  }
}
