use crate::{
  git::{Diff, LineDiff},
  location::FileLocation,
  tab::editor::ScopedCtx,
};

use super::{Editor, Plugin, Transaction};

pub struct GitPlugin {
  diff: Option<Diff>,
}

impl GitPlugin {
  pub fn new() -> Self { GitPlugin { diff: None } }
}

impl Plugin for GitPlugin {
  fn gutter_width(&self) -> u32 { 10 }

  fn render_gutter(&self, _editor: &Editor, scroll_row: u64, ctx: &mut ScopedCtx) {
    if let Some(diff) = &self.diff {
      let res = diff.lines.binary_search_by_key(&scroll_row, |(line, _)| *line);
      let start = match res {
        Ok(i) => i,
        Err(i) => i,
      };

      for (line, diff) in diff.lines.iter().skip(start) {
        if *line > scroll_row + ctx.visible_rows() as u64 {
          break;
        }
        let style_key = match diff {
          LineDiff::Add => "editor.gutter.diff.add",
          LineDiff::Remove => "editor.gutter.diff.remove",
          LineDiff::Change => "editor.gutter.diff.change",
        };
        ctx.draw_rect_px(
          0,
          ctx.row_to_px((*line - scroll_row) as u16),
          4,
          ctx.row_to_px(1),
          ctx.theme().get(style_key).foreground,
        );
      }
    }
  }

  fn on_post_change(&mut self, editor: &Editor, _: &Transaction) {
    let diff = editor.file.as_ref().and_then(|(location, _)| {
      let path = match location {
        FileLocation::Path(path) => path,
      };
      let git_file = crate::git::head_file(path)?;
      Some(crate::git::diff(&git_file, &editor.doc.to_string()))
    });
    self.diff = diff;
  }
}
