use crate::gui::api::Color;
use std::collections::HashMap;

mod default_theme;
mod style;

pub use style::{Style, StyleFlags};

pub struct Theme {
  styles:      HashMap<String, ThemeStyle>,
  pub default: Style,
}

/// A style with optional fields, so that a default can be used.
#[derive(Debug, Clone, Copy, PartialEq, Default)]
pub struct ThemeStyle {
  pub foreground: Option<Color>,
  pub background: Option<Color>,
  pub flags:      Option<StyleFlags>,
}

impl Theme {
  pub fn new() -> Self { Theme { styles: HashMap::new(), default: Style::default() } }

  pub fn set(&mut self, key: &str, style: ThemeStyle) { self.styles.insert(key.into(), style); }

  pub fn get_no_default(&self, key: &str) -> ThemeStyle {
    let mut ret = ThemeStyle::default();

    // Loops through in reverse order, slicing the key each time, so that `partial`
    // looks like:
    //
    // foo.bar.baz (iter 0)
    // foo.bar     (iter 1)
    // foo         (iter 2)
    let mut end = key.len();
    for section in key.split('.').rev() {
      let partial = &key[..end];

      if let Some(style) = self.styles.get(partial) {
        ret.foreground = ret.foreground.or(style.foreground);
        ret.background = ret.background.or(style.background);
        ret.flags = ret.flags.or(style.flags);

        if ret.foreground.is_some() && ret.background.is_some() && ret.flags.is_some() {
          break;
        }
      }

      if section.len() >= end {
        break;
      }
      end -= section.len() + 1;
    }

    ret
  }

  pub fn get(&self, key: &str) -> Style { self.get_no_default(key).unwrap_or(self.default) }
}

impl Style {
  pub fn merge_with(&mut self, other: &ThemeStyle) {
    if let Some(foreground) = other.foreground {
      self.foreground = foreground;
    }
    if let Some(background) = other.background {
      self.background = background;
    }
    if let Some(flags) = other.flags {
      self.flags = flags;
    }
  }
}

#[allow(unused)]
impl ThemeStyle {
  pub fn unwrap_or(self, other: Style) -> Style {
    Style {
      foreground: self.foreground.unwrap_or(other.foreground),
      background: self.background.unwrap_or(other.background),
      flags:      self.flags.unwrap_or(other.flags),
    }
  }

  pub fn foreground(color: impl Into<Color>) -> Self {
    ThemeStyle::default().with_foreground(color)
  }
  pub fn background(color: impl Into<Color>) -> Self {
    ThemeStyle::default().with_background(color)
  }
  pub fn flags(flags: impl Into<StyleFlags>) -> Self { ThemeStyle::default().with_flags(flags) }

  pub fn with_foreground(mut self, color: impl Into<Color>) -> Self {
    self.foreground = Some(color.into());
    self
  }
  pub fn with_background(mut self, color: impl Into<Color>) -> Self {
    self.background = Some(color.into());
    self
  }
  pub fn with_flags(mut self, flags: impl Into<StyleFlags>) -> Self {
    self.flags = Some(flags.into());
    self
  }
}

#[cfg(test)]
mod tests {
  use super::*;

  #[test]
  fn lookup() {
    let mut theme = Theme::new();

    theme.set("foo.bar.baz", ThemeStyle::foreground(0xff0000));
    theme.set("foo.bar", ThemeStyle::background(0x00ff00));
    theme.default = Style::flags(StyleFlags(3));

    assert_eq!(
      theme.get("foo.bar.baz"),
      Style { foreground: 0xff0000.into(), background: 0x00ff00.into(), flags: StyleFlags(3) }
    );
  }
}
