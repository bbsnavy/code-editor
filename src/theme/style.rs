use crate::gui::api::Color;

#[derive(Clone, Copy, PartialEq)]
pub struct StyledChar {
  pub c:     char,
  pub style: Style,
}

impl Default for Style {
  fn default() -> Self {
    Style {
      foreground: 0xffffff.into(),
      background: 0x000000.into(),
      flags:      StyleFlags::default(),
    }
  }
}

#[derive(Clone, Copy, Debug, PartialEq)]
pub struct Style {
  pub foreground: Color,
  pub background: Color,
  pub flags:      StyleFlags,
}

#[derive(Clone, Copy, Debug, PartialEq, Default)]
pub struct StyleFlags(pub u8);

const BOLD_BIT: u8 = 0x01;
const DIM_BIT: u8 = 0x02;
const ITALIC_BIT: u8 = 0x04;
const UNDERLINE_BIT: u8 = 0x08;
const BLINKING_BIT: u8 = 0x10;
const INVERSE_BIT: u8 = 0x20;
const HIDDEN_BIT: u8 = 0x40;
const STRIKETHROUGH_BIT: u8 = 0x80;

#[allow(unused)]
impl StyleFlags {
  pub fn bold(&self) -> bool { self.0 & BOLD_BIT != 0 }
  pub fn dim(&self) -> bool { self.0 & DIM_BIT != 0 }
  pub fn italic(&self) -> bool { self.0 & ITALIC_BIT != 0 }
  pub fn underline(&self) -> bool { self.0 & UNDERLINE_BIT != 0 }
  pub fn blinking(&self) -> bool { self.0 & BLINKING_BIT != 0 }
  pub fn inverse(&self) -> bool { self.0 & INVERSE_BIT != 0 }
  pub fn hidden(&self) -> bool { self.0 & HIDDEN_BIT != 0 }
  pub fn strikethrough(&self) -> bool { self.0 & STRIKETHROUGH_BIT != 0 }

  pub fn set_bold(&mut self) { self.0 |= BOLD_BIT }
  pub fn set_dim(&mut self) { self.0 |= DIM_BIT }
  pub fn set_italic(&mut self) { self.0 |= ITALIC_BIT }
  pub fn set_underline(&mut self) { self.0 |= UNDERLINE_BIT }
  pub fn set_blinking(&mut self) { self.0 |= BLINKING_BIT }
  pub fn set_inverse(&mut self) { self.0 |= INVERSE_BIT }
  pub fn set_hidden(&mut self) { self.0 |= HIDDEN_BIT }
  pub fn set_strikethrough(&mut self) { self.0 |= STRIKETHROUGH_BIT }
}

#[allow(unused)]
impl Style {
  pub fn foreground(color: impl Into<Color>) -> Self { Style::default().with_foreground(color) }
  pub fn background(color: impl Into<Color>) -> Self { Style::default().with_background(color) }
  pub fn flags(flags: impl Into<StyleFlags>) -> Self { Style::default().with_flags(flags) }

  pub fn with_foreground(mut self, color: impl Into<Color>) -> Self {
    self.foreground = color.into();
    self
  }
  pub fn with_background(mut self, color: impl Into<Color>) -> Self {
    self.background = color.into();
    self
  }
  pub fn with_flags(mut self, flags: impl Into<StyleFlags>) -> Self {
    self.flags = flags.into();
    self
  }
}
