// TODO: Move to config file or somethin

use super::{Style, StyleFlags, Theme, ThemeStyle};

impl Theme {
  #[allow(unused)]
  pub fn default_theme() -> Self {
    let mut theme = Theme::new();

    // Is tokyonight
    //
    // https://github.com/folke/tokyonight.nvim

    let bg_dark = 0x16161e;
    let bg = 0x1a1b26;
    let bg_highlight = 0x292e42;
    let terminal_black = 0x414868;
    let fg = 0xc0caf5;
    let fg_dark = 0xa9b1d6;
    let fg_gutter = 0x3b4261;
    let dark3 = 0x545c7e;
    let comment = 0x565f89;
    let dark5 = 0x737aa2;
    let blue0 = 0x3d59a1;
    let blue = 0x7aa2f7;
    let cyan = 0x7dcfff;
    let blue1 = 0x2ac3de;
    let blue2 = 0x0db9d7;
    let blue5 = 0x89ddff;
    let blue6 = 0xb4f9f8;
    let blue7 = 0x394b70;
    let magenta = 0xbb9af7;
    let magenta2 = 0xff007c;
    let purple = 0x9d7cd8;
    let orange = 0xff9e64;
    let yellow = 0xe0af68;
    let green = 0x9ece6a;
    let green1 = 0x73daca;
    let green2 = 0x41a6b5;
    let teal = 0x1abc9c;
    let red = 0xf7768e;
    let red1 = 0xdb4b4b;

    let mut underline = StyleFlags::default();
    underline.set_underline();

    theme.default = Style::background(bg).with_foreground(fg);

    theme.set("file_tree", ThemeStyle::background(bg_dark));
    theme.set("file_tree.active", ThemeStyle::background(bg_highlight));

    theme.set("status", ThemeStyle::background(bg_dark));
    theme.set("status.mode.normal", ThemeStyle::background(blue1));
    theme.set("status.mode.insert", ThemeStyle::background(green));
    theme.set("status.mode.visual", ThemeStyle::background(magenta));
    theme.set("status.mode.command", ThemeStyle::background(orange));

    theme.set("editor.gutter", ThemeStyle::background(bg).with_foreground(fg_dark));
    theme.set("editor.gutter.diff.add", ThemeStyle::foreground(green));
    theme.set("editor.gutter.diff.remove", ThemeStyle::foreground(red));
    theme.set("editor.gutter.diff.change", ThemeStyle::foreground(orange));
    theme.set("editor.selection", ThemeStyle::background(blue1));

    theme
      .set("completion.background", ThemeStyle::background(bg_dark).with_foreground(bg_highlight));
    theme.set("completion.active", ThemeStyle::background(bg_highlight));
    // I chose some random colors here
    theme.set("completion.item.icon.text", ThemeStyle::default());
    theme.set("completion.item.icon.method", ThemeStyle::foreground(blue5));
    theme.set("completion.item.icon.function", ThemeStyle::foreground(blue));
    theme.set("completion.item.icon.constructor", ThemeStyle::foreground(red));
    theme.set("completion.item.icon.field", ThemeStyle::foreground(green));
    theme.set("completion.item.icon.variable", ThemeStyle::foreground(magenta2));
    theme.set("completion.item.icon.class", ThemeStyle::foreground(purple));
    theme.set("completion.item.icon.interface", ThemeStyle::foreground(magenta));
    theme.set("completion.item.icon.module", ThemeStyle::foreground(purple));
    theme.set("completion.item.icon.property", ThemeStyle::foreground(orange));
    theme.set("completion.item.icon.unit", ThemeStyle::default());
    theme.set("completion.item.icon.value", ThemeStyle::default());
    theme.set("completion.item.icon.enum", ThemeStyle::foreground(green));
    theme.set("completion.item.icon.keyword", ThemeStyle::foreground(yellow));
    theme.set("completion.item.icon.snippet", ThemeStyle::foreground(green2));
    theme.set("completion.item.icon.color", ThemeStyle::foreground(red));
    theme.set("completion.item.icon.file", ThemeStyle::foreground(green1));
    theme.set("completion.item.icon.reference", ThemeStyle::foreground(blue1));
    theme.set("completion.item.icon.folder", ThemeStyle::foreground(yellow));
    theme.set("completion.item.icon.enum-member", ThemeStyle::foreground(green1));
    theme.set("completion.item.icon.constant", ThemeStyle::foreground(teal));
    theme.set("completion.item.icon.struct", ThemeStyle::foreground(orange));
    theme.set("completion.item.icon.event", ThemeStyle::foreground(yellow));
    theme.set("completion.item.icon.operator", ThemeStyle::foreground(purple));
    theme.set("completion.item.icon.type-parameter", ThemeStyle::foreground(orange));

    theme.set("highlight.comment", ThemeStyle::foreground(comment));
    theme.set("highlight.constant", ThemeStyle::foreground(orange));
    theme.set("highlight.type", ThemeStyle::foreground(blue1));
    theme.set("highlight.function", ThemeStyle::foreground(cyan));
    theme.set("highlight.function.macro", ThemeStyle::foreground(purple));
    theme.set("highlight.property", ThemeStyle::foreground(green1));
    theme.set("highlight.keyword", ThemeStyle::foreground(purple));
    theme.set("highlight.punctuation.bracket", ThemeStyle::foreground(fg));
    theme.set("highlight.punctuation.delimiter", ThemeStyle::foreground(magenta));
    theme.set("highlight.label", ThemeStyle::foreground(magenta2));
    theme.set("highlight.variable.builtin", ThemeStyle::foreground(teal));
    theme.set("highlight.string", ThemeStyle::foreground(green));

    theme.set("editor.gutter.diagnostic.error", ThemeStyle::foreground(red));
    theme.set("editor.gutter.diagnostic.warning", ThemeStyle::foreground(orange));
    theme.set("editor.gutter.diagnostic.info", ThemeStyle::foreground(green2));
    theme.set("highlight.diagnostic.error", ThemeStyle::foreground(red).with_flags(underline));
    theme.set("highlight.diagnostic.warning", ThemeStyle::foreground(orange).with_flags(underline));
    theme.set("highlight.diagnostic.info", ThemeStyle::foreground(green2).with_flags(underline));

    theme
  }
}
