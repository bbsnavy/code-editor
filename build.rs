use std::{io::Write, path::Path, process::Command};

fn main() {
  let ts_path = Path::new("./deps/tree-sitter/lib");

  std::fs::create_dir_all("deps").unwrap();
  Command::new("git")
    .arg("clone")
    .arg("git@github.com:tree-sitter/tree-sitter")
    .arg("deps/tree-sitter")
    .status()
    .unwrap();

  cc::Build::new()
    .flag_if_supported("-std=c99")
    .flag_if_supported("-Wno-unused-parameter")
    .include(ts_path.join("src"))
    .include(ts_path.join("include"))
    .file(ts_path.join("src/lib.c"))
    .compile("tree-sitter");

  nerdfonts();
}

fn nerdfonts() {
  let url = format!("https://www.nerdfonts.com/cheat-sheet");

  Command::new("curl")
    .arg("-L")
    .arg(&url)
    .arg("-o")
    .arg("deps/nerd-fonts-cheat-sheet.html")
    .status()
    .unwrap();

  let str = std::fs::read_to_string(format!("deps/nerd-fonts-cheat-sheet.html")).unwrap();

  let mut glyphs = vec![];

  let start = "const glyphs = {";
  for line in str.lines().skip_while(|line| line != &start) {
    if line == start {
      continue;
    }
    if line == "}" {
      break;
    }

    let (name, unicode) = line.trim().strip_suffix(",").unwrap().split_once(':').unwrap();

    let name = name.trim().strip_prefix('"').unwrap().strip_suffix('"').unwrap();
    let unicode = unicode.trim().strip_prefix('"').unwrap().strip_suffix('"').unwrap();

    if name.starts_with("nfold") {
      continue;
    }

    glyphs.push((name, char::from_u32(u32::from_str_radix(unicode, 16).unwrap()).unwrap()));
  }

  let mut out = std::fs::File::create("src/nerdfonts/icons.rs").unwrap();

  writeln!(out, "//! This file is auto-generated. See build.rs").unwrap();
  writeln!(out, "").unwrap();

  writeln!(out, "#[derive(Clone, Copy, Debug, PartialEq, Eq)]").unwrap();
  writeln!(out, "#[allow(unused)]").unwrap();
  writeln!(out, "pub enum Icon {{").unwrap();
  for (name, c) in &glyphs {
    let enum_name = to_enum_name(name);
    writeln!(out, "  /// {name} - {}  - \\u{{{:04x}}}", c, *c as u32).unwrap();
    writeln!(out, "  {enum_name},").unwrap();
  }
  writeln!(out, "}}").unwrap();

  writeln!(out, "#[allow(unused)]").unwrap();
  writeln!(out, "pub fn to_char(icon: Icon) -> char {{").unwrap();
  writeln!(out, "  match icon {{").unwrap();
  for (name, c) in glyphs {
    let enum_name = to_enum_name(&name);
    writeln!(out, "    Icon::{enum_name} => '\\u{{{:04x}}}',", c as u32).unwrap();
  }
  writeln!(out, "  }}").unwrap();
  writeln!(out, "}}").unwrap();
}

fn to_enum_name(name: &str) -> String {
  let mut out = String::new();
  let name = name.strip_prefix("nf-").unwrap();
  let mut capitalize = true;
  for char in name.chars() {
    match char {
      c if c.is_lowercase() => {
        out.push(if capitalize { c.to_ascii_uppercase() } else { c });
        capitalize = false;
      }
      c if c.is_digit(10) => {
        capitalize = true;
        out.push(c)
      }
      _ => capitalize = true,
    }
  }
  out
}
